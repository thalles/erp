<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonAuth for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Auth;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router'       => [
        'routes' => [
            'auth'        => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/auth[/:action]',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'logout'      => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/logout',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'logout',
                    ],
                ],
            ],
            'auth-acl'    => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/auth/acl[/:action][[/id]/:id][/page/:page]',
                    'defaults' => [
                        'controller' => Controller\AclController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'auth-pessoa' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/auth/pessoa[/:action][[/id]/:id][/page/:page]',
                    'defaults' => [
                        'controller' => Controller\PessoaController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
    'controllers'  => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map'             => [
            'auth/index/index' => __DIR__ . '/../view/auth/index/index.phtml',
            'error/404'        => __DIR__ . '/../view/error/404.phtml',
            'error/index'      => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack'      => [
            __DIR__ . '/../view',
        ],
    ],
];
