<?php

namespace Auth\Controller;

use App\Controller\AbstractController;
use Auth\Form\Login as Login;
use Zend\Authentication\AuthenticationService;

class IndexController extends AbstractController {

    public function indexAction() {
        $formLogin = new Login();
        if (!$this->isPost()) {
            return ['formLogin' => $formLogin];
        }
        if (!$formLogin->setData($this->fromPost())->isValid()) {
            $this->setMessageError('Preencha o formulário corretamente.');
            return ['formLogin' => $formLogin];
        }
        $authService = new AuthenticationService();
        $authAdapter = $this->getServiceManager()->get('Auth\Adapter\Adapter');
        $authAdapter->setEmail($this->fromPost('email'))->setSenha($this->fromPost('senha'));
        $result      = $authService->authenticate($authAdapter);

        if (!$result->isValid()) {
            $this->setMessageError(current($result->getMessages()));
            return ['formLogin' => $formLogin];
        }
        return $this->redirect()->toRoute('manage');
    }

    public function logoutAction() {
        $authService = new AuthenticationService;
        $authService->clearIdentity();
        return $this->redirect()->toRoute('auth');
    }

}
