<?php

namespace Auth\Form;

/**
 * Description of Login
 *
 * @author Thalles
 */
class Login extends AbstractForm {

    public function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
        $this->setAttributes([
            'method' => 'post',
            'name' => 'form-login'
        ]);
        $this->setAttribute('autocomplete', 'off');

        //$this->setInputFilter(new Filter\Login());
        $this->setAttribute('method', 'post');
        $this->setLabelOptions(['Should_create_template' => false]);


        //Adição do campo Nome
        $email = new \Zend\Form\Element\Text('email');
        $email->setLabel('E-mail')
                ->setLabelAttributes(['class' => 'label-texto col-xs-12'])
                ->setAttributes(['placeholder' => 'Login', 'class' => 'form-control', 'aria-describedby' => "basic-addon1"]);
        $this->add($email);

        $senha = new \Zend\Form\Element\Password('senha');
        $senha->setLabel('Senha')
                ->setLabelAttributes(['class' => 'label-texto col-xs-12'])
                ->setAttributes(['placeholder' => 'Senha', 'class' => 'form-control', 'aria-describedby' => "basic-addon1"]);
        $this->add($senha);

        $relembrar = new \Zend\Form\Element\Checkbox('relembrar');
        $relembrar->setLabel('Relembrar-me')
                ->setLabelAttributes(['class' => 'checkbox-login  col-xs-12'])
                ->setCheckedValue('SIM')
                ->setUncheckedValue("NAO");
        $this->add($relembrar);

        $submit = new \Zend\Form\Element\Submit('submit');
        $submit->setLabelAttributes(['class' => 'label-texto col-xs-12'])
                ->setValue('Autenticar')
                ->setAttribute('class', 'btn btn-submit pull-right')
        ;
        $this->add($submit);
    }

}
