<?php

namespace Auth\Adapter;

use Zend\Authentication\Adapter\AdapterInterface,
    Zend\Authentication\Result;
use Doctrine\ORM\EntityManager;

class Adapter implements AdapterInterface {

    /**
     * @var EntityManager
     */
    protected $em;
    protected $email;
    protected $senha;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    public function getEm() {
        return $this->em;
    }

    public function setEm($em) {
        $this->em = $em;
        return $this;
    }

    function getEmail() {
        return $this->apelido;
    }

    function getSenha() {
        return $this->senha;
    }

    function setEmail($email) {
        $this->apelido = $email;
        return $this;
    }

    function setSenha($senha) {
        $this->senha = sha1($senha);
        return $this;
    }

    public function authenticate() {
        $repository = $this->em->getRepository('App\Entity\Seguranca');
        $usuario = $repository->getUser($this->getEmail());
        if ($usuario and ( $usuario->getStatus() != 1)) {
            return new Result(Result::FAILURE_UNCATEGORIZED, null, [
                'Usuário não autorizado(a).'
            ]);
        }
        if ($usuario and ( $usuario->getSenha() == $this->getSenha())) {
            return new Result(Result::SUCCESS, $usuario, [
                'ok'
            ]);
        }
        return new Result(Result::FAILURE_CREDENTIAL_INVALID, null, [
            'Usuário e/ou senha inválido(s).'
        ]);
    }

}
