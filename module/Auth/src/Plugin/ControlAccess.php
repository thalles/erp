<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Auth\Plugin;

use Zend\Authentication\AuthenticationService;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Mvc\MvcEvent;
use Zend\Permissions\Acl\Acl as Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Session\Container as SessionContainer;

/**
 * Description of ControlAccess
 *
 * @copyright (c) 2017, MidiaUAI
 * @author Thalles Ferreira
 */
class ControlAccess extends AbstractPlugin {

    protected $session;
    protected $e;

    /**
     * @param MvcEvent $e
     * @return type
     */
    public function getEm(MvcEvent $e) {
        $em = $e->getApplication()->getServiceManager()->get('Doctrine\ORM\EntityManager');
        return $em;
    }

    /**
     * @return SessionContainer
     */
    private function getSession(): SessionContainer {
        if (!$this->session) {
            $this->session = new SessionContainer();
        }
        return $this->session;
    }

    /**
     * @param MvcEvent $e
     * @return string
     */
    public function getModuleName(MvcEvent $e): string {
        $controllerClass = get_class($e->getTarget());

        return strtolower(substr($controllerClass, 0, strpos($controllerClass, '\\')));
    }

    /**
     * @param MvcEvent $e
     * @return string
     */
    public function getControllerName(MvcEvent $e): string {
        $params = str_replace('controller', '', strtolower(get_class($e->getTarget())));
        if (strripos($params, '\\')) {
            return substr($params, strripos($params, '\\') + 1, strlen($params));
        }
        return $params;
    }

    /**
     * @param MvcEvent $e
     * @return string
     */
    public function getActionName(MvcEvent $e): string {
        return strtolower($e->getRouteMatch()->getParam('action'));
    }

    public function isAuthenticated(MvcEvent $e) {

        $authService = new AuthenticationService();
        $controllerClass = get_class($e->getTarget());
        if (!$authService->hasIdentity() and ( $controllerClass != 'Auth\Controller\IndexController')) {
            return $e->getTarget()->redirect()->toRoute('auth');
            //return false;
        }
        return $authService;
    }

    /**
     * @param MvcEvent $e
     * @return type
     */
    public function setAutorization(MvcEvent $e) {
        if ($this->isAuthenticated($e)) {

        }



        $acl = new Acl();
        $acl->addResource('application/index')->addRole(new Role('usuario'));
        $acl->allow('usuario', 'application/index', ['index']);
        $pessoa = $this->getEm($e)->getRepository('App\Entity\Pessoa')->find($this->isAuthenticated($e)->getIdentity()->getId());
        $aclContainer->grupos = [];
        foreach ($pessoa->getPessoaGrupo() as $pessoaGrupo) {
            $grupo = strtolower($pessoaGrupo->getGrupo()->getNome());
            $aclContainer->grupos[] = $grupo;
            foreach ($pessoaGrupo->getGrupo()->getPermissaoGrupo() as $permissaoGrupo) {
                $resource = strtolower($permissaoGrupo->getPermissao()->getController());
                $privileges = explode(',', strtolower($permissaoGrupo->getPermissao()->getActions()));
                if ($acl->hasResource($resource)) {
                    $acl->allow('usuario', $resource, $privileges);
                } else {
                    $acl->addResource($resource)->allow('usuario', $resource, $privileges);
                }
            }
        }
        return $acl;
    }

    /**
     * @param MvcEvent $e
     * @return type
     */
    public function getAutorization(MvcEvent $e) {
        $aclContainer = new SessionContainer('acl');
        if ($aclContainer->acl) {
            return $aclContainer->acl;
        }
        $aclContainer->acl = $this->setAutorization($e);
        return $aclContainer->acl;
    }

    /**
     * @param MvcEvent $e
     * @return boolean
     */
    public function isAutorization(MvcEvent $e) {
        $moduleName = $this->getModuleName($e);
        $controllerName = $this->getControllerName($e);
        $actionName = $this->getActionName($e);
        return true; //remover essa linha

        $authService = $this->isAuthenticated($e);

        if (!$authService) {
            return $e->getTarget()->redirect()->toRoute('auth-index');
        }
        if ($authService->hasIdentity()) {

            $acl = $this->getAutorization($e);
            $resourceName = $moduleName . '/' . $controllerName;
            $session = $this->getSession();
            if (!$acl->hasResource($resourceName) || !$acl->isAllowed('usuario', $resourceName, $actionName)) {
                $session->mensagem = ['tipo'  => 'danger',
                    'texto' => '<strong>Erro!</strong> Você não tem permissão para executar essa ação. Duvida! Consulte o <strong> administrador.</strong> '
                    . '  <button class="btn pull-right" style="margin-top: -9px;" onclick="window.history.back();">Voltar</button>',
                ];
                return $e->getTarget()->redirect()->toRoute('home');
            }
            return true;
        }
    }

}
