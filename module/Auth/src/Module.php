<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonAuth for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Auth;

use Auth\Adapter\Adapter as AuthAdapter;
use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\MvcEvent;

class Module {

    const VERSION = '3.0.3-dev';

    public function getConfig() {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function init(ModuleManager $mm) {

        //$mm->getEventManager()->getSharedManager()->attach(__NAMESPACE__, 'dispatch', function($e) {
        //    $e->getTarget()->layout('auth/layout');
        //});
    }

    public function onBootstrap(MvcEvent $e) {

        $eventManager = $e->getApplication()->getEventManager();
        $eventManager->attach('route', array($this, 'loadPlugin'), 100);

        $eventManager->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e) {
            $controller = $e->getTarget();
            $controllerClass = get_class($controller);
            $moduleNamespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));
            $config = $e->getApplication()->getServiceManager()->get('config');

            $routeMatch = $e->getRouteMatch();
            $actionName = strtolower($routeMatch->getParam('action', 'not-found')); // get the action name

            if (isset($config['view_manager']['template_map'][strtolower($moduleNamespace) . '/layout'])) {
                $controller->layout($config['view_manager']['template_map'][strtolower($moduleNamespace) . '/layout']);
            }
        }, 100);
    }

    public function loadPlugin(MvcEvent $e) {

        $application = $e->getApplication();
        $sm = $application->getServiceManager();
        $sharedManager = $application->getEventManager()->getSharedManager();

        $matchedRoute = $sm->get('router')->match($sm->get('request'));

        if (null !== $matchedRoute) {
            $sharedManager->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e) use ($sm) {
                //remover comentario para ativa controle de acesso
                return $sm->get('ControllerPluginManager')->get('Auth\Plugin\ControlAccess')->isAuthenticated($e);
            }, 100);
        }
    }

    public function getServiceConfig() {

        return array(
            'factories' => array(
                'Auth\Adapter\Adapter' => function ($service) {
                    return new AuthAdapter($service->get('Doctrine\ORM\EntityManager'));
                },
            ),
        );
    }

}
