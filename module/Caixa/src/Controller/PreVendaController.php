<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Caixa\Controller;

use App\Entity\Venda as EntityVenda;
use App\Form\Products\Venda as FormVenda;
use Doctrine\DBAL\DBALException;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Description of GestorController
 *
 * @author Thalles Ferreira
 */
class PreVendaController extends AbstractController {

    public function finalizarAction() {
        $id = $this->params()->fromRoute('id', false);
        if (!$id) {
            return $this->redirect()->toRoute('caixa');
        }

        $formVenda = new FormVenda();
        if (!$this->isPost()) {
            $objProdutoVenda = $this->getRepository('ProdutoVenda')->findBy(['venda' => $id]);
            return new ViewModel(['objProdutoVenda' => $objProdutoVenda, 'formVenda' => $formVenda]);
        }

        $formVenda->setData($this->params()->fromPost());
        if ($formVenda->isValid()) {
            try {
                $this->getRepository(EntityVenda::class)->salvar($this->params()->fromPost());
                $this->setMessageSuccess('Venda Finalizada!');
                return $this->redirect()->toRoute('caixa');
            } catch (DBALException $ex) {
                $this->setMessageError('Erro ao finalizar venda');
                return $this->redirect()->toRoute('caixa');
            }
        }
    }

    public function getAction() {
        $vendaId = $this->params()->fromRoute('id', 2);
        $venda = $this->getRepository('Venda')->find($vendaId);
        return new JsonModel([
            'produtos' => $this->getRepository('ProdutoVenda')->getProdutoByVenda($vendaId),
            'venda'    => $venda->toArray(),
            'vendedor' => $venda->getVendedor()->toArray()
        ]);
    }

}
