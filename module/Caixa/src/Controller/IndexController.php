<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Caixa\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Description of GestorController
 *
 * @author Thalles Ferreira
 */
class IndexController extends AbstractController {

    public function indexAction() {
        $collPreVendas = $this->getRepository('Venda')->findBy(['estatusVenda' => 1]);

//        echo $collPreVendas[0]->getProduto()->count();
//        exit;
        //print_r(($collPreVendas));
        return new ViewModel(['collPreVendas' => $collPreVendas]);
    }

}
