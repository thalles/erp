<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonCaixa for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Caixa;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

$rota = '[/:action][[/id]/:id][[/pessoa]/:pessoa][[/marca]/:marca][[/clone]/:clone][/]';
$constrains = [
    'controller' => '[a-zA-Z_-]*',
    'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
    'id'         => '[0-9]+',
    'pessoa'     => '[0-9]+',
    'marca'      => '[0-9]+',
    'clone'      => '[0-9]+',
];
return [
    'router'       => [
        'routes' => [
            'caixa'           => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/caixa' . $rota,
                    'constraints' => $constrains,
                    'defaults'    => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'caixa-pre-venda' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/caixa/pre-venda' . $rota,
                    'constraints' => $constrains,
                    'defaults'    => [
                        'controller' => Controller\PreVendaController::class,
                    ],
                ],
            ],
        ],
    ],
    'controllers'  => [
        'factories' => [
            Controller\IndexController::class    => InvokableFactory::class,
            Controller\PreVendaController::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map'             => [
            'caixa/index/index' => __DIR__ . '/../view/caixa/index/index.phtml',
            'error/404'         => __DIR__ . '/../view/error/404.phtml',
            'error/index'       => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack'      => [
            'Caixa'         => __DIR__ . '/../view',
            'layout/layout' => __DIR__ . '/../view/layout/caixa.phtml',
        ],
    ],
];
