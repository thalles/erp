<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeleton Auth for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Auth;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

//echo __DIR__ . '/../view/layout/layout.phtml', '<br>';

return [
    'session' => [
        'remember_me_seconds' => 1800,
        'use_cookies' => true,
        'cookie_httponly' => true,
        'upload_progress' => true
    ],
    'router' => [
        'routes' => [
            'auth-index' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/auth[/:action]',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'auth-acl' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/auth/acl[/:action][[/id]/:id][/page/:page]',
                    'defaults' => [
                        'controller' => Controller\AclController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'auth-pessoa' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/auth/pessoa[/:action][[/id]/:id][/page/:page]',
                    'defaults' => [
                        'controller' => Controller\PessoaController::class,
                        'action' => 'index',
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'abstract_factories' => [
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ],
    ],
    'controllers' => [
        'invokables' => [
            'Auth\Controller\Index' => 'Auth\Controller\IndexController',
            'Auth\Controller\Acl' => 'Auth\Controller\AclController',
            'Auth\Controller\Pessoa' => 'Auth\Controller\PessoaController',
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'auth/layout' => 'layout/auth',
            'auth/index/index' => __DIR__ . '/../view/auth/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
//        'strategies' => [
//            'ViewJsonStrategy',
//        ]
    ],
];
