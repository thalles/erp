<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Auth\Controller;

use Auth\Form\Esqueci;
use Auth\Form\Login as formLogin;
use Auth\Plugin\Mailer;
use Auth\Plugin\SessionStorage;
use Doctrine\DBAL\DBALException;
use Zend\Authentication\AuthenticationService;
use Zend\Session\Container as sessionContainer;
use Zend\View\Model\ViewModel;

/**
 * @autor Thalles Ferreira
 * @description ação de fazer login
 */
class IndexController extends AbstractController {

    /** @description ação de fazer login */
    public function indexAction() {
        //$this->layout('layout/auth');

        $formLogin = new formLogin();
        $request = $this->getRequest();
        $modulo = $this->params('modulo', 'secretaria');


        if (!$request->isPost()) {
            $this->dieSession();
            return ['form' => $formLogin];
        }

        $formLogin->setData($request->getPost());
        if (!$formLogin->isValid()) {
            $this->getSession()->mensagem = [
                'tipo' => 'danger',
                'texto' => '<b>Erro!</b> Os campos Usuário e Senha devem ser preenchidos.'
            ];
            return ['form' => $formLogin];
        }

        $data = $request->getPost()->toArray();
        $auth = new AuthenticationService;

        $authAdapter = $this->getServiceManager()->get('Auth\Adapter\Adapter');
        $authAdapter->setEmail($data['email'])->setSenha(sha1($data['senha']));
        $result = $auth->authenticate($authAdapter);
        if (!$result->isValid()) {
            $this->getSession()->mensagem = [
                'tipo' => 'danger',
                'texto' => '<b>Erro!</b> Usuário ou Senha inválido(s).'
            ];
            return ['form' => $formLogin];
        }

        $Identity = $auth->getIdentity();
        $sessionStorage = new SessionStorage("Usuario");
        $Identity['usuario']->relembrar = false;
        if ($data['relembrar'] === "SIM") {
            $Identity['usuario']->relembrar = true;
        }
        $sessionStorage->write($Identity['usuario'], null);

        return $this->redirect()->toRoute('home');
    }

    protected function containerDestroy($containeName) {
        $container = new sessionContainer($containeName);
        $container->getManager()->destroy();
    }

    public function dieSession() {

        $this->containerDestroy('acl');
        $this->containerDestroy('aluno');

        $auth = new AuthenticationService;
        $auth->setStorage(new SessionStorage('Usuario'));
        $auth->getStorage()->clear();
        $auth->clearIdentity();
    }

    /** @description ação de sair da conta */
    public function logoutAction() {

        $modulo = $this->params()->fromRoute('modulo', false);

        $this->dieSession();
        return $this->redirect()->toRoute('full-auth', ['controller' => 'index', 'action' => 'index']);
    }

    /** @description Recuperar senha pelo Help Desk da Astroluz */
    public function esqueciAction() {

        $session = $this->getSession();
        $formEsqueci = new Esqueci();
        $modulo = $this->params('modulo', 'secretaria');

        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayEsqueci = $request->getPost()->toArray();
            $formEsqueci->setData($arrayEsqueci);

            if ($formEsqueci->isValid()) {
                $pessoa = $this->getRepository('Pessoa')->findOneBy(['apelido' => $arrayEsqueci['email']]);
                if (count($pessoa) > 0) {

                    $senha = substr(md5(microtime() . date('dmYhmi')), 8, 8);
                    $arraySenha = [
                        'id' => $pessoa->getId(),
                        'senha' => sha1($senha)
                    ];

                    try {
                        $this->getRepository('Pessoa')->salvar($arraySenha);

                        $html = "Olá {$pessoa->getNome()},<br/>"
                                . "uma nova senha foi gerada para o Help Desk da Astroluz.<br/>"
                                . "Abaixo informações de acesso:<br/>"
                                . "Endereço: {$this->getBaseUrl()}<br/>"
                                . "Login: {$pessoa->getEmail()}<br/>"
                                . "Senha: {$senha}<br/>";


                        $mail = new Mailer();
                        $mail->addAddress($pessoa->getEmail(), $pessoa->getNome());     // Add a recipient

                        $mail->Subject = 'Esqueci a senha - ' . date('d/m/Y');
                        $mail->Body = $html;

                        if ($mail->send()) {
                            $this->getSession()->mensagem = [
                                'tipo' => 'success',
                                'texto' => "<b>Sucesso!</b> O sua nova senha foi gerada e enviado para <b>{$pessoa->getEmail()}</b>."
                            ];
                            return $this->redirect()->toRoute('full-auth', ['controller' => 'index', 'action' => 'index']);
                        } else {
                            $this->getSession()->mensagem = [
                                'tipo' => 'danger',
                                'texto' => "<b>Erro ao enviar dados de acesso!</b> Tente novamente.<br/>" . $mail->Host
                            ];
                        }
                    } catch (DBALException $exc) {
                        echo $exc->getTraceAsString();
                    }
                } else {
                    $this->getSession()->mensagem = [
                        'tipo' => 'danger',
                        'texto' => "<b>Erro!</b> E-mail não cadastrado no sistema."
                    ];
                }
            } else {
                $this->getSession()->mensagem = [
                    'tipo' => 'danger',
                    'texto' => "<b>Erro!</b> O formulário não foi preenchido corretamente."
                ];
            }
        } else {
            if (!isset($session->mensagem)) {
                $this->getSession()->mensagem = [
                    'tipo' => 'info',
                    'texto' => "<b>Atenção!</b> Todos os campos marcados com * são de preenchimento obrigatório."
                ];
            }
        }

//$this->addJsFile('/js/jquery.maskedinput.min.js');
//$this->addJsFile('/js/auth/index.esqueci.js');
        return new ViewModel([
            'form' => $formEsqueci,
        ]);
    }

}
