<?php

/**
 * Description of CategoriasController
 *
 * @author Thalles Ferreira
 */

namespace Auth\Controller;

abstract class AbstractController extends \App\Controller\AbstractController {

    /**
     *
     * @var EntityManager;
     */
    protected $em;
    protected $service;
    protected $entity;
    protected $form;
    protected $route;
    protected $controller;

}
