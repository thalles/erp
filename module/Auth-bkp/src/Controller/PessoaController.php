<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Auth\Controller;

use Auth\Form\Senha;
use Doctrine\DBAL\DBALException;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

/**
 * @autor Thalles Ferreira
 * @descri ação de fazer login
 */
class PessoaController extends AbstractController {

    /** @description Alterar senha */
    public function senhaAction() {

        $this->layout('layout/layout');

        $route = $this->params()->fromRoute("route", false);
        $controller = $this->params()->fromRoute("controller", false);
        $action = $this->params()->fromRoute("action", false);

        $session = new Container();
        $request = $this->getRequest();

        $formSenha = new Senha();


        if ($request->isPost()) {
            $arraySenha = $request->getPost()->toArray();
            $formSenha->setData($arraySenha);

            $arraySenha['senha'] = sha1($arraySenha['senha']);
            $arraySenha['id'] = $this->getIdentity()->getId();



            if ($formSenha->isValid()) {

                $usuario = $this->getRepository('pessoa')->findOneBy(['id' => $arraySenha['id'], 'senha' => sha1($arraySenha['senhaAtual'])]);
                if (count($usuario) > 0) {
                    try {
                        $this->getRepository('Pessoa')->salvar($arraySenha);
                        $session->mensagem = [
                            'tipo' => 'success',
                            'texto' => "<b>Sucesso!</b> Sua senha foi alterada."
                        ];
                        if ($route) {
                            return $this->redirect()->toRoute($route, ['controller' => $controller, 'action' => $action]);
                        }
                        return $this->redirect()->toRoute('full-auth', ['controller' => 'pessoa', 'action' => 'senha']);
                    } catch (DBALException $exc) {
                        $session->mensagem = [
                            'tipo' => 'danger',
                            'texto' => "<b>Erro!</b> Não foi possivel salvar a nova senha."
                        ];
                    }
                } else {
                    $session->mensagem = [
                        'tipo' => 'danger',
                        'texto' => "<b>Erro!</b> Senha atual inválida."
                    ];
                }
            } else {
                $session->mensagem = [
                    'tipo' => 'danger',
                    'texto' => "<b>Erro!</b> O formulário não foi preenchido de forma correta."
                ];
            }
        } else {
            if (!isset($session->mensagem)) {
                $session->mensagem = [
                    'tipo' => 'info',
                    'texto' => "<b>Atenção!</b> Todos os campos marcados com * são de preenchimento obrigatório."
                ];
            }
        }



        return new ViewModel([
            'formSenha' => $formSenha,
        ]);
    }

}
