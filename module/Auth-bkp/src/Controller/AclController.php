<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Auth\Controller;

use Zend\Json\Json;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Paginator\Paginator;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

/**
 * @autor Thalles Ferreira
 * @descri ação de fazer login
 */
class AclController extends AbstractController {

    public function __construct() {

        //$this->setLayout('layout/secretaria.phtml');

        $this->session = new Container();
        $this->entity = 'Application\Entity\Grupo';
        $this->service = 'Application\Service\Grupo';
        $this->route = 'full-sec';
        $this->controller = 'Acl';
    }

    /** @description Lista de grupos de usuários */
    public function IndexAction() {

        $listGrupos = $this->getRepository('Grupo')
                ->findBy([]);

        $page = $this->params()->fromRoute('page');

        $paginator = new Paginator(new ArrayAdapter($listGrupos));
        $paginator->setCurrentPageNumber($page);
        $paginator->setDefaultItemCountPerPage(10);


        $this->getViewHelper('HeadScript')->appendFile('/js/Auth/acl.index.js');

        return new ViewModel(array(
            'grupos' => $listGrupos
        ));
    }

    /** @description Conceder ou revogar permissões a grupos de usuarios */
    public function PermissoesAction() {

        $grupo = $this->params()->fromRoute('id', false);

        $listGrupo = false;
        if ($grupo) {
            $listGrupo = $this->getRepository('Grupo')
                    ->findOneById($grupo);
        }

        $this->getViewHelper('HeadScript')->appendFile('/js/jquery.form.min.js');
        $this->getViewHelper('HeadScript')->appendFile('/js/Auth/acl.permissoes.js');

        $this->session->mensagem = array(
            'tipo' => 'info',
            'texto' => 'Use o checklist para cosseder ou revogar permissões dos grupos.'
        );

        return new ViewModel([
            'modulesDirectory' => dirname($this->getRequest()->getServer('DOCUMENT_ROOT')) . '/module',
            'serviceLocator' => $this->getServiceManager(),
            'listGrupo' => $listGrupo,
        ]);
    }

    /** @description Salvar informações do grupo */
    public function salvarAction() {

        $request = $this->getRequest();
        if ($request->isPost()) {

            $idGrupo = $this->params()->fromPost('idGrupo', false);
            $nome = $this->params()->fromPost('nome', false);
            $descricao = $this->params()->fromPost('descricao', false);
            $controllers = $this->params()->fromPost('controllers', false);

            if ($nome && $descricao && $controllers) {

                $arrayGrupo = [
                    'id' => $idGrupo,
                    'nome' => $nome,
                    'descricao' => $descricao
                ];

                $resultGrupo = $this->getRepository('Grupo')->salvar($arrayGrupo);

                if ($resultGrupo) {
                    if ($idGrupo) {
                        $this->getRepository('permissao')->deleteByGrupo($idGrupo);
                    } else {
                        $idGrupo = $resultGrupo->getId();
                    }
                    foreach ($controllers as $controller => $actions) {

                        $arrayPermissao = [
                            'controller' => $controller,
                            'actions' => implode(',', $actions)
                        ];
                        $resultPermissao = $this->getRepository('Permissao')->salvar($arrayPermissao);

                        if ($resultPermissao) {
                            $arrayPermissaoGrupo = [
                                'permissao' => $resultPermissao->getId(),
                                'grupo' => $resultGrupo->getId(),
                            ];

                            $resultPermissaoGrupo = $this->getRepository('PermissaoGrupo')->salvar($arrayPermissaoGrupo);

                            if ($resultPermissaoGrupo) {
                                $mensagem = array(
                                    'tipo' => 'success',
                                    'texto' => 'Informações salvas com sucesso.'
                                );
                            } else {
                                $mensagem = array(
                                    'tipo' => 'danger',
                                    'texto' => 'Erro ao salvar as permissões do grupo.'
                                );
                            }
                        } else {
                            $mensagem = array(
                                'tipo' => 'danger',
                                'texto' => 'Erro ao salvar as permissões.'
                            );
                        }
                    }
                } else {
                    $mensagem = array(
                        'tipo' => 'danger',
                        'texto' => 'Erro ao salvar as informações do grupo.'
                    );
                }
            } else {
                $mensagem = array(
                    'tipo' => 'danger',
                    'texto' => 'Os campos Nome, Descrição e a concessão de ao menos um privilégio são obrigátórios.'
                );
            }
        }


        $response = $this->getResponse();
        $response->setContent(Json::encode(array(
                    'mensagem' => $mensagem,
                    //'formMensages' => $formAluno->getMessages(),
                    'idGrupo' => $idGrupo
        )));
        return $response;
    }

}
