<?php

namespace Auth\Form;

use Zend\Form\Form;

/**
 * Description of Usuarios
 *
 * @author Thalles Ferreira
 */
class Esqueci extends Form {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);

        //$this->setInputFilter(new Filter\Login());
        $this->setAttribute('method', 'post');
        //$this->setLabelOptions(['Should_create_template'=> false]);

        $tipoPessoa = new \Zend\Form\Element\Select('tipoPessoa');
        $tipoPessoa->setLabel('Tipo de Pessoa')
                ->setLabelAttributes(['class' => 'labeltexto'])
                ->setAttribute('options', ['CPF' => 'Pessoa Fisica', 'CNPJ' => 'Pessoa Jurídica'])
                ->setAttribute('class', 'campo-texto');
        //$this->add($tipoPessoa);

        $cpf = new \Zend\Form\Element\Text('cpf');
        $cpf->setLabel('CPF *')
                ->setLabelAttributes(['class' => 'labeltexto'])
                ->setAttribute('placeholder', 'Informe a senha')
                ->setAttribute('class', 'campo-texto');
        //$this->add($cpf);
        //Adição do campo e-mail
        $apelido = new \Zend\Form\Element\Email('email');
        $apelido->setLabel('E-mail *')
                ->setLabelAttributes(['class' => 'labeltexto'])
                ->setAttribute('placeholder', 'Informe o e-mail')
                ->setAttribute('class', 'campo-texto');
        $this->add($apelido);
    }

}
