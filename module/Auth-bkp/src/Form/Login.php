<?php

namespace Security\Form;

use Zend\Form\Form;

/**
 * Description of Usuarios 
 *
 * @author Letivo
 */
class Login extends Form {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);

        //$this->setInputFilter(new Filter\Login());
        $this->setAttribute('method', 'post');
        $this->setLabelOptions(['Should_create_template' => false]);


        //Adição do campo Nome
        $email = new \Zend\Form\Element\Text('email');
        $email->setLabel('Email')
                ->setLabelAttributes(['class' => 'label-texto'])
                ->setAttributes(['placeholder' => 'Login', 'class' => 'form-control', 'aria-describedby' => "basic-addon1"]);
        $this->add($email);

        $senha = new \Zend\Form\Element\Password('senha');
        $senha->setLabel('Senha')
                ->setLabelAttributes(['class' => 'label-texto'])
                ->setAttributes(['placeholder' => 'Senha', 'class' => 'form-control', 'aria-describedby' => "basic-addon1"]);
        $this->add($senha);

        $relembrar = new \Zend\Form\Element\Checkbox('relembrar');
        $relembrar->setLabel('Relembrar-me')
                ->setLabelAttributes(['class' => 'checkbox-login'])
                ->setCheckedValue('SIM')
                ->setUncheckedValue("NAO");


        $this->add($relembrar);


//        $submit = new \Zend\Form\Element\Submit('submit');
//        $submit->setAttribute('value', ' Adicionar usuário ');
//        $this->add($submit);
//
    }

}
