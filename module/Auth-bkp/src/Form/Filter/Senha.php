<?php

namespace Auth\Form\Filter;

use Zend\InputFilter\InputFilter;

/**
 * Description of UsuariosFilter
 *
 * @author Letivo
 */
class Senha extends InputFilter {

    function __construct() {
        $this->add([
            'name' => 'senhaConfirma',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'Identical',
                    'options' => [
                        'token' => 'senha',
                        'message' => "A nova senha não confere com a sua confirmação.",
                    ]
                ]
            ]
        ]);
    }

}
