<?php

namespace Auth\Adapter;

use Zend\Authentication\Adapter\AdapterInterface,
    Zend\Authentication\Result;
use Doctrine\ORM\EntityManager;

class Adapter implements AdapterInterface {

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     *
     */
    protected $email;
    protected $senha;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    public function getEm() {
        return $this->em;
    }

    public function setEm($em) {
        $this->em = $em;
        return $this;
    }

    function getEmail() {
        return $this->apelido;
    }

    function getSenha() {
        return $this->senha;
    }

    function setEmail($email) {
        $this->apelido = $email;
        return $this;
    }

    function setSenha($senha) {
        $this->senha = $senha;
        return $this;
    }

    public function authenticate() {
        $repository = $this->em->getRepository('App\Entity\Pessoa');
        $usuario = $repository->findOneByEmail($this->getEmail());

        if ($usuario and ( $usuario->getSenha() == $this->getSenha())) {
            return new Result(Result::SUCCESS, array('usuario' => $usuario), array('ok'));
        } else {
            return new Result(Result::FAILURE_CREDENTIAL_INVALID, null, array());
        }
    }

}

?>