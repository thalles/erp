<?php

namespace Auth\Plugin;

/**
 * Description of MyPlugin
 *
 * @author Thalles Ferreira
 */
use Zend\Mvc\Controller\Plugin\AbstractPlugin,
    Zend\Session\Container as SessionContainer,
    Zend\Permissions\Acl\Acl as Acl,
    Zend\Permissions\Acl\Role\GenericRole as Role,
    Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\Mvc\MvcEvent,
    Zend\ServiceManager\ServiceManager;

//use Zend\Session\Container as Container;

class ControlAccess extends AbstractPlugin {

    protected $session;
    protected $e;

    public function getEm() {
        $em = $this->e->getApplication()->getServiceManager()->get('Doctrine\ORM\EntityManager');
        return $em;
    }

    private function getSession() {
        if (!$this->session) {
            $this->session = new SessionContainer();
        }
        return $this->session;
    }

    public function getModuleName(MvcEvent $e) {
        $controllerClass = get_class($e->getTarget());
        return strtolower(substr($controllerClass, 0, strpos($controllerClass, '\\')));
    }

    public function getControllerName2(ServiceManager $sm) {
        $params = $sm->get('router')->match($sm->get('request'))->getParams();
        if (strripos($params['controller'], '\\')) {
            $controllerName = substr($params['controller'], strripos($params['controller'], '\\') + 1, strlen($params['controller']));
        } else {
            $controllerName = $params['controller'];
        }
        return strtolower($controllerName);
    }

    public function getControllerName(MvcEvent $e) {
        $params = get_class($e->getTarget());

        if (strripos($params, '\\')) {
            $controllerName = substr($params, strripos($params, '\\') + 1, strlen($params));
        } else {
            $controllerName = $params;
        }
        $controllerName = strtolower($controllerName);
        return str_replace('controller', '', $controllerName);
    }

    public function getActionName2(ServiceManager $sm) {
        $params = $sm->get('router')->match($sm->get('request'))->getParams();

        return strtolower($params['action']);
    }

    public function getActionName(MvcEvent $e) {
        return strtolower($e->getRouteMatch()->getParam('action'));
    }

    private function isAuthentication(MvcEvent $e) {

        $controllerClass = get_class($e->getTarget());

        $authService = new \Zend\Authentication\AuthenticationService();

        $sessionStorage = new SessionStorage('Usuario');

        $authService->setStorage($sessionStorage);


        if (isset($authService->getIdentity()->relembrar) && $authService->getIdentity()->relembrar === true) {
            //$sessionStorage->getSession()->getManager()->rememberMe('1296000');
            //echo 'relembrar';
        } else {
            //$sessionStorage->getSession()->getManager()->rememberMe(1800);
            //echo 'Não relembrar';
        }

        $authService->setStorage($sessionStorage);

        if (!$authService->hasIdentity() and ( $controllerClass != 'Auth\Controller\IndexController')) {
            return false;
        } else {
            return $authService;
        }
    }

    public function allow(MvcEvent $e, $grupo, $resource, $privileges) {
        $acl = new Acl();
        $role = new Role($grupo);
        $acl->addRole($role);
        $acl->addResource($resource);
        $acl->allow($role, $resource, $privileges);

        $e->getViewModel()->acl = $acl;
    }

    public function setAutorization(MvcEvent $e) {

        $this->e = $e;
        $sm = $e->getApplication()->getServiceManager();

        $moduleName = $this->getModuleName($e);
        $controllerName = $this->getControllerName($e);

        $actionName = $this->getActionName($e);

        $authService = $this->isAuthentication($e);

        if ($authService) {
            //para verificação;
            //return true;
            if ($authService->hasIdentity()) {
                $aclContainer = new SessionContainer('acl');

                if (!$aclContainer->acl) {

                    $acl = new Acl();

                    $acl->addResource('application/index');
                    $acl->addRole(new Role('usuario'));
                    $acl->allow('usuario', 'application/index', ['index']);
                    $pessoa = $this->getEm()->getRepository('App\Entity\Pessoa')->find($authService->getIdentity()->getId());
                    $aclContainer->grupos = [];
                    foreach ($pessoa->getPessoaGrupo() as $pessoaGrupo) {
                        $grupo = strtolower($pessoaGrupo->getGrupo()->getNome());
                        $aclContainer->grupos[] = $grupo;
                        foreach ($pessoaGrupo->getGrupo()->getPermissaoGrupo() as $permissaoGrupo) {
                            $resource = strtolower($permissaoGrupo->getPermissao()->getController());
                            $privileges = explode(',', strtolower($permissaoGrupo->getPermissao()->getActions()));
                            if ($acl->hasResource($resource)) {
                                $acl->allow('usuario', $resource, $privileges);
                            } else {
                                $acl->addResource($resource);
                                $acl->allow('usuario', $resource, $privileges);
                            }
                        }
                    }
                    $aclContainer->acl = $acl;
                } else {
                    $acl = $aclContainer->acl;
                }

                $resourceName = $moduleName . '/' . $controllerName;

                $session = $this->getSession();
                if ($acl->hasResource($resourceName)) {

                    if (!$acl->isAllowed('usuario', $resourceName, $actionName)) {
                        $session->mensagem = [
                            'tipo' => 'danger',
                            'texto' => '<strong>Erro!</strong> Você não tem permissão para executar essa ação. Duvida! Consulte o <strong> administrador.</strong> '
                            . '  <button class="btn pull-right" style="margin-top: -9px;" onclick="window.history.back();">Voltar</button>',
                        ];
                        return $e->getTarget()->redirect()->toRoute('home');
                    }
                } else {

                    $session->mensagem = [
                        'tipo' => 'danger',
                        'texto' => '<strong>Erro!</strong> Você não tem permissão para executar essa ação. Duvida! Consulte o <strong> administrador</strong>'
                        . '<button class="btn pull-right" style="margin-top: -9px;" onclick="window.history.back();">Voltar</button>',
                    ];

                    return $e->getTarget()->plugin('redirect')->toRoute('home');
                }
            }
        } else {

            return $e->getTarget()->redirect()->toRoute('auth-index', array('action' => 'index'));
        }
    }

    public function pre($var) {
        echo '<pre>';
        print_r($var);
        echo '</pre>';
    }

}
