<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Manage\Controller;

use App\Form\Products\Modelo;
use Doctrine\DBAL\DBALException;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Description of GestorController
 *
 * @author Thalles Ferreira
 */
class ModelController extends AbstractController {

    public function listAction() {
        $list = $this->getRepository('Modelo')->findBy([], []);
        return new ViewModel(['rota' => $this->getRota(), 'lista' => $list]);
    }

    public function lista1Action() {

        $list = $this->getRepository('Modelo')->findBy([], []);
        $array = [];
        foreach ($list as $atual):
            $filha = $atual->getNome();
            $array[] = ['id' => $atual->getId(), 'name' => $filha];
            while (is_object($atual->getModeloPai())):
                $atual = $atual->getModeloPai();
                $filha = $atual->getNome() . ' - ' . $filha;
                $array = ['id' => $atual->getId(), 'name' => $filha];
            endwhile;
        endforeach;
        return new JsonModel($array);
    }

    public function listaAction() {
        $marca = $this->params()->fromRoute('marca', null);
        $list = $this->getRepository('Modelo')->findBy(['marca' => $marca], ['nome' => 'ASC']);

        $array = [];
        foreach ($list as $atual):
            $filha = $atual->getNome(); //. ' - ' . $atual->getNumeroNcm() . ' - ' . $atual->getDescricao()
            $id = $atual->getId();
            $compl = ";";
            $compl .= strlen($atual->getPortas()) > 0 ? " " . $atual->getPortas() . " pts" : "";
            $compl .= strlen($atual->getGavetas()) > 0 ? " " . $atual->getGavetas() . " gvts" : "";
            while (is_object($atual->getModeloPai())):
                $atual = $atual->getModeloPai();
                $filha = $atual->getNome() . ' - ' . $filha;
            endwhile;
            $array[] = ['id' => $id, 'name' => $filha . $compl];
        endforeach;
        return new JsonModel($array);
    }

    public function addAction() {
        $formCategory = new Modelo();
        $arrayPost = $this->getRequest()->getPost()->toArray();
        $formCategory->setData($arrayPost);
        if ($formCategory->isValid()) {
            try {
                $repositoryCategory = $this->getRepository('Modelo');
                if ($arrayPost['modeloPai'] < 1) {
                    unset($arrayPost['modeloPai']);
                }
                $entityCategory = $repositoryCategory->salvar($arrayPost);
                return new JsonModel(['id' => $entityCategory->getId()]);
            } catch (DBALException $exc) {
                echo $exc;
                return new JsonModel(['mensage' => 'Erro ao salvar dados.']);
            }
        }
        return new JsonModel(['mensage' => 'Erro durante o processamento']);
    }

    public function removeAction() {
        return new ViewModel(['rota' => $this->getRota()]);
    }

}
