<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Manage\Controller;

use App\Form\Persons\Endereco as formEndereco;
use App\Form\Persons\ContatoFisica as formContatoFisica;
use App\Form\Persons\ContatoJuridica as formContatoJuridica;
use App\Form\Persons\SegurancaFisica as formAcessoFisica;
use App\Form\Persons\SegurancaJuridica as formAcessoJuridica;
use Zend\View\Model\JsonModel;

//use App\Form\Fisica as formPessoa;
/**
 * Description of EmployessController
 *
 * @author Midiauai
 */
class PersonController extends AbstractController {

    /**
     * Nomes comuns a todos os formulários
     */
    protected $formFisica;
    protected $formJuridica;
    private $formEndereco;
    private $formContatoFisica;
    private $formContatoJuridica;
    private $formAcessoFisica;
    private $formAcessoJuridica;

    /**
     * Formulários que são prenchidos automaticamente, os outros vão ser definidos na classe filha
     *  */
    public function __construct() {
        $this->formEndereco = new formEndereco();
        $this->formContatoFisica = new formContatoFisica();
        $this->formContatoJuridica = new formContatoJuridica();
        $this->formAcessoFisica = new formAcessoFisica();
        $this->formAcessoJuridica = new formAcessoJuridica();
    }

    /**
     * Ação de excluir para todas as pessoas.
     */
    public function deleteAction() {
        $id = $this->params()->fromRoute('id', null);
        try {
            $this->getRepository('Fisica')->delete($id);
            $this->getRepository('Juridica')->delete($id);
            $this->getRepository('Pessoa')->delete($id);
        } catch (\Doctrine\DBAL\DBALException $ex) {

        } catch (\Zend\Http\Header\Exception $ex) {
            $this->redirect()->toUrl('/');
        }

        $this->redirect()->toUrl($this->getReferer());
    }

    /**
     *
     * @param type $dados
     * @return type Array
     * Salva exclusivamente pessoas, deve-se preencher os dados e se for um post char esta função
     */
    public function save($dados) {
        $forms = $dados['form'];
        $arrayPost = $this->getRequest()->getPost()->toArray();
        $pessoa = $dados['repositoryPessoa']->salvar($arrayPost);
        $arrayPost['pessoa'] = $pessoa->getId();
        //unset($arrayPost['id']);
        $tipo = $dados['repositoryTipo']->salvar($arrayPost);
        $forms['formPessoa']->setData($tipo->toArray());
        $forms['pessoa'] = $pessoa;
        return $forms;
    }

    /**
     *
     * @param type $tipo
     * @return type Array
     * Ao fazer as escolhas da pessoa física ou jurídica, através dos formulários,
     * invoca-se este método para o correto e necessário preenchimento.
     */
    public function preenche($tipo = 'Fisica') {
        $array = $this->loadForms($tipo);
        $array['seguranca'] = [];
        $arrayRetorno['repositoryPessoa'] = $this->getRepository('Pessoa');
        $arrayRetorno['repositoryTipo'] = $this->getRepository($tipo);
        $id = $this->params()->fromRoute('id', false);
        $arrayRetorno['pessoa'] = [];
        if ($id) {
            $fisica = $arrayRetorno['tipo'] = $arrayRetorno['repositoryTipo']->find($id);
            $pessoa = $arrayRetorno['pessoa'] = $arrayRetorno['repositoryPessoa']->find($id);
            $array['formPessoa']->setData($pessoa->toArray());
            if (!is_object($fisica)) {
                $array['formPessoa']->get('pessoa')->setValue($pessoa->getId());
            } else {
                $array['formPessoa']->setData($fisica->toArray());
            }
            if (is_object($pessoa->getEndereco())) {
                //$ufId = $pessoa->getEndereco()->getUf();
                //$array['formEndereco']->get('cidade')->setAttribute('options', $array['formEndereco']->getOpcoes('Cidade', ['nome'], ['estado' => $ufId]));
                $array['formEndereco']->get('pessoa')->setValue($pessoa);
                //$array['formEndereco']->setData($pessoa->getEndereco()->toArray());
            }

            //is_object($pessoa->getContato()) ? $array['formContato']->setData($pessoa->getContato()->toArray()) : '';

            if (is_object($fisica) && is_object($fisica->getSeguranca())) {
                $array['formAcesso']->setData($fisica->getSeguranca()->toArray());
                $array['seguranca'] = $fisica->getSeguranca()->toArray();
            }
        }
        $arrayRetorno['form'] = $array;
        return $arrayRetorno;
    }

    /**
     * Retorna o conjunto de formulários para se trabalhar com qualquer pessoa.
     * @param type $tipo {Pessoa Física ou Jurídica}
     * @return type Array $array prontos para a visão
     */
    public function loadForms($tipo) {
        eval('$array["formPessoa"] = $this->form' . $tipo . ';');
        eval('$array["formAcesso"] = $this->formAcesso' . $tipo . ';');
        eval('$array["formContato"] = $this->formContato' . $tipo . ';');
        $array['formEndereco'] = $this->formEndereco;
        $array['rota'] = $this->getRota();
        return $array;
    }

}
