<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Manage\Controller;

use App\Form\Persons\Contato;
use Doctrine\DBAL\DBALException;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Description of GestorController
 *
 * @author Thalles Ferreira
 */
class ContactController extends AbstractController {

    public function listAction() {
        return new ViewModel(['rota' => $this->getRota()]);
    }

    public function addAction() {
        $formContact = new \App\Form\Persons\ContatoFisica();
        $formContact->setData($this->getRequest()->getPost());

        if ($formContact->isValid()) {
            try {
                $repositoryContact = $this->getRepository('Contato');
                $entityContact = $repositoryContact->salvar($formContact->getData());
                return new JsonModel($entityContact->toArray());
            } catch (DBALException $exc) {
                return new JsonModel(['mensage' => 'Erro ao salvar dados.', 'error' => $exc]);
            }
        }
        return new JsonModel(['mensage' => 'Preencha o formulário corretamente.']);
    }

    public function deleteAction() {
        $id = $this->fromPost('id', false);
        if (!$id) {
            return new JsonModel(['mensagem' => 'Parâmetro de exclusão inválido.']);
        }
        $endereco = $this->getRepository('Contato')->delete($id);
        if ($endereco) {
            return new JsonModel([
                'mensagem' => 'O contato foi excluído.',
                'excluido' => 1
            ]);
        }
        return new JsonModel([]);
    }

    public function removeAction() {
        return new ViewModel(['rota' => $this->getRota()]);
    }

}
