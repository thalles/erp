<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Manage\Controller;

use App\Form\UnidadeMedida;
use Doctrine\DBAL\DBALException;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Description of GestorController
 *
 * @author Thalles Ferreira
 */
class UnitMeasureController extends AbstractController {

    public function listAction() {
        $list = $this->getRepository('UnidadeMedida')->findBy([], []);
        return new ViewModel(['rota' => $this->getRota(), 'lista' => $list]);
    }

    public function listaAction() {

        $list = $this->getRepository('UnidadeMedida')->findBy([], []);
        $array = [];
        foreach ($list as $atual):
            $filha = $atual->getUnidade() . ' - ' . $atual->getDescricao();
            $array[] = ['id' => $atual->getId(), 'name' => $filha];
        endforeach;
        return new JsonModel($array);
    }

    public function addAction() {
        $formUnidade = new UnidadeMedida();
        $arrayPost = $this->getRequest()->getPost()->toArray();
        $formUnidade->setData($arrayPost);
        if ($formUnidade->isValid()) {
            try {
                $entityUnidade = $this->getRepository("UnidadeMedida")->salvar($arrayPost);
                return new JsonModel(['id' => $entityUnidade->getId()]);
            } catch (DBALException $exc) {
                echo $exc;
                return new JsonModel(['mensage' => 'Erro ao salvar dados.']);
            }
        }
        return new JsonModel(['mensage' => 'Erro durante o processamento']);
    }

    public function removeAction() {
        return new ViewModel(['rota' => $this->getRota()]);
    }

}
