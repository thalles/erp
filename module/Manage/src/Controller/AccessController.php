<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Manage\Controller;

use App\Form\Persons\SegurancaFisica;
use Doctrine\DBAL\DBALException;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Description of GestorController
 *
 * @author Thalles Ferreira
 */
class AccessController extends AbstractController {

    public function listAction() {
        return new ViewModel(['rota' => $this->getRota()]);
    }

    public function addAction() {
        $formSecurity = new SegurancaFisica();
        $formSecurity->setData($this->fromPost());

        if ($formSecurity->isValid()) {
            $dataSecurity = $this->getRequest()->getPost()->toArray();
            $dataSecurity['senha'] = substr(md5(microtime() . time() . date('dmyHis')), 5, 8);
            try {
                $repositorySecurity = $this->getRepository('Seguranca');
                $entitySecurity = $repositorySecurity->salvar($dataSecurity);
                $this->sendPassword([
                    'email'   => $this->fromPost('email'),
                    'apelido' => $this->fromPost('apelido'),
                    'senha'   => $dataSecurity['senha']
                ]);
                return new JsonModel($entitySecurity->toArray());
            } catch (DBALException $exc) {
                echo $exc;
                return new JsonModel(['message' => 'Erro ao salvar dados.']);
            }
        }
        return new JsonModel(['message' => 'Erro durante o processamento']);
    }

    public function sendPassword2() {
        $mail = new \Zend\Mail\Message;
        $mail->setBody('This is the text of the email.');
        $mail->setFrom('Freeaqingme@example.org', "Sender's name");
        $mail->addTo('thallesvinicius@gmail.com', 'Name of recipient');
        $mail->setSubject('TestSubject');

        $transport = new \Zend\Mail\Transport\Smtp();
        $options = new \Zend\Mail\Transport\SmtpOptions([
            'name'              => 'gmail',
            'host'              => 'smtp.gmail.com',
            'port'              => 465,
            // Notice port change for TLS is 587
            'connection_class'  => 'plain',
            'connection_config' => [
                'username' => 'thallesvinicius@gmail.com',
                'password' => 'ferreira09',
                'ssl'      => 'ssl',
            ],
        ]);
        $transport->setOptions($options);
        $transport->send($mail);
    }

    public function sendPassword($data) {

        try {
            $mail = new \App\Plugin\Mailer();
            $mail->addAddress($data['email'], $data['apelido']);
            $mail->Subject = 'Usuário Criado' . date('d/m/Y H:i');
            $mail->Body = "Olá {$data['apelido']}!<br>"
                    . "Um novo usuário foi criado pra você no ERP Brasão, abaixo dados de acesso:<br>"
                    . "Login: {$data['apelido']} ou {$data['email']}<br>"
                    . "Senha: {$data['senha']}";

            return $mail->send();
        } catch (Exception $exc) {
            return false;
        }
    }

    public function removeAction() {
        return new ViewModel(['rota' => $this->getRota()]);
    }

}
