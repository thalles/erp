<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Manage\Controller;

use App\Form\Persons\FornecedorFisica as formFisica;
use App\Form\Persons\FornecedorJuridica as formJuridica;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

//use App\Form\Fisica as formPessoa;filho@123
/**
 * Description of EmployessController
 *
 * @author Midiauai
 */
class ProviderController extends PersonController {

    public function __construct() {
        $this->formFisica = new formFisica();
        $this->formJuridica = new formJuridica();
        parent::__construct();
    }

    public function listAction() {
        $repositoryPessoa = $this->getRepository('Pessoa');
        $listClient = $repositoryPessoa->findBy(['padrao' => '3'], ['nome' => 'ASC']);
        return new ViewModel(['list' => $listClient, 'rota' => $this->getRota()]);
    }

    public function listaAction() {
        $list = $this->getRepository('Pessoa')->findBy(['padrao' => '3'], ['nome' => 'ASC']);
//        print_r($list[0]->toArray());
//        print_r($list[1]->toArray());
        $fornecedores = [];
        foreach ($list as $fornecedor) {
            $fornecedores[] = ['id' => $fornecedor->getId(), 'name' => $fornecedor->getNome()];
        }
        return new JsonModel($fornecedores);
    }

    public function addAction() {
        return new ViewModel([]);
    }

    public function addFisicaAction() {
        $dados = $this->preenche();
        $forms = $dados['form'];
        $forms['pessoa'] = $dados['pessoa'];
        $request = $this->getRequest();
        if ($request->isPost()) {
            $forms = $this->save($dados);
            $pessoa = $forms['pessoa'];
            return new JsonModel(['id' => $pessoa->getId()]);
        }
        return new ViewModel($forms);
    }

    public function addJuridicaAction() {
        $dados = $this->preenche('Juridica');
        $forms = $dados['form'];
        $forms['pessoa'] = $dados['pessoa'];
        $request = $this->getRequest();
        if ($request->isPost()) {
            $forms = $this->save($dados);
            $pessoa = $forms['pessoa'];
            return new JsonModel(['id' => $pessoa->getId()]);
        }
        return new ViewModel($forms);
    }

    public function removeAction() {
        return new ViewModel();
    }

}
