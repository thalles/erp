<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Manage\Controller;

use App\Form\Products\Marca;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;


/**
 * Description of GestorController
 *
 * @author Thalles Ferreira
 */
class BrandController extends AbstractController {

    public function listAction() {
        $listBrand = $this->getRepository('Marca')->findBy([], []);
        return new ViewModel(['rota' => $this->getRota(), 'lista' => $listBrand]);
    }

    public function listaAction() {
        $listBrand = $this->getRepository('Marca')->findBy([], []);
        $array = [];
        foreach ($listBrand as $brand):
            $array[] = ['id' => $brand->getId(), 'name' => $brand->getNome()];
        endforeach;
        return new JsonModel($array);
    }

    public function addAction() {
        $formBrand = new Marca();
        $formBrand->setData($this->getRequest()->getPost());

        if ($formBrand->isValid()) {
            try {
                $repositoryBrand = $this->getRepository('Marca');
                $entityBrand = $repositoryBrand->salvar($this->getRequest()->getPost()->toArray());
                return new JsonModel(['id' => $entityBrand->getId()]);
            } catch (DBALException $exc) {
                echo $exc;
                return new JsonModel(['mensage' => 'Erro ao salvar dados.']);
            }
        }
        return new JsonModel(['mensage' => 'Erro durante o processamento']);
    }

    public function removeAction() {
        return new ViewModel(['rota' => $this->getRota()]);
    }

}
