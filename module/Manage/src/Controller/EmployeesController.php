<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Manage\Controller;

use App\Form\Persons\Funcionario as formFisica;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

//use App\Form\Fisica as formPessoa;
/**
 * Description of EmployessController
 *
 * @author Midiauai
 */
class EmployeesController extends PersonController {

    public function __construct() {
        $this->formFisica = new formFisica();
        parent::__construct();
    }

    public function listAction() {
        $repositoryFisica = $this->getRepository('Pessoa');
        $listFuncionario = $repositoryFisica->findBy(['padrao' => '2'], ['nome' => 'ASC']);
//        echo count($listFuncionario);
//        exit;
        return new ViewModel(['list' => $listFuncionario, 'rota' => $this->getRota()]);
    }

    public function addAction() {
        $dados = $this->preenche();
        $forms = $dados['form'];
        $forms['pessoa'] = $dados['pessoa'];


        if ($this->isPost()) {
            $forms = $this->save($dados);
            $pessoa = $forms['pessoa'];
            return new JsonModel(['id' => $pessoa->getId()]);
        }

        return new ViewModel($forms);
    }

    public function listaAction() {
        $collEmployees = $this->getRepository('Pessoa')->findBy(['padrao' => '2'], ['nome' => 'ASC']);
        $arrayEmployees = [];
        foreach ($collEmployees as $employe) {
            $arrayEmployees[] = [
                'id'   => $employe->getId(),
                'name' => $employe->getNome(),
            ];
        }
        return new JsonModel($arrayEmployees);
    }

    public function deleteAction() {
        $id = $this->params('id', false);
        if (!$id) {
            $this->setMessageError('Erro ao desativar usuário!');
            return $this->redirect()->toUrl($this->getReferer());
        }
        $this->getRepository('Pessoa')->salvar(['id' => $id, 'status' => '0']);
        $this->setMessageSuccess('Usuário inativado com sucesso!');
        return $this->redirect()->toUrl($this->getReferer());
    }

    public function activeAction() {
        $id = $this->params('id', false);
        if (!$id) {
            $this->setMessageError('Erro ao ativar usuário!');
            return $this->redirect()->toUrl($this->getReferer());
        }
        $this->getRepository('Pessoa')->salvar(['id' => $id, 'status' => '1']);
        $this->setMessageSuccess('Usuário ativado com sucesso!');
        return $this->redirect()->toUrl($this->getReferer());
    }

}
