<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Manage\Controller;

use App\Form\Products\Categoria;
use Doctrine\DBAL\DBALException;

use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Description of GestorController
 *
 * @author Thalles Ferreira
 */
class CategoryController extends AbstractController {

    public function listAction() {
        $list = $this->getRepository('Categoria')->findBy([], []);
        return new ViewModel(['rota' => $this->getRota(), 'lista' => $list]);
    }

    public function lista1Action() {

        $list = $this->getRepository('Categoria')->findBy([], []);
        $array = [];
        foreach ($list as $atual): 
            $filha = $atual->getNome() ; //. ' - ' . $atual->getNumeroNcm() . ' - ' . $atual->getDescricao()
            $array[] = ['id' => $atual->getId(), 'name' => $filha];
//            while (is_object($atual->getCategoriaPai())):
//                $atual = $atual->getCategoriaPai();
//                $filha = $atual->getNome() . ' - ' . $filha;
//                $array = ['id' => $atual->getId(),  'name' => $filha];
//            endwhile;
        endforeach;
        return new JsonModel($array);
    }
    public function listaAction() {

        $list = $this->getRepository('Categoria')->findBy([], []);
        $array = [];
        foreach ($list as $atual): 
            $filha = $atual->getNome() ; //. ' - ' . $atual->getNumeroNcm() . ' - ' . $atual->getDescricao()
        $id =    $atual->getId();
        while (is_object($atual->getCategoriaPai())):
                $atual = $atual->getCategoriaPai();
                $filha = $atual->getNome() . ' - ' . $filha;
            endwhile;
            $array[] = ['id' => $id,  'name' => $filha];
            
        endforeach;
//        echo "<pre>";
//        print_r($array);
//        exit;
        return new JsonModel($array);
    }

    public function addAction() {

        $formCategory = new Categoria();
        $formCategory->setData($this->getRequest()->getPost());
//        return new JsonModel(['mensage' => 'Erro durante o processamento']);
        if ($formCategory->isValid()) {
            try {
                $repositoryCategory = $this->getRepository('Categoria');
                $arrayPost = $this->getRequest()->getPost()->toArray();
                if ($arrayPost['categoriaPai'] < 1) {
                    unset($arrayPost['categoriaPai']);
                }
                $entityCategory = $repositoryCategory->salvar($arrayPost);
//                return new JsonModel($arrayPost);
                return new JsonModel(['id' => $entityCategory->getId()]);
            } catch (DBALException $exc) {
                echo $exc;
                return new JsonModel(['mensage' => 'Erro ao salvar dados.']);
            }
        }
        return new JsonModel(['mensage' => 'Erro durante o processamento']);
    }

    public function removeAction() {
        return new ViewModel(['rota' => $this->getRota()]);
    }

}
