<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Manage\Controller;

use App\Form\Products\Produto;
use App\Form\Products\Mercadoria;
use App\Form\Products\Acabamento;
use App\Form\Products\Categoria;
use App\Form\Products\Cor;
use App\Form\Products\Marca;
use App\Form\Products\Modelo;
use App\Form\Imagem;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

/**
 * Description of GestorController
 *
 * @author Thalles Ferreira
 */
class ProductsController extends AbstractController {

    public function listAction() {
        $list = $this->getRepository("Produto")->findBy([], []);
        return new ViewModel([
            'list' => $list,
            'rota' => $this->getRota()]);
    }

    public function addAction() {

        $formProduto = new Produto();

        $formMerchandise = new Mercadoria();
//        $formFinishing = new Acabamento();
        $formCategory = new Categoria;
//        $formColor = new Cor();
        $formBrand = new Marca();
        //$formModel = new Modelo();
//        echo "OKKK ";
//        exit;
        $formImage = new Imagem();

        $request = $this->getRequest();
        if ($id = $this->params()->fromRoute('id', null)) {
            $produto = $this->getRepository("Produto")->find($id);
            $formProduto->setData($produto->toArray());
        } else if ($clone = $this->params()->fromRoute('clone', null)) {
            $produto = $this->getRepository("Produto")->find($clone);
            $produtoArray = $produto->toArray();
            unset($produtoArray['id']);
            $formProduto->setData($produtoArray);
        }
        if ($request->isPost()) {
            $formProduto->setData($request->getPost()->toArray());
            if ($formProduto->isValid()) {
                $novo = $this->getRepository("Produto")->salvar($request->getPost()->toArray());
                $this->redirect()->toRoute($this->getRota(), ['action' => 'add', 'id' => $novo->getId()]);
            } else {
                $this->setMessageError("Erro ai preencher o formulário!");
            }
        }

        return new ViewModel([
            'rota' => $this->getRota(),
            'formProduto' => $formProduto,
//            'formAcabamento' => $formFinishing,
//            'formCor' => $formColor,
            'formMarca' => $formBrand,
//            'formModelo' => $formModel,
            'formImagem' => $formImage,
            'formMercadoria' => $formMerchandise,
            'formCategoria' => $formCategory,
        ]);
    }

    public function listaAction() {

        $listProdutos = $this->getRepository('Produto')->findAll();
        $produtos = [];
        foreach ($listProdutos as $produto) {

            $produtos[] = [
                'id' => $produto->getId(),
                'name' => $produto->getMercadoriaDescricao()
                . ' - ' . $produto->getMarcaNome()
                . ' | Dimensões: ' . $produto->getDimensoes(),
                'valor' => $produto->getValorAtual(),
                'dimensoes' => $produto->getDimensoes(),
                    //'' => $produto->get(),
                    //$produto->toArray()
            ];
        }

        return new JsonModel($produtos);
    }

    public function removeAction() {
        return new ViewModel(['rota' => $this->getRota()]);
    }

}
