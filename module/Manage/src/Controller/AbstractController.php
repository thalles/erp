<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Manage\Controller;

/**
 * Description of AbstractController
 *
 * @author Thalles Ferreira
 */
class AbstractController extends \App\Controller\AbstractController {

    public function adicionar($entity, $form, $arrayPost = []) {
        if (count($arrayPost) < 1) {
            $arrayPost = $this->getRequest()->getPost()->toArray();
        }
        if (isset($arrayPost[lcfirst($entity) . 'Pai'])) {
            if ($arrayPost[lcfirst($entity) . 'Pai'] < 1) {
                unset($arrayPost[lcfirst($entity) . 'Pai']);
            }
        }
        $form->setData($this->getRequest()->getPost()->toArray());
        if ($form->isValid()) {
            try {
                $entity = $this->getRepository(ucfirst($entity))->salvar($arrayPost);
                return $entity->toArray();
            } catch (DBALException $exc) {
                return ['mensage' => 'Erro ao salvar dados.'];
            }
        }
        return ['mensage' => 'Erro durante o processamento'];
    }

}
