<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Manage\Controller;

use App\Form\Persons\ClienteFisica as formFisica;
use App\Form\Persons\ClienteJuridica as formJuridica;
use App\Form\Products\Marca;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

//use App\Form\Fisica as formPessoa;filho@123
/**
 * Description of EmployessController
 *
 * @author Midiauai
 */
class ClientController extends PersonController {

    public function __construct() {
        $this->formFisica = new formFisica();
        $this->formJuridica = new formJuridica();
        parent::__construct();
    }

    public function listAction() {
        $repositoryPessoa = $this->getRepository('Pessoa');
        $listClient = $repositoryPessoa->findBy(['padrao' => '1'], ['nome' => 'ASC']);
        return new ViewModel(['list' => $listClient, 'rota' => $this->getRota()]);
    }

    public function listaAction() {
        $collClients = $this->getRepository('Pessoa')->findBy(['padrao' => '1'], ['nome' => 'ASC']);
        $arrayClients = [];
        foreach ($collClients as $client) {
            $arrayClients[] = [
                'id' => $client->getId(),
                'name' => $client->getNome(),
            ];
        }
        return new JsonModel($arrayClients);
    }

    public function addAction() {
        return new ViewModel([]);
    }

    public function addFisicaAction() {
        $dados = $this->preenche();
        $forms = $dados['form'];
        $forms['pessoa'] = $dados['pessoa'];
        $request = $this->getRequest();
        if ($request->isPost()) {
            $forms = $this->save($dados);
            $pessoa = $forms['pessoa'];
            return new JsonModel(['id' => $pessoa->getId()]);
        }
        return new ViewModel($forms);
    }

    public function addJuridicaAction() {
        $dados = $this->preenche('Juridica');
        $forms = $dados['form'];
        $forms['pessoa'] = $dados['pessoa'];
        $request = $this->getRequest();
        if ($request->isPost()) {
            $forms = $this->save($dados);
            $pessoa = $forms['pessoa'];
            return new JsonModel(['id' => $pessoa->getId()]);
        }
        return new ViewModel($forms);
    }

    public function removeAction() {
        return new ViewModel();
    }

    public function testeAction() {
        $formBrand = new Marca();
        return new ViewModel([
            'formBrand' => $formBrand
        ]);
    }

}
