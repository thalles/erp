<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Manage\Controller;

use App\Form\Persons\Endereco;
use Doctrine\DBAL\DBALException;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Description of GestorController
 *
 * @author Thalles Ferreira
 */
class AdressController extends AbstractController {

    public function listAction() {
        return new ViewModel(['rota' => $this->getRota()]);
    }

    public function cidadesAction() {
        $id = $this->params()->fromPost('id');
        $cidades = $this->getRepository("Cidade")->getArrayOptions($id);
        return new JsonModel($cidades);
    }

    public function enderecosAction() {
        $id = $this->params()->fromPost('id');
        $enderecos = $this->getRepository("Pessoa")->getArrayEnderecos($id);
        return new JsonModel($enderecos);
    }

    public function addAction() {
        $formAdress = new Endereco();
        $formAdress->setData($this->getRequest()->getPost());

        if ($formAdress->isValid()) {
            try {
                $repositoryAdress = $this->getRepository('Endereco');
                $entityAdress = $repositoryAdress->salvar($this->getRequest()->getPost()->toArray());
                return new JsonModel($entityAdress->toArray());
            } catch (DBALException $exc) {
                echo $exc;
                return new JsonModel(['mensage' => 'Erro ao salvar dados.']);
            }
        }
        return new JsonModel(['mensage' => 'Verifique se o formulário foi preenchido corretamente.']);
    }

    public function deleteAction() {
        $id = $this->fromPost('id', false);
        if (!$id) {
            return new JsonModel(['mensagem' => 'Parâmetro de exclusão inválido.']);
        }
        $endereco = $this->getRepository('Endereco')->delete($id);
        if ($endereco) {
            return new JsonModel([
                'mensagem' => 'O endereço foi excluído.',
                'excluido' => 1
            ]);
        }
        return new JsonModel([]);
    }

    public function removeAction() {
        return new ViewModel(['rota' => $this->getRota()]);
    }

}
