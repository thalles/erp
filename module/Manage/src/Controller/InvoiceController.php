<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Manage\Controller;

use App\Form\Entrada\EntradaNF;
use App\Form\Persons\TransportadorFisica AS TransportadorFisica;
use App\Form\Persons\TransportadorJuridica;
use App\Form\Persons\Endereco;
use App\Form\Entrada\MovimentoProduto;
use App\Form\Entrada\Imposto;
use App\Form\UnidadeMedida;
//use App\Form\Imagem;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

/**
 * Description of GestorController
 *
 * @author Thalles Ferreira
 */
class InvoiceController extends AbstractController {

    public function listAction() {
        $array = [];
        $list = $this->getRepository("NotaFiscal")->findBy($array, ['dataSaida' => 'desc']);
        return new ViewModel([
            'list' => $list,
            'rota' => $this->getRota()
        ]);
    }

    public function addAction() {
        $id = $this->params()->fromRoute('id', false);

        $formNotaFiscal = new EntradaNF();
        $formTranspFisica = new TransportadorFisica();
        $formTranspJuridica = new TransportadorJuridica();
        $formEndereco = new Endereco();
        $request = $this->getRequest();

        if ($request->isPost()) {
            $arrayPost = $request->getPost()->toArray();
            $notaFiscalIn = $this->getRepository("NotaFiscal")->salvar($arrayPost);
            $arrayPost['notaFiscal'] = $notaFiscalIn->getId();
            $this->getRepository("Carga")->salvar($arrayPost);
            $this->redirect()->toRoute($this->getRota(), ['action' => 'addProdutos', 'id' => $notaFiscalIn->getId()]);
        } else if ($id) {
            $notaFiscal = $this->getRepository("NotaFiscal")->find($id);
            $formNotaFiscal->setData($notaFiscal->toArray());
            $formNotaFiscal->setData($notaFiscal->getCarga()->toArray());
        }

        return new ViewModel([
            'rota' => $this->getRota(),
            'formNotaFiscal' => $formNotaFiscal,
            'formTranspFisica' => $formTranspFisica,
            'formTranspJuridica' => $formTranspJuridica,
            'formEndereco' => $formEndereco,
            'formMovimento' => new MovimentoProduto(),
        ]);
    }

    public function addProdutosAction() {
        $id = $this->params()->fromRoute('id', false);
        $produto = $this->params()->fromRoute('produto', false);

        $formMovimento = new MovimentoProduto($id);
        $formUnidade = new UnidadeMedida();
        $formImposto = new Imposto($id);

        if ($produto) {
            $movimento = $this->getRepository('MovimentoProduto')->find($produto)->toArray();
            $formMovimento->setData($movimento);
        }

        $notaFiscal = $this->getRepository("NotaFiscal")->find($id);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $arrayPost = $request->getPost()->toArray();
            if ($arrayPost['desconto'] < 1) {
                $arrayPost['desconto'] = 0;
            }
            $this->getRepository('MovimentoProduto')->salvar($arrayPost);
            return $this->redirect()->toRoute($this->getRota(), ['action' => 'addProdutos', 'id' => $id]);
        }
        $formImposto->setData($this->getRepository('NotaFiscal')->calculaImposto($id));

        return new ViewModel([
            'rota' => $this->getRota(),
            'formMovimento' => $formMovimento,
            'formUnidade' => $formUnidade,
            'formImposto' => $formImposto,
            'notaFiscal' => $notaFiscal,
        ]);
    }

    public function removeProdutoAction() {
        $id = $this->params()->fromRoute('id', false);
        $produto = $this->params()->fromRoute('produto', false);
        if ($produto) {
            $movimento = $this->getRepository('MovimentoProduto')->delete($produto);
        }
        return $this->redirect()->toRoute($this->getRota(), ['action' => 'addProdutos', 'id' => $id]);
    }

    public function impostosAction() {
        if ($this->getRequest()->isPost()) {
            $nota = $this->getRepository('NotaFiscal')->salvar($this->getRequest()->getPost()->toArray());
        }
        return $this->redirect()->toRoute($this->getRota(), ['action' => 'addProdutos', 'id' => $nota->getId()]);
    }

    public function transportadoresAction() {
        $list = $this->getRepository('Pessoa')->findBy(['padrao' => '4'], ['nome' => 'ASC']);
//        print_r($list[0]->toArray());
//        print_r($list[1]->toArray());
        $transportadores = [];
        foreach ($list as $transportador) {
            $fornecedores[] = ['id' => $transportador->getId(), 'name' => $transportador->getNome()];
        }
        return new JsonModel($fornecedores);
    }

    public function transportadorAction() {
        $dadosTransportador = $this->getRequest()->getPost()->toArray();
//        $cpf = $this->params()->fromPost('cpf', false);
        $pessoa = $this->getRepository("Pessoa")->salvar($dadosTransportador);
        $dadosTransportador['pessoa'] = $pessoa->getId();
        $transportador = $this->getRepository("Transportador")->salvar($dadosTransportador);
        $dadosTransportador['id'] = is_object($pessoa->getEnderecos()[0]) ? $transportador->getEnderecos()[0]->getId() : '';
        $endereco = $this->getRepository("Endereco")->salvar($dadosTransportador);
        return new JsonModel([
            'id' => $pessoa->getId(),
        ]);
    }

    public function removeAction() {
        return new ViewModel(['rota' => $this->getRota()]);
    }

}
