<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Manage\Controller;

use App\Form\Products\Categoria;
use Doctrine\DBAL\DBALException;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Description of GestorController
 *
 * @author Thalles Ferreira
 */
class CategoryController extends AbstractController {

    public function listAction() {
        $list = $this->getRepository('Categoria')->findBy([], []);
        return new ViewModel(['rota' => $this->getRota(), 'lista' => $list]);
    }

    public function lista1Action() {

        $list = $this->getRepository('Categoria')->findBy([], []);
        $array = [];
        foreach ($list as $atual):
            $filha = $atual->getNome(); //. ' - ' . $atual->getNumeroNcm() . ' - ' . $atual->getDescricao()
            $array[] = ['id' => $atual->getId(), 'name' => $filha];
//            while (is_object($atual->getCategoriaPai())):
//                $atual = $atual->getCategoriaPai();
//                $filha = $atual->getNome() . ' - ' . $filha;
//                $array = ['id' => $atual->getId(),  'name' => $filha];
//            endwhile;
        endforeach;
        return new JsonModel($array);
    }

    public function getCategoriaArray() {
        $list = $this->getRepository('Categoria')->findBy([], ['descricao' => 'ASC']);
        $array = [];
        foreach ($list as $atual):
            $array[] = ['id' => $atual->getId(), 'name' => $atual->getDescricao(), 'ncm' => $atual->getNumeroNcm()];
        endforeach;
        return $array;
    }

    public function listaAction() {
        return new JsonModel($this->getCategoriaArray());
    }

    public function addAction() {

        $formCategory = new Categoria();

        if ($this->getRequest()->isPost()) {
            $formCategory->setData($this->getRequest()->getPost());
//        return new JsonModel(['mensage' => 'Erro durante o processamento']);
            if ($formCategory->isValid()) {
                try {
                    $repositoryCategory = $this->getRepository('Categoria');
                    $arrayPost = $this->getRequest()->getPost()->toArray();
                    $entityCategory = $repositoryCategory->salvar($arrayPost);
                    return new JsonModel(['id' => $entityCategory->getId(), 'categorias' => $this->getCategoriaArray()]);
                } catch (DBALException $exc) {
                    echo $exc;
                    return new JsonModel(['mensage' => 'Erro ao salvar dados.']);
                }
            }
            return new JsonModel(['mensage' => 'Erro durante o processamento']);
        }
        return new ViewModel(['rota' => $this->getRota(), 'form' => $formCategory]);
    }

    public function removeAction() {
        return new ViewModel(['rota' => $this->getRota()]);
    }

}
