<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Manage\Controller;

use App\Form\NaturezaOperacao;
use Doctrine\DBAL\DBALException;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Description of GestorController
 * @author Thalles Ferreira
 */
class NatureOperationController extends AbstractController {

    public function addAction() {
        $formContact = new \App\Form\Persons\ContatoFisica();
        $formContact->setData($this->getRequest()->getPost());

        if ($formContact->isValid()) {
            try {
                $repositoryContact = $this->getRepository('Contato');
                $entityContact = $repositoryContact->salvar($this->getRequest()->getPost()->toArray());
                return new JsonModel(['id' => $entityContact->getId()]);
            } catch (DBALException $exc) {
                echo $exc;
                return new JsonModel(['mensage' => 'Erro ao salvar dados.']);
            }
        }
        return new JsonModel(['mensage' => 'Erro durante o processamento']);
    }

    public function listaAction() {
        $list = $this->getRepository('NaturezaOperacao')->findBy([], []);
        $array = [];
        foreach ($list as $atual):
            $filha = $atual->getCfop() . ' - ' . $atual->getDescricao() . " (" . str_replace(".", "", $atual->getCfop()) . ")";
            $array[] = ['id' => $atual->getId(), 'name' => $filha];
        endforeach;
        return new JsonModel($array);
    }

    public function removeAction() {
        return new ViewModel(['rota' => $this->getRota()]);
    }

}
