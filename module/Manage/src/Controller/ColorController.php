<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Manage\Controller;

use App\Form\Products\Cor;
use Doctrine\DBAL\DBALException;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * Description of GestorController
 *
 * @author Thalles Ferreira
 */
class ColorController extends AbstractController {

    public function listAction() {
        $list = $this->getRepository('Cor')->findBy([], []);
        return new ViewModel(['rota' => $this->getRota(), 'lista' => $list]);
    }

    public function lista1Action() {

        $list = $this->getRepository('Cor')->findBy([], []);
        $array = [];
        foreach ($list as $atual):
            $filha = $atual->getNome();
            $array[] = ['id' => $atual->getId(), 'name' => $filha];
//            while (is_object($atual->getCategoriaPai())):
//                $atual = $atual->getCategoriaPai();
//                $filha = $atual->getNome() . ' - ' . $filha;
//                $array = ['id' => $atual->getId(),  'name' => $filha];
//            endwhile;
        endforeach;
        return new JsonModel($array);
    }

    public function listaAction() {

        $list = $this->getRepository('Cor')->findBy([], []);
        $array = [];
        foreach ($list as $atual):
            $filha = $atual->getNome(); //. ' - ' . $atual->getNumeroNcm() . ' - ' . $atual->getDescricao()
            $id = $atual->getId();
            while (is_object($atual->getCorPai())):
                $atual = $atual->getCorPai();
                $filha = $atual->getNome() . ' - ' . $filha;
            endwhile;
            $array[] = ['id' => $id, 'name' => $filha];

        endforeach;
//        echo "<pre>";
//        print_r($array);
//        exit;
        return new JsonModel($array);
    }

    public function addAction() {
        $formCor = new Cor();
        return new JsonModel($this->adicionar('Cor', $formCor));
    }

    public function removeAction() {
        return new ViewModel(['rota' => $this->getRota()]);
    }

}
