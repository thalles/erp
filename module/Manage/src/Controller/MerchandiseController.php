<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Manage\Controller;

use App\Form\Products\Mercadoria;
use App\Form\Products\Categoria;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

/**
 * Description of GestorController
 *
 * @author Thalles Ferreira
 */
class MerchandiseController extends AbstractController {

    public function listAction() {
        $list = $this->getRepository("Mercadoria")->findBy([], []);
        return new ViewModel(['list' => $list, 'rota' => $this->getRota()]);
    }

    public function listaAction() {

        $list = $this->getRepository('Mercadoria')->findBy([], []);
        $array = [];
        foreach ($list as $atual):
            $filha = $atual->getDescricao() . ' NCM: ' . $atual->getCategoria()->getNumeroNcm();
            $array[] = ['id' => $atual->getId(), 'name' => $filha];
        endforeach;
        return new JsonModel($array);
    }

    public function addAction() {
        $formMerchandise = new Mercadoria();
        $formCategory= new Categoria();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $repositoryMercadoria = $this->getRepository("Mercadoria")->salvar($request->getPost()->toArray());
            return new JsonModel([
                'rota' => $this->getRota(),
                'id' => $repositoryMercadoria->getId()
            ]);
        }
        $id = $this->params()->fromRoute('id', false);
        if ($id) {
            $mercadoria = $this->getRepository("Mercadoria")->find($id);
            $formMerchandise->setData($mercadoria->toArray());
//            $formCategory->setData($mercadoria->getMercadoria()->toArray());
        }

        return new ViewModel([
            'rota' => $this->getRota(),
            'formMercadoria' => $formMerchandise,
            'formCategoria' => $formCategory
        ]);
    }

    public function removeAction() {
        return new ViewModel(['rota' => $this->getRota()]);
    }

}
