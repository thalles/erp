<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonmanage for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Manage;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;
use Zend\Mvc\Controller\LazyControllerAbstractFactory;

$rota = '[/:action][[/id]/:id][[/pessoa]/:pessoa][[/marca]/:marca][[/clone]/:clone][[/produto]/:produto][/]';
$constrains = [
    'controller' => '[a-zA-Z_-]*',
    'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
    'id' => '[0-9]+',
    'pessoa' => '[0-9]+',
    'marca' => '[0-9]+',
    'clone' => '[0-9]+',
    'produto' => '[0-9]+',
];
return [
    'router' => [
        'routes' => [
            'manage' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/gestao/employees' . $rota,
                    'constraints' => $constrains,
                    'defaults' => [
                        'controller' => Controller\EmployeesController::class,
                        'action' => 'list',
                    ],
                ],
            ],
            'manage-client' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/gestao/cliente' . $rota,
                    'constraints' => $constrains,
                    'defaults' => [
                        'controller' => Controller\ClientController::class,
                        'action' => 'list',
                    ],
                ],
            ],
            'manage-provider' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/gestao/fornecedor' . $rota,
                    'constraints' => $constrains,
                    'defaults' => [
                        'controller' => Controller\ProviderController::class,
                        'action' => 'list',
                    ],
                ],
            ],
            'manage-product' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/gestao/produto' . $rota,
                    'constraints' => $constrains,
                    'defaults' => [
                        'controller' => Controller\ProductsController::class,
                        'action' => 'list',
                    ],
                ],
            ],
            'manage-adress' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/gestao/endereco' . $rota,
                    'constraints' => $constrains,
                    'defaults' => [
                        'controller' => Controller\AdressController::class,
                        'action' => 'list',
                    ],
                ],
            ],
            'manage-contact' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/gestao/contato' . $rota,
                    'constraints' => $constrains,
                    'defaults' => [
                        'controller' => Controller\ContactController::class,
                        'action' => 'list',
                    ],
                ],
            ],
            'manage-access' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/gestao/acesso' . $rota,
                    'constraints' => $constrains,
                    'defaults' => [
                        'controller' => Controller\AccessController::class,
                        'action' => 'list',
                    ],
                ],
            ],
            'manage-brand' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/gestao/marca' . $rota,
                    'constraints' => $constrains,
                    'defaults' => [
                        'controller' => Controller\BrandController::class,
                        'action' => 'list',
                    ],
                ],
            ],
            'manage-category' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/gestao/categoria' . $rota,
                    'constraints' => $constrains,
                    'defaults' => [
                        'controller' => Controller\CategoryController::class,
                        'action' => 'list',
                    ],
                ],
            ],
            'manage-model' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/gestao/modelo' . $rota,
                    'constraints' => $constrains,
                    'defaults' => [
                        'controller' => Controller\ModelController::class,
                        'action' => 'list',
                    ],
                ],
            ],
            'manage-color' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/gestao/cor' . $rota,
                    'constraints' => $constrains,
                    'defaults' => [
                        'controller' => Controller\ColorController::class,
                        'action' => 'list',
                    ],
                ],
            ],
            'manage-merchandise' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/gestao/mercadoria' . $rota,
                    'constraints' => $constrains,
                    'defaults' => [
                        'controller' => Controller\MerchandiseController::class,
                        'action' => 'list',
                    ],
                ],
            ],
            'manage-finishing' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/gestao/acabamento' . $rota,
                    'constraints' => $constrains,
                    'defaults' => [
                        'controller' => Controller\FinishingController::class,
                        'action' => 'list',
                    ],
                ],
            ],
            'manage-image' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/gestao/imagem' . $rota,
                    'constraints' => $constrains,
                    'defaults' => [
                        'controller' => Controller\ImageController::class,
                        'action' => 'list',
                    ],
                ],
            ],
            'manage-invoice' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/gestao/entrada' . $rota,
                    'constraints' => $constrains,
                    'defaults' => [
                        'controller' => Controller\InvoiceController::class,
                        'action' => 'list',
                    ],
                ],
            ],
            'manage-nature-operation' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/gestao/natureza-operacao' . $rota,
                    'constraints' => $constrains,
                    'defaults' => [
                        'controller' => Controller\NatureOperationController::class,
                        'action' => 'list',
                    ],
                ],
            ],
            'manage-unit-measure' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/gestao/unidade-medida' . $rota,
                    'constraints' => $constrains,
                    'defaults' => [
                        'controller' => Controller\UnitMeasureController::class,
                        'action' => 'list',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'abstract_factories' => [
            LazyControllerAbstractFactory::class,
        ],
        'factories' => [
            'MyModule\Controller\FooController' => LazyControllerAbstractFactory::class,
        ],
    ],
//    'controllers' => [
//        'factories' => [
//            Controller\AccessController::class => InvokableFactory::class,
//            Controller\AdressController::class => InvokableFactory::class,
//            Controller\BrandController::class => InvokableFactory::class,
//            Controller\CategoryController::class => InvokableFactory::class,
//            Controller\ClientController::class => InvokableFactory::class,
//            Controller\ColorController::class => InvokableFactory::class,
//            Controller\ContactController::class => InvokableFactory::class,
//            Controller\EmployeesController::class => InvokableFactory::class,
//            Controller\FinishingController::class => InvokableFactory::class,
//            Controller\ImageController::class => InvokableFactory::class,
//            Controller\InvoiceController::class => InvokableFactory::class,
//            Controller\MerchandiseController::class => InvokableFactory::class,
//            Controller\ModelController::class => InvokableFactory::class,
//            Controller\NatureOperationController::class => InvokableFactory::class,
//            Controller\ProductsController::class => InvokableFactory::class,
//            Controller\ProviderController::class => InvokableFactory::class,
//            Controller\UnitMeasureController::class => InvokableFactory::class,
//        ],
//    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'manage/index/index' => __DIR__ . '/../view/manage/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            'manage' => __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ]
    ],
];
