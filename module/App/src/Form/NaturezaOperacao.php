<?php

namespace App\Form;

use App\Form\AbstractForm;

/**
 * Description of Unidade Medida
 *
 * @author Midiauai
 */
class UnidadeMedida extends AbstractForm {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);

        $this->setAttributes([
            'method' => 'post',
            'id' => 'form-unidadeMedida',
            'action' => '/gestao/unidade-medida/add'
        ]);

        $this->setLabelOptions(['Should_create_template' => false]);

        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);

        $descricao = new \Zend\Form\Element\Text('descricao');
        $descricao->setLabel('Descrição')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($descricao);

        $especie = new \Zend\Form\Element\Text('especie');
        $especie->setLabel('Espécie')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($especie);

        $cfop = new \Zend\Form\Element\Text('cfop');
        $cfop->setLabel('CFOP')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($cfop);

        $indNfe = new \Zend\Form\Element\Text('indNfe');
        $indNfe->setLabel('Ind. NFE')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($indNfe);

        $indComunica = new \Zend\Form\Element\Text('indComunica');
        $indComunica->setLabel('Ind. Comunica')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($indComunica);

        $indTransp = new \Zend\Form\Element\Text('indTransp');
        $indTransp->setLabel('Ind. Transp.')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($indTransp);

        $indDevol = new \Zend\Form\Element\Text('indDevol');
        $indDevol->setLabel('Ind. Devol.')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($indDevol);
    }

}
