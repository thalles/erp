<?php

namespace App\Form;

use App\Form\AbstractForm;

/**
 * Description of Produtos
 *
 * @author Midiauai
 */
class CargaNF extends NotaFiscal {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);

        $transportador = new \Zend\Form\Element\Hidden('transportador');
        $transportador->setAttribute('id', 'transportador');
        $this->add($transportador);

        $pesquisaTransportador = new \Zend\Form\Element\Text('pesquisaTransportador');
        $pesquisaTransportador->setLabel('Transportador')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('autocomplete', 'off')
                ->setAttribute('class', 'campotexto th-transportador');
        $this->add($pesquisaTransportador);

        $quantidade = new \Zend\Form\Element\Text('quantidade');
        $quantidade->setLabel('Quantidade')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($quantidade);

        $especie = new \Zend\Form\Element\Text('especie');
        $especie->setLabel('Espécie')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($especie);

        $marca = new \Zend\Form\Element\Text('marca');
        $marca->setLabel('Marca')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($marca);

        $numeracao = new \Zend\Form\Element\Text('numeracao');
        $numeracao->setLabel('Numeração')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($numeracao);

        $pesoBruto = new \Zend\Form\Element\Text('pesoBruto');
        $pesoBruto->setLabel('Peso Bruto')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($pesoBruto);

        $pesoLiquido = new \Zend\Form\Element\Text('pesoLiquido');
        $pesoLiquido->setLabel('Peso Liquido')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($pesoLiquido);

        $placaVeiculo = new \Zend\Form\Element\Text('placaVeiculo');
        $placaVeiculo->setLabel('Placa do Veículo')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($placaVeiculo);
    }

}
