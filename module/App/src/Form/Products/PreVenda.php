<?php

namespace App\Form\Products;

use App\Form\AbstractForm;
use Zend\Form\Element\{
    Button,
    Hidden,
    Text
};

/**
 * Description of Produtos
 *
 * @author Midiauai
 */
class PreVenda extends AbstractForm {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);

//        $this->setInputFilter(new Filter\Cidade());
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'form-pre-venda');
        $this->setLabelOptions(['Should_create_template' => false]);

        $pesquisaVendedor = new Text('pesquisaVendedor');
        $pesquisaVendedor->setLabel('Vendedor *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-4'])
                ->setAttribute('autocomplete', 'off')
                ->setAttribute('class', 'campotexto');
        $this->add($pesquisaVendedor);

        $pesquisaProduto = new Text('pesquisaProduto');
        $pesquisaProduto->setLabel('Produto *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-5'])
                ->setAttribute('autocomplete', 'off')
                ->setAttribute('class', 'campotexto');
        $this->add($pesquisaProduto);

        $idProduto = new Hidden('idProduto');
        $idProduto->setAttribute('id', 'idProduto');
        $this->add($idProduto);

        $submit = new Button('submit');
        $submit->setAttribute('type', 'button')
                ->setLabel('Adicionar ao Carrinho')
                ->setAttribute('class', 'col-md-3 btn btn-info btn-same-line')
                ->setAttribute('v-on:click', 'addProduct');
        ;
        $this->add($submit);
    }

}
