<?php

namespace App\Form\Products;

use App\Form\AbstractForm;

/**
 * Description of Produtos
 *
 * @author Midiauai
 */
class Mercadoria extends AbstractForm {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);
       
         $this->setAttributes([
            'method' => 'post',
            'id' => 'form-mercadoria',
            'action' => '/gestao/mercadoria/add'
        ]);
        $this->setLabelOptions(['Should_create_template' => false]);

        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);

        $descricao = new \Zend\Form\Element\Text('descricao');
        $descricao->setLabel('Produto *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto  alvo-modal');
        $this->add($descricao);
        
        $categoria = new \Zend\Form\Element\Select('categoria');
        $categoria->setLabel('Categoria')
                ->setLabelAttributes(['class' => 'labeltexto col-md-4'])
                ->setAttribute('class', 'campotexto')
                ->setAttribute('options', $this->getOpcoes('Categoria', ['descricao', 'numeroNcm'], [], ['descricao' => 'ASC']));
        $this->add($categoria); 
        
        $nova = new \Zend\Form\Element\Button('novaCategoria');
        $nova->setLabel('Nova')
                ->setLabelAttributes(['class' => 'col-md-2'])
                ->setAttribute('class', 'btn btn-submit');
        $this->add($nova);
    }

}
