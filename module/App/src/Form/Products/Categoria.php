<?php

namespace App\Form\Products;

use App\Form\AbstractForm;

/**
 * Description of Produtos
 *
 * @author Midiauai
 */
class Categoria extends AbstractForm {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);

//        $this->setInputFilter(new Filter\Cidade());
        $this->setAttributes([
            'method' => 'post',
            'id' => 'form-categoria',
            'action' => '/gestao/categoria/add',
           // 'style'=>'display:none;'
        ]);
        $this->setLabelOptions(['Should_create_template' => false]);
        //Adição do campo id
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);

        $descricao = new \Zend\Form\Element\Text('descricao');
        $descricao->setLabel('Categoria')
                ->setLabelAttributes(['class' => 'labeltexto col-md-5'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto alvo-modal');
        $this->add($descricao);

        $numeroNcm = new \Zend\Form\Element\Text('numeroNcm');
        $numeroNcm->setLabel('Número NCM')
                ->setLabelAttributes(['class' => 'labeltexto col-md-5'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($numeroNcm);
        
//        $nova = new \Zend\Form\Element\Button('novaCategoria');
//        $nova->setLabel('Cadastrar')
//                ->setLabelAttributes(['class' => 'col-md-2'])
//                ->setAttribute('class', 'btn btn-submit');
//        $this->add($nova);

    }

}
