<?php

namespace App\Form\Products;

use App\Form\AbstractForm;

/**
 * Description of Produtos
 *
 * @author Midiauai
 */
class Modelo extends AbstractForm {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);

//        $this->setInputFilter(new Filter\Cidade());
        $this->setAttributes([
            'method' => 'post',
            'id' => 'form-modelo',
            'action' => '/gestao/modelo/add'
        ]);
        $this->setLabelOptions(['Should_create_template' => false]);

        //Adição do campo id
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);
        $marca = new \Zend\Form\Element\Hidden('marca');
        $this->add($marca);

        $nome = new \Zend\Form\Element\Text('nome');
        $nome->setLabel('Modelo')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto alvo-modal');
        $this->add($nome);
        
        $portas = new \Zend\Form\Element\Text('portas');
        $portas->setLabel('Quant. Portas')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($portas);
        
        $gavetas = new \Zend\Form\Element\Text('gavetas');
        $gavetas->setLabel('Quant. Gavetas')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($gavetas);
        
        $modeloPai = new \Zend\Form\Element\Select('modeloPai');
        $modeloPai->setLabel('Modelo Original')
                ->setLabelAttributes(['class' => 'labeltexto col-md-5'])
                ->setAttribute('class', 'campotexto')
                ->setAttribute('options', $this->getOpcoes('Modelo', ['nome'], [], ['nome' => 'ASC']));
        $this->add($modeloPai);        
        
    }
}
