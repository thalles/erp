<?php

namespace App\Form\Products;

use App\Form\AbstractForm;
use Zend\Form\Element\Date;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Select;
use Zend\Form\Element\Text;

/**
 * Description of Produtos
 *
 * @author Midiauai
 */
class Venda extends AbstractForm {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);
//        $this->setInputFilter(new Filter\Cidade());
        $this->setAttributes([
            'method' => 'post',
            'id'     => 'form-venda',
        ]);
        $this->setLabelOptions(['Should_create_template' => false]);
        //Adição do campo id
        $id = new Hidden('id');
        $this->add($id);




        $vendedor = new Hidden('vendedor');
        $vendedor->setAttribute('id', 'vendedor');
        //->setAttribute('v-model', 'vendedor.id');
        $this->add($vendedor);

        $cliente = new Hidden('cliente');
        $cliente->setAttribute('id', 'cliente');
        $this->add($cliente);

        $pesquisaVendedor = new Text('pesquisaVendedor');
        $pesquisaVendedor->setLabel('Vendedor')
                ->setLabelAttributes(['class' => 'label-texto col-md-6'])
                ->setAttribute('v-model', 'vendedor.nome')
                ->setAttribute('class', 'campo-texto');
        $this->add($pesquisaVendedor);

        $pesquisaCliente = new Text('pesquisaCliente');
        $pesquisaCliente->setLabel('Cliente')
                ->setLabelAttributes(['class' => 'label-texto col-md-6'])
                ->setAttribute('v-model', 'venda.pesquisaCliente')
                ->setAttribute('class', 'campo-texto');
        $this->add($pesquisaCliente);

        $estatusVenda = new Select('estatusVenda');
        $estatusVenda->setLabel('Estatus da Venda')
                ->setLabelAttributes(['class' => 'label-texto col-md-4'])
                ->setAttribute('options', $this->getOpcoes('EstatusVenda', ['descricao']))
                ->setAttribute('v-model', 'venda.estatusVenda')
                ->setAttribute('class', 'campo-texto');
        $this->add($estatusVenda);

        $tipoPagamento = new Select('tipoPagamento');
        $tipoPagamento->setLabel('Tipo de Pagamento')
                ->setLabelAttributes(['class' => 'label-texto col-md-4'])
                ->setAttribute('options', $this->getOpcoes('TipoPagamento', ['descricao']))
                ->setAttribute('v-model', 'venda.tipoPagamento')
                ->setAttribute('class', 'campo-texto');
        $this->add($tipoPagamento);

        $dataVenda = new Date('dataVenda');
        $dataVenda->setLabel('Data da Venda')
                ->setLabelAttributes(['class' => 'label-texto col-md-4'])
                ->setAttribute('v-model', 'venda.dataVenda')
                ->setAttribute('class', 'campo-texto');
        $this->add($dataVenda);

        $aVista = new Text('aVista');
        $aVista->setLabel('Valor à Vista')
                ->setLabelAttributes(['class' => 'label-texto col-md-4'])
                ->setAttribute('v-model', 'venda.aVista')
                ->setAttribute('class', 'campo-texto');
        $this->add($aVista);

        $quantidadeParcelas = new Text('quantidadeParcelas');
        $quantidadeParcelas->setLabel('Quantidade de Parcelas')
                ->setLabelAttributes(['class' => 'label-texto col-md-4'])
                ->setAttribute('v-model', 'venda.quantidadeParcelas')
                ->setAttribute('class', 'campo-texto');
        $this->add($quantidadeParcelas);

        $valorParcela = new Text('valorParcela');
        $valorParcela->setLabel('Valor Parcelas')
                ->setLabelAttributes(['class' => 'label-texto col-md-4'])
                ->setAttribute('class', 'campo-texto')
                ->setAttribute('readonly', 'readonly');
        $this->add($valorParcela);













//        $descricao = new Text('descricao');
//        $descricao->setLabel('Categoria')
//                ->setLabelAttributes(['class' => 'labeltexto col-md-5'])
//                ->setAttribute('placeholder', '')
//                ->setAttribute('class', 'campotexto alvo-modal');
//        $this->add($descricao);
//
//        $numeroNcm = new Text('numeroNcm');
//        $numeroNcm->setLabel('Número NCM')
//                ->setLabelAttributes(['class' => 'labeltexto col-md-5'])
//                ->setAttribute('placeholder', '')
//                ->setAttribute('class', 'campotexto');
//        $this->add($numeroNcm);
//        $nova = new \Zend\Form\Element\Button('novaCategoria');
//        $nova->setLabel('Cadastrar')
//                ->setLabelAttributes(['class' => 'col-md-2'])
//                ->setAttribute('class', 'btn btn-submit');
//        $this->add($nova);
    }

}
