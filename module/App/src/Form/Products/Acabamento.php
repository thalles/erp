<?php

namespace App\Form\Products;

use App\Form\AbstractForm;

/**
 * Description of Produtos
 *
 * @author Midiauai
 */
class Acabamento extends AbstractForm {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);

//        $this->setInputFilter(new Filter\Cidade());
        $this->setAttributes([
            'method' => 'post',
            'id' => 'form-acabamento',
            'action' => '/gestao/acabamento/add'
        ]);
        $this->setLabelOptions(['Should_create_template' => false]);

        //Adição do campo id
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);

        $nome = new \Zend\Form\Element\Text('nome');
        $nome->setLabel('Acabamento')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto alvo-modal');
        $this->add($nome);
        
        $sigla = new \Zend\Form\Element\Text('sigla');
        $sigla->setLabel('Sigla')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($sigla);
    }

}
