<?php

namespace App\Form\Products;

use App\Form\AbstractForm;

/**
 * Description of Produtos
 *
 * @author Midiauai
 */
class Cor extends AbstractForm {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);

//        $this->setInputFilter(new Filter\Cidade());
        $this->setAttributes([
            'method' => 'post',
            'id' => 'form-cor',
            'action' => '/gestao/cor/add'
        ]);
        
        
        $this->setLabelOptions(['Should_create_template' => false]);

        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);

        $nome = new \Zend\Form\Element\Text('nome');
        $nome->setLabel('Cor')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto alvo-modal');
        $this->add($nome);

        $corPai = new \Zend\Form\Element\Select('corPai');
        $corPai->setLabel('Cor Original')
                ->setLabelAttributes(['class' => 'labeltexto col-md-5'])
                ->setAttribute('class', 'campotexto')
                ->setAttribute('options', $this->getOpcoes('Cor', ['nome'], [], ['nome' => 'ASC']));
        $this->add($corPai);
        $this->getInputFilter()->get('corPai')->setRequired(false);
    }

}
