<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Form\Products;

/**
 * Description of MovimentoProduto
 *
 * @author claua
 */
class MovimentoProduto {

    public function __construct() {
        $valorMinimo = new \Zend\Form\Element\Text('valorMinimo');
        $valorMinimo->setLabel('Valor Mínimo de Venda')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($valorMinimo);


        $cfop = new \Zend\Form\Element\Text('cfop');
        $cfop->setLabel('CFOP')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($cfop);
    }

}
