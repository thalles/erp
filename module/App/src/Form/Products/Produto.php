<?php

namespace App\Form\Products;

use App\Form\AbstractForm;

/**
 * Description of Produtos
 *
 * @author Midiauai
 */
class Produto extends AbstractForm {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);

//        $this->setInputFilter(new Filter\Cidade());
        $this->setAttribute('method', 'post');
        $this->setLabelOptions(['Should_create_template' => false]);

        //Adição do campo id
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);

        $mercadoria = new \Zend\Form\Element\Hidden('mercadoria');
        $mercadoria->setAttribute('id', 'mercadoria')
                ->setAttribute('value', '');
        $this->add($mercadoria);

        $marca = new \Zend\Form\Element\Hidden('marca');
        $marca->setAttribute('id', 'marca');
        $this->add($marca);

//        $modelo = new \Zend\Form\Element\Hidden('modelo');
//        $modelo->setAttribute('id', 'modelo');
//        $this->add($modelo);
//
//        $acabamento = new \Zend\Form\Element\Hidden('acabamento');
//        $acabamento->setAttribute('id', 'acabamento');
//        $this->add($acabamento);
//
//        $cor = new \Zend\Form\Element\Hidden('cor');
//        $cor->setAttribute('id', 'cor');
//        $this->add($cor);
//        $pesquisaModelo = new \Zend\Form\Element\Text('pesquisaModelo');
//        $pesquisaModelo->setLabel('Modelo *')
//                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
//                ->setAttribute('autocomplete', 'off')
//                ->setAttribute('class', 'campotexto th-modelo');
//        $this->add($pesquisaModelo);
//
//        $pesquisaAcabamento = new \Zend\Form\Element\Text('pesquisaAcabamento');
//        $pesquisaAcabamento->setLabel('Acabamento ')
//                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
//                ->setAttribute('autocomplete', 'off')
//                ->setAttribute('class', 'campotexto th-acabamento');
//        $this->add($pesquisaAcabamento);
//
//        $pesquisaCor = new \Zend\Form\Element\Text('pesquisaCor');
//        $pesquisaCor->setLabel('Cor ')
//                ->setLabelAttributes(['class' => 'labeltexto col-md-4'])
//                ->setAttribute('autocomplete', 'off')
//                ->setAttribute('class', 'campotexto th-cor');
//        $this->add($pesquisaCor);

        $codigoFabrica = new \Zend\Form\Element\Text('codigoFabrica');
        $codigoFabrica->setLabel('Código de Fábrica*')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($codigoFabrica);


        $descricao = new \Zend\Form\Element\Text('descricao');
        $descricao->setLabel('Descrição*')
                ->setLabelAttributes(['class' => 'labeltexto col-md-8'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($descricao);

        $valorAtual = new \Zend\Form\Element\Text('valorAtual');
        $valorAtual->setLabel('Valor Atual')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($valorAtual);

        $pesquisaMercadoria = new \Zend\Form\Element\Text('pesquisaMercadoria');
        $pesquisaMercadoria->setLabel('Mercadoria *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('autocomplete', 'off')
                ->setAttribute('class', 'campotexto th-mercadoria');
        $this->add($pesquisaMercadoria);

        $pesquisaMarca = new \Zend\Form\Element\Text('pesquisaMarca');
        $pesquisaMarca->setLabel('Marca *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('autocomplete', 'off')
                ->setAttribute('class', 'campotexto th-marca');
        $this->add($pesquisaMarca);

        $altura = new \Zend\Form\Element\Text('altura');
        $altura->setLabel('Altura')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($altura);

        $largura = new \Zend\Form\Element\Text('largura');
        $largura->setLabel('Largura')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($largura);

        $profundidade = new \Zend\Form\Element\Text('profundidade');
        $profundidade->setLabel('Profundidade')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($profundidade);

        $informacaoAdicional = new \Zend\Form\Element\TextArea('informacaoAdicional');
        $informacaoAdicional->setLabel('Informações adicionais')
                ->setLabelAttributes(['class' => 'labeltexto col-md-12'])
                ->setAttribute('placeholder', 'Gavetas, portas, lugares')
                ->setAttributes(['class' => 'campotexto', 'rows' => '4']);
        $this->add($informacaoAdicional);
    }

}
