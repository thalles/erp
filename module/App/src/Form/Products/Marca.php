<?php

namespace App\Form\Products;

use App\Form\AbstractForm;

/**
 * Description of Produtos
 *
 * @author Midiauai
 */
class Marca extends AbstractForm {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);

//        $this->setInputFilter(new Filter\Cidade());
        $this->setAttributes([
            'method' => 'post',
            'id' => 'form-marca',
            'action' => '/gestao/marca/add'
        ]);
        $this->setLabelOptions(['Should_create_template' => false]);

        //Adição do campo id
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);

        $nome = new \Zend\Form\Element\Text('nome');
        $nome->setLabel('Marca')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto alvo-modal');
        $this->add($nome);
    }

}
