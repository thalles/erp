<?php

namespace App\Form;

/**
 * Description of Geral
 *
 * @author Letivo
 */
class Imagem extends AbstractForm {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);

//        $this->setInputFilter(new Filter\Pais());
        $this->setAttribute('method', 'post');
        $this->setLabelOptions(['Should_create_template'=> false]);

        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);

        $src = new \Zend\Form\Element\File('src');
        $src->setLabel('Imagem *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('placeholder', 'Informe o nome do País')
                ->setAttribute('class', 'teste');
        $this->add($src);
        
        $legenda = new \Zend\Form\Element\Text('legenda');
        $legenda->setLabel('Legenda')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('class', 'campotexto');
        $this->add($legenda);
        
        $ordem = new \Zend\Form\Element\Text('ordem');
        $ordem->setLabel('Ordem')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('class', 'campotexto');
        $this->add($ordem);
        
//        $submit = new \Zend\Form\Element\Submit('submit');
//        $submit->setAttribute('value', ' Adicionar usuário ');
//        $this->add($submit);
//        
    }

}
