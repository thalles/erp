<?php

namespace App\Form;

/**
 * Description of Usuarios
 *
 * @author Letivo
 */
class Pais extends AbstractForm {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);

//        $this->setInputFilter(new Filter\Pais());
        $this->setAttribute('method', 'post');
        $this->setLabelOptions(['Should_create_template'=> false]);

        //Adição do campo id
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);

        //Adição do campo Nome
        $nome = new \Zend\Form\Element\Text('nome');
        $nome->setLabel('Nome *')
                ->setLabelAttributes(['class' => 'teste'])
                ->setAttribute('placeholder', 'Informe o nome do País')
                ->setAttribute('class', 'teste');
        $this->add($nome);
        
        $sigla = new \Zend\Form\Element\Text('sigla');
        $sigla->setLabel('Sigla *')
                ->setLabelAttributes(['class' => 'teste'])
                ->setAttribute('placeholder', 'Informe a sigla do País')
                ->setAttribute('class', 'teste');
        $this->add($sigla);
        
        
        
//        $submit = new \Zend\Form\Element\Submit('submit');
//        $submit->setAttribute('value', ' Adicionar usuário ');
//        $this->add($submit);
//        
    }

}
