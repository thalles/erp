<?php

namespace App\Form;

/**
 * Description of Usuarios
 *
 * @author Midiauai
 */
class Estado extends AbstractForm {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);

//        $this->setInputFilter(new Filter\Estado());
        $this->setAttribute('method', 'post');
        $this->setLabelOptions(['Should_create_template'=> false]);

        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);
        
        $pais = new \Zend\Form\Element\Hidden('pais');
        $this->add($pais);
         
        $nome = new \Zend\Form\Element\Text('nome');
        $nome->setLabel('Estado')
                ->setLabelAttributes(['class' => 'teste'])
                ->setAttribute('placeholder', 'Estado')
                ->setAttribute('class', 'teste');
        $this->add($nome);
        
        $uf = new \Zend\Form\Element\Text('uf');
        $uf->setLabel('Sigla')
                ->setLabelAttributes(['class' => ''])
                ->setAttribute('placeholder', 'Estado')
                ->setAttribute('class', '');
        $this->add($uf);
        
        $aliquotaIcms = new \Zend\Form\Element\Text('aliquotaIcms');
        $aliquotaIcms->setLabel('Alíquota ICMS')
                ->setLabelAttributes(['class' => ''])
                ->setAttribute('placeholder', 'Estado')
                ->setAttribute('class', '');
        $this->add($aliquotaIcms);
        
        $aliquotaIcmsSaida = new \Zend\Form\Element\Text('aliquotaIcmsSaida');
        $aliquotaIcmsSaida->setLabel('Alíquota ICMS Saída')
                ->setLabelAttributes(['class' => ''])
                ->setAttribute('placeholder', 'Estado')
                ->setAttribute('class', '');
        $this->add($aliquotaIcmsSaida);
        
        $aliquotaIcmsEntrada = new \Zend\Form\Element\Text('aliquotaIcmsEntrada');
        $aliquotaIcmsEntrada->setLabel('Alíquota ICMS Entrada')
                ->setLabelAttributes(['class' => ''])
                ->setAttribute('placeholder', 'Estado')
                ->setAttribute('class', '');
        $this->add($aliquotaIcmsEntrada);
        
        $aliquotaIcmsSaidaCpf = new \Zend\Form\Element\Text('aliquotaIcmsSaidaCpf');
        $aliquotaIcmsSaidaCpf->setLabel('Alíquota ICMS Saída com CPF')
                ->setLabelAttributes(['class' => ''])
                ->setAttribute('placeholder', 'Estado')
                ->setAttribute('class', '');
        $this->add($aliquotaIcmsSaidaCpf);
        
        $aliquotaIcmsEntradaCpf = new \Zend\Form\Element\Text('aliquotaIcmsEntradaCpf');
        $aliquotaIcmsEntradaCpf->setLabel('Alíquota ICMS Entrada com CPF')
                ->setLabelAttributes(['class' => ''])
                ->setAttribute('placeholder', 'Estado')
                ->setAttribute('class', '');
        $this->add($aliquotaIcmsEntradaCpf);
        
        $inscricaoSubstituicaoTributaria = new \Zend\Form\Element\Text('inscricaoSubstituicaoTributaria');
        $inscricaoSubstituicaoTributaria->setLabel('Inscricao de Substituicao Tributaria')
                ->setLabelAttributes(['class' => ''])
                ->setAttribute('placeholder', 'Estado')
                ->setAttribute('class', '');
        $this->add($inscricaoSubstituicaoTributaria);
        
        $ufIbge = new \Zend\Form\Element\Text('ufIbge');
        $ufIbge->setLabel('Código do IBGE')
                ->setLabelAttributes(['class' => ''])
                ->setAttribute('placeholder', 'Estado')
                ->setAttribute('class', '');
        $this->add($ufIbge);
        
        $aliquotaPobreza = new \Zend\Form\Element\Text('alquotaPobreza');
        $aliquotaPobreza->setLabel('Código do IBGE')
                ->setLabelAttributes(['class' => ''])
                ->setAttribute('placeholder', 'Estado')
                ->setAttribute('class', '');
        $this->add($aliquotaPobreza);

//        $submit = new \Zend\Form\Element\Submit('submit');
//        $submit->setAttribute('value', ' Adicionar usuário ');
//        $this->add($submit);
        
    }

}
