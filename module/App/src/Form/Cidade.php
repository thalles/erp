<?php

namespace App\Form;


/**
 * Description of Usuarios
 *
 * @author Midiauai
 */
class Cidade extends AbstractForm {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);

//        $this->setInputFilter(new Filter\Cidade());
        $this->setAttribute('method', 'post');
        $this->setLabelOptions(['Should_create_template'=> false]);

        //Adição do campo id
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);
        
        $estado = new \Zend\Form\Element\Hidden('estado');
        $this->add($estado);
        
        
        
        //Adicionando campo de telefone
        $cidade = new \Zend\Form\Element\Text('nome');
        $cidade->setLabel('Cidade')
                ->setLabelAttributes(['class' => 'teste'])
                ->setAttribute('placeholder', 'Nome da cidade')
                ->setAttribute('class', 'teste');
        $this->add($cidade);
        
       
//        $submit = new \Zend\Form\Element\Submit('submit');
//        $submit->setAttribute('value', ' Adicionar usuário ');
//        $this->add($submit);     
    }

}
