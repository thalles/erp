<?php

namespace App\Form\Persons;
use App\Form\AbstractForm;

/**
 * Description of Usuarios
 * @author Midiauai
 */
class Grupo extends AbstractForm {

    function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
//        $this->setInputFilter(new Filter\Pessoa());
        $this->setAttribute('method', 'post');
        $this->setAttribute('autocomplete', 'off');

        //Adição do campo id
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);

        //Adição do campo Nome
        $nome = new \Zend\Form\Element\Text('nome');
        $nome->setLabel('Nome *')
                ->setLabelAttributes(array('class' => 'labeltexto medio'))
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($nome);

        $descricao = new \Zend\Form\Element\Text('descricao');
        $descricao > setLabel('Breve descrição')
                        ->setLabelAttributes(array('class' => 'labeltexto medio'))
                        ->setAttribute('placeholder', '')
                        ->setAttribute('class', 'campotexto');
        $this->add($descricao);

        $icone = new \Zend\Form\Element\Text('icone');
        $icone->setLabel('Ícone')
                ->setLabelAttributes(array('class' => 'labeltexto medio'))
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($icone);
    }

}
