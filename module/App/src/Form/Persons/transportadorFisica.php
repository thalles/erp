<?php

namespace App\Form\Persons;

use Zend\Form\Element\Hidden;
use Zend\Form\Element\Text;

/**
 * Description of Usuarios
 * @author Midiauai
 */
class TransportadorFisica extends Transportador {

    function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
        $this->setAttribute('id', 'form-transportador-cpf');
        $this->getAttribute('id');
        $tipoPessoa = new Hidden('tipoPessoa');
        $tipoPessoa->setValue(1);
        $this->add($tipoPessoa);


        $cpf = new Text('cpf');
        $cpf->setLabel('CPF *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($cpf);
    }

}
