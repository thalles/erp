<?php

namespace App\Form\Persons;

use Zend\Form\Element\Date;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Select;
use Zend\Form\Element\Text;

/**
 * Description of Usuarios
 * @author Midiauai
 */
class Fisica extends Pessoa {

    function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);

        $pessoa = new Hidden('pessoa');
        $this->add($pessoa);

        $tipoPessoa = new Hidden('tipoPessoa');
        $tipoPessoa->setValue(1);
        $this->add($tipoPessoa);

        $cpf = new Text('cpf');
        $cpf->setLabel('CPF *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($cpf);

        $sexo = new Select('sexo');
        $sexo->setLabel('Sexo')
                ->setLabelAttributes(['class' => 'labeltexto col-md-4'])
                ->setAttribute('options', ['', 'F' => 'Feminino', 'M' => 'Masculino'])
                ->setAttribute('class', 'campotexto');
        $this->add($sexo);

        $identidade = new Text('identidade');
        $identidade->setLabel('Identidade *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-4'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($identidade);


        $tipoIdentidade = new Select('tipoIdentidade');
        $tipoIdentidade->setLabel('Tipo Identidade *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-4'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto')
                ->setValueOptions($this->getOpcoes('TipoIdentidade', ['titulo', 'emissor', 'sigla']));
        $this->add($tipoIdentidade);

        $estadoCivil = new Select('estadoCivil');
        $estadoCivil->setLabel('Estado Civil *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-4'])
                ->setAttribute('options', ['', 'C' => 'Casado', 'S' => 'Solteiro'])
                ->setAttribute('class', 'campotexto');
        $this->add($estadoCivil);

        $nascimento = new Date('nascimento');
        $nascimento->setLabel('Data de Nascimento *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-4'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($nascimento);

        $nacionalidade = new Select('nacionalidade');
        $nacionalidade->setLabel('Nacionalidade *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-4'])
                ->setAttribute('options', ['B' => 'Brasileiro', 'E' => 'Estrangeiro', 'N' => 'Naturalizado'])
                ->setAttribute('class', 'campotexto');
        $this->add($nacionalidade);

        //Se ainda pode acessaor o sistema. etc.
        $status = new Select('status');
        $status->setLabel('Estado no Sistema *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('options', ['1' => 'Ativo', '2' => 'Inativo'])
                ->setAttribute('class', 'campotexto');
        $this->add($status);
    }

}
