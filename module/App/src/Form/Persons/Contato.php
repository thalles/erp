<?php

namespace App\Form\Persons;

use App\Form\AbstractForm;

/**
 * Description dos contatos
 * @author Midiauai
 */
class Contato extends AbstractForm {

    function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);

        $this->setAttributes([
            'method' => 'post',
            'name' => 'form-contatoFisica',
            'action' => '/gestao/contato/add'
        ]);
        $this->setAttribute('autocomplete', 'off');

        $id = new \Zend\Form\Element\Hidden('id');
        $id->setAttribute('v-model', 'contactEdit.id');
        $this->add($id);

        $pessoa = new \Zend\Form\Element\Hidden('pessoa');
        $pessoa->setAttribute('v-model', 'contactEdit.pessoa');
        $this->add($pessoa);

        $responsavel = new \Zend\Form\Element\Text('responsavel');
        $responsavel->setLabel('Responsável')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('class', 'campotexto')
                ->setAttribute('v-model', 'contactEdit.responsavel');
        $this->add($responsavel);

        $descricao = new \Zend\Form\Element\Select('descricao');
        $descricao->setLabel('Tipo do telefone')
                ->setLabelAttributes(['class' => 'labeltexto labeltexto col-md-3'])
                ->setValueOptions(['Residencial' => 'Residencial', 'Celular' => 'Celular', 'Comercial' => 'Comercial'])
                ->setAttribute('class', 'campotexto')
                ->setAttribute('v-model', 'contactEdit.descricao');
        $this->add($descricao);

        $numero = new \Zend\Form\Element\Tel('numero');
        $numero->setLabel('Número')
                ->setLabelAttributes(['class' => 'labeltexto labeltexto col-md-3'])
                ->setAttribute('placeholder', '(__) _ ____-____')
                ->setAttribute('class', 'campotexto')
                ->setAttribute('v-model', 'contactEdit.numero')
                ->setAttribute('v-mask', "'(##) ?# ####-####'");
        $this->add($numero);

        $principal = new \Zend\Form\Element\Select('principal');
        $principal->setLabel('Telefone Principal')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setValueOptions(['1' => 'Sim', '0' => 'Não'])
                ->setAttribute('class', 'campotexto')
                ->setAttribute('v-model', 'contactEdit.principal');
        $this->add($principal);
    }

}
