<?php

namespace App\Form\Persons;
use App\Form\AbstractForm;

/**
 * Description do padrão
 * @author Midiauai
 */
class Padrao extends AbstractForm {

    function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
//        $this->setInputFilter(new Filter\Padrao());
        $this->setAttribute('method', 'post');
        $this->setAttribute('autocomplete', 'off');

        //Adição do campo id
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);

        //Adição do campo Nome
        $descricao = new \Zend\Form\Element\Text('descricao');
        $descricao->setLabel('Nome do Padrão de acesso *')
                ->setLabelAttributes(array('class' => 'labeltexto medio'))
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($descricao);
    }

}
