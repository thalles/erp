<?php

namespace App\Form\Persons;

use App\Form\AbstractForm;

/**
 * Description of Usuarios
 * @author Midiauai
 */
class Juridica extends Pessoa {

    function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);

        //Adição do campo id
        $idJuridica = new \Zend\Form\Element\Hidden('idJuridica');
        $this->add($idJuridica);

        $pessoa = new \Zend\Form\Element\Hidden('pessoa');
        $this->add($pessoa);

        $tipoPessoa = new \Zend\Form\Element\Hidden('tipoPessoa');
        $tipoPessoa->setValue(2);
        $this->add($tipoPessoa);


        $cnpj = new \Zend\Form\Element\Text('cnpj');
        $cnpj->setLabel('CNPJ *')
                ->setLabelAttributes(array('class' => 'labeltexto col-md-3'))
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($cnpj);

        //Adição do padrão de acesso, funcionário, cliente, fornecedor. etc.
        $razaoSocial = new \Zend\Form\Element\Text('razaoSocial');
        $razaoSocial->setLabel('Razão Social')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('class', 'campotexto');
        $this->add($razaoSocial);

        $inscricaoEstadual = new \Zend\Form\Element\Text('inscricaoEstadual');
        $inscricaoEstadual->setLabel('Inscrição Estadual *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($inscricaoEstadual);

        $inscricaoMunicipal = new \Zend\Form\Element\Text('inscricaoMunicipal');
        $inscricaoMunicipal->setLabel('Inscricao Municipal')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($inscricaoMunicipal);

        $enderecoEletronico = new \Zend\Form\Element\Text('enderecoEletronico');
        $enderecoEletronico->setLabel('Endereço Eletrônico *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($enderecoEletronico);

        $responsavel = new \Zend\Form\Element\Text('nomeContato');
        $responsavel->setLabel('Nome do Responsável')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($responsavel);
    }

}
