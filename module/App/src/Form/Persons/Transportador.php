<?php

namespace App\Form\Persons;

use App\Form\AbstractForm;

/**
 * Description of Usuarios
 * @author Midiauai
 */
class Transportador extends Pessoa {

    function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);

        $padrao = new \Zend\Form\Element\Hidden('padrao');
        $padrao->setValue(4);

        $principal = new \Zend\Form\Element\Hidden('principal');
        $principal->setValue('1');
        $this->add($principal);

        $this->add($padrao);
        $pessoa = new \Zend\Form\Element\Hidden('pessoa');
        $this->add($pessoa);

        $codigoAntt = new \Zend\Form\Element\Text('codigoAntt');
        $codigoAntt->setLabel('Código ANTT *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($codigoAntt);

        $cadastroNacional = new \Zend\Form\Element\Text('cadastroNacional');
        $cadastroNacional->setLabel('Cadastro Nacional *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($cadastroNacional);
    }

}
