<?php

namespace App\Form\Persons\Filter;

/**
 * Description of Endereco
 *
 * @author Thalles
 */
class Endereco extends \Zend\InputFilter\InputFilter {

    public function __construct() {
        $this->add([
            'name' => 'regiao',
            'required' => false,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => 'Selecione o evento deste dia']]]
            ]
        ]);
        $this->add([
            'name' => 'titulo',
            'required' => true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                ['name' => 'NotEmpty', 'options' => ['messages' => ['isEmpty' => 'O titulo é obrigatório']]]
            ]
        ]);
    }

}
