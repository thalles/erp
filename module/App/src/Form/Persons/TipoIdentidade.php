<?php

namespace App\Form\Persons;
use App\Form\AbstractForm;

/**
 * Description of Usuarios
  @author Thalles
 */
class TipoIdentidade extends FormAbstract {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);

//        $this->setInputFilter(new Filter\TipoIdentidade());
        $this->setAttribute('method', 'post');
        $this->setLabelOptions(['Should_create_template' => false]);

        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);

        $titulo = new \Zend\Form\Element\Text('titulo');
        $titulo->setLabel('Título *')
                ->setLabelAttributes(['class' => 'labeltexto pequeno'])
                ->setAttribute('placeholder', 'Informe o tipo')
                ->setAttribute('class', 'campotexto');
        $this->add($titulo);

        $emissor = new \Zend\Form\Element\Text('emissor');
        $emissor->setLabel('Emissor da Identidade *')
                ->setLabelAttributes(['class' => 'labeltexto pequeno'])
                ->setAttribute('placeholder', 'Nome do órgão')
                ->setAttribute('class', 'campotexto');
        $this->add($emissor);

        $sigla = new \Zend\Form\Element\Text('sigla');
        $sigla->setLabel('Emissor da Identidade *')
                ->setLabelAttributes(['class' => 'labeltexto pequeno'])
                ->setAttribute('placeholder', 'Sigla do órgão')
                ->setAttribute('class', 'campotexto');
        $this->add($sigla);
    }

}
