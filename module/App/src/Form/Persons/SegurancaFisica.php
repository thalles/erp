<?php

namespace App\Form\Persons;

use App\Form\AbstractForm;

/**
 * Description of Usuarios
 * @author Midiauai
 */
class SegurancaFisica extends Seguranca {

    function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
        $this->setAttribute('autocomplete', "off");

        $fisica = new \Zend\Form\Element\Hidden('fisica');
        $fisica->setAttribute('v-model', 'acessEdit.pessoa');

        $this->add($fisica);
    }

}
