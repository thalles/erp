<?php

namespace App\Form\Persons;

/**
 * Description of Usuarios
 * @author Midiauai
 */
class SegurancaJuridica extends Seguranca {

    function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);

        $juridica = new \Zend\Form\Element\Hidden('juridica');
        $juridica->setValue('1');
        $this->add($juridica);
    }

}
