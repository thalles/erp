<?php

namespace App\Form\Persons;

use App\Form\AbstractForm;

/**
 * Description of Usuarios
 * @author Midiauai
 */
class TransportadorJuridica extends Transportador {

    function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
        $this->setAttribute('id', 'form-transportador-cnpj');

        $pessoa = new \Zend\Form\Element\Hidden('pessoa');
        $this->add($pessoa);

        $tipoPessoa = new \Zend\Form\Element\Hidden('tipoPessoa');
        $tipoPessoa->setValue(2);
        $this->add($tipoPessoa);

        $cnpj = new \Zend\Form\Element\Text('cnpj');
        $cnpj->setLabel('CNPJ *')
                ->setLabelAttributes(array('class' => 'labeltexto col-md-3'))
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($cnpj);

        $inscricaoEstadual = new \Zend\Form\Element\Text('inscricaoEstadual');
        $inscricaoEstadual->setLabel('Inscrição Estadual *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($inscricaoEstadual);
    }

}
