<?php

namespace App\Form\Persons;
use App\Form\AbstractForm;

/**
 * Description of Usuarios
 * @author Midiauai
 */
class Funcionario extends Fisica {

    function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
        $this->get('padrao')->setValue(2);
        //Adição do padrão de acesso, funcionário, cliente, fornecedor. etc.

        $dataAdmissao = new \Zend\Form\Element\Date('dataAdmissao');
        $dataAdmissao->setLabel('Data de Admissao *')
                ->setLabelAttributes(array('class' => 'labeltexto col-md-6'))
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($dataAdmissao);
    }

}
