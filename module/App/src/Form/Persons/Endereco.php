<?php

namespace App\Form\Persons;

use App\Form\AbstractForm;

/**
 * Description of Usuarios
 * @author Midiauai
 */
class Endereco extends AbstractForm {

    function __construct($name = null, $ufId = null, $options = []) {
        parent::__construct($name, $options);
        $this->setInputFilter(new Filter\Endereco());
        $this->setAttributes([
            'method'              => 'post',
            'name'                => 'form-endereco',
            'action'              => '/gestao/endereco/add',
            'v-on:submit.prevent' => 'saveAdress()'
        ]);

        $this->setAttribute('autocomplete', 'off');

        $this->setLabelOptions(['Should_create_template' => false]);

        //Adição do campo id
        $id = new \Zend\Form\Element\Hidden('id');
        $id->setAttribute('v-model', 'adressEdit.id');
        $this->add($id);

        $pessoa = new \Zend\Form\Element\Hidden('pessoa');
        //$pessoa->setAttribute('id', 'pessoa');
        $this->add($pessoa);


        $titulo = new \Zend\Form\Element\Text('titulo');
        $titulo->setLabel('Título do endereço *')
                ->setLabelAttributes(array('class' => 'labeltexto col-md-3'))
                ->setAttribute('placeholder', 'Comercial, Residencial, entrega...')
                ->setAttribute('v-model', 'adressEdit.titulo')
                ->setAttribute('class', 'campotexto');
        $this->add($titulo);

        $cep = new \Zend\Form\Element\Text('cep');
        $cep->setLabel('Cep *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('v-model', 'adressEdit.cep')
                ->setAttribute('v-on:blur', 'setLogradouro()')
                ->setAttribute('class', 'campotexto');
        $this->add($cep);

        //Adição do padrão de acesso, funcionário, cliente, fornecedor. etc.
        $logradouro = new \Zend\Form\Element\Text('logradouro');
        $logradouro->setLabel('Logradouro')
                ->setLabelAttributes(['class' => 'labeltexto col-md-5'])
                ->setAttribute('v-model', 'adressEdit.logradouro')
                ->setAttribute('class', 'campotexto');
        $this->add($logradouro);

        $numero = new \Zend\Form\Element\Text('numero');
        $numero->setLabel('Número')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('v-model', 'adressEdit.numero')
                ->setAttribute('class', 'campotexto');
        $this->add($numero);

        $bairro = new \Zend\Form\Element\Text('bairro');
        $bairro->setLabel('Bairro *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-4'])
                ->setAttribute('v-model', 'adressEdit.bairro')
                ->setAttribute('class', 'campotexto');
        $this->add($bairro);

        $complemento = new \Zend\Form\Element\Text('complemento');
        $complemento->setLabel('Complemento')
                ->setLabelAttributes(['class' => 'labeltexto col-md-4'])
                ->setAttribute('v-model', 'adressEdit.complemento')
                ->setAttribute('class', 'campotexto');
        $this->add($complemento);

        $uf = new \Zend\Form\Element\Select('uf');
        $uf->setLabel('UF do Endereço *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-4'])
                ->setAttribute('options', $this->getOpcoes('Estado', ['nome']))
                ->setAttribute('v-model', 'adressEdit.uf')
                ->setAttribute('v-on:change', 'getCities()')
                ->setAttribute('class', 'campotexto');
        $this->add($uf);

        $cidade = new \Zend\Form\Element\Select('cidade');
        $cidade->setLabel('Cidade *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-4'])
                ->setAttribute('options', ['-1' => 'Primeiro Selecione a UF'])//
                ->setOptions(['disable_inarray_validator' => true])
                ->setAttribute('v-model', 'adressEdit.cidade')
                ->setAttribute('class', 'campotexto');
        $this->add($cidade);

        $referencia = new \Zend\Form\Element\Text('referencia');
        $referencia->setLabel('Referência')
                ->setLabelAttributes(['class' => 'labeltexto col-md-4'])
                ->setAttribute('v-model', 'adressEdit.referencia')
                ->setAttribute('class', 'campotexto');
        $this->add($referencia);

        $regiao = new \Zend\Form\Element\Select('regiao');
        $regiao->setLabel('Mapeamento de região')
                ->setLabelAttributes(['class' => 'labeltexto col-md-4'])
                ->setAttribute('options', $this->getOpcoes('Regiao', ['regiao']))
                ->setAttribute('v-model', 'adressEdit.regiao')
                ->setAttribute('class', 'campotexto');
        $this->add($regiao);

        $submit = new \Zend\Form\Element\Button('submit');
        $submit->setLabel('Salvar Endereço')
                ->setAttribute('type', 'submit')
                ->setAttribute('class', 'btn btn-submit');
        $this->add($submit);

        $submit = new \Zend\Form\Element\Button('reset');
        $submit->setLabel('Novo Endereço')
                ->setAttribute('type', 'reset')
                ->setAttribute('v-on:click', 'resetAdress')
                ->setAttribute('class', 'btn btn-submit');
        $this->add($submit);
    }

}
