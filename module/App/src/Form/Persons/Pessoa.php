<?php

namespace App\Form\Persons;

use App\Form\AbstractForm;

/**
 * Description of Usuarios
 * @author Midiauai
 */
class Pessoa extends AbstractForm {

    function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
//        $this->setInputFilter(new Filter\Pessoa());
        $this->setAttribute('method', 'post');
        $this->setAttribute('name', 'form-pessoa');
        $this->setAttribute('autocomplete', 'off');

        //Adição do campo id
        $id = new \Zend\Form\Element\Hidden('id');
        $id->setAttribute('id', 'pessoa');
        $this->add($id);

        //Adição do campo Nome
        $nome = new \Zend\Form\Element\Text('nome');
        $nome->setLabel('Nome *')
                ->setLabelAttributes(array('class' => 'labeltexto col-md-6'))
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($nome);

        //Adição do padrão de acesso, funcionário, cliente, fornecedor. etc.
        $padrao = new \Zend\Form\Element\Select('padrao');
        $padrao->setLabel('Padrão de acesso')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('class', 'campotexto readonly')
                ->setValueOptions($this->getOpcoes('Padrao', ['descricao']));
        $this->add($padrao);
    }

}
