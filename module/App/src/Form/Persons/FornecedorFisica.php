<?php

namespace App\Form\Persons;
use App\Form\AbstractForm;

/**
 * Description of Usuarios
 * @author Midiauai
 */
class FornecedorFisica extends Fisica {

    function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
        $this->get('padrao')->setValue(3);
    }

}
