<?php

namespace App\Form\Persons;

use App\Form\AbstractForm;

/**
 * Description of Usuarios
 * @author Midiauai
 */
class Seguranca extends AbstractForm {

    function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
//        $this->setInputFilter(new Filter\Pessoa());
        $this->setAttributes([
            'method'              => 'post',
            'name'                => 'form-seguranca',
            'action'              => '/gestao/acesso/add',
            'v-on:submit.prevent' => 'saveAccess()'
        ]);
        $this->setAttribute('autocomplete', 'off');

        //Adição do campo id
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);

        //Adição do padrão de acesso, funcionário, cliente, fornecedor. etc.
        $grupo = new \Zend\Form\Element\Select('grupo');
        $grupo->setLabel('Padrão de acesso')
                ->setLabelAttributes(['class' => 'labeltexto col-md-4'])
                ->setAttribute('class', 'campotexto')
                ->setAttribute('v-model', 'acessEdit.grupo')
                ->setValueOptions($this->getOpcoes('Grupo', ['nome']));
        $this->add($grupo);

        $email = new \Zend\Form\Element\Text('email');
        $email->setLabel('E-mail *')
                ->setLabelAttributes(array('class' => 'labeltexto col-md-4'))
                ->setAttribute('placeholder', '')
                ->setAttribute('v-model', 'acessEdit.email')
                ->setAttribute('class', 'campotexto');
        $this->add($email);

        $apelido = new \Zend\Form\Element\Text('apelido');
        $apelido->setLabel('Apelido *')
                ->setLabelAttributes(array('class' => 'labeltexto col-md-4'))
                ->setAttribute('placeholder', '')
                ->setAttribute('v-model', 'acessEdit.apelido')
                ->setAttribute('class', 'campotexto');
        $this->add($apelido);


        $submit = new \Zend\Form\Element\Button('submit');
        $submit->setLabel('Salvar Dados de Acesso')
                ->setAttribute('type', 'submit')
                ->setAttribute('class', 'btn btn-submit');
        $this->add($submit);
    }

}
