<?php

namespace App\Form\Persons;

use App\Form\AbstractForm;

/**
 * Description dos contatos
 * @author Midiauai
 */
class ContatoJuridica extends Contato {

    function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);

        $ramal = new \Zend\Form\Element\Text('ramal');
        $ramal->setLabel('Ramal')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($ramal);

        $status = new \Zend\Form\Element\Select('status');
        $status->setLabel('Responsável correto')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setValueOptions(['1' => 'Sim', '0' => 'Não'])
                ->setAttribute('class', 'campotexto');
        $this->add($status);

        $submit = new \Zend\Form\Element\Button('submit');
        $submit->setLabel('Salvar Contato')
                ->setAttribute('type', 'button')
                ->setAttribute('v-on:click', 'saveContact')
                ->setAttribute('class', 'btn btn-submit');
        $this->add($submit);

        $submit = new \Zend\Form\Element\Button('limpar');
        $submit->setLabel('Novo Contato')
                ->setAttribute('type', 'button')
                ->setAttribute('v-on:click', 'resetFormContact()')
                ->setAttribute('class', 'btn btn-submit');
        $this->add($submit);

        $this->setPriority('limpar', '-1');
        $this->setPriority('submit', '-1');
    }

}
