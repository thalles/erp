<?php

namespace App\Form\Persons;
use App\Form\AbstractForm;

/**
 * Description of Usuarios
 * @author Midiauai
 */
class ClienteFisica extends Fisica {

    function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
        $this->get('padrao')->setValue(1);

        $rendaMensal = new \Zend\Form\Element\Text('rendaMensal');
        $rendaMensal->setLabel('Renda Mensal *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($rendaMensal);
    }

}
