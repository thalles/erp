<?php

namespace App\Form\Persons;

use App\Form\AbstractForm;

/**
 * Description dos contatos
 * @author Midiauai
 */
class ContatoFisica extends Contato {

    function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);

        $submit = new \Zend\Form\Element\Button('submit');
        $submit->setLabel('Salvar Contato')
                ->setAttribute('type', 'button')
                ->setAttribute('v-on:click', 'saveContact')
                ->setAttribute('class', 'btn btn-submit');
        $this->add($submit);

        $submit = new \Zend\Form\Element\Button('limpar');
        $submit->setLabel('Novo Contato')
                ->setAttribute('type', 'button')
                ->setAttribute('v-on:click', 'resetFormContact()')
                ->setAttribute('class', 'btn btn-submit');
        $this->add($submit);
    }

}
