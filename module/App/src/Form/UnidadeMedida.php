<?php

namespace App\Form;

/**
 * Description of Usuarios
 *
 * @author Midiauai
 */
class UnidadeMedida extends AbstractForm {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);

        $this->setAttributes([
            'method' => 'post',
            'id' => 'form-unidadeMedida',
            'action' => '/gestao/unidade-medida/add'
        ]);
        $this->setLabelOptions(['Should_create_template' => false]);

        //Adição do campo id
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);

        $unidade = new \Zend\Form\Element\Text('unidade');
        $unidade->setLabel('Abreveatura')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('class', 'campotexto');
        $this->add($unidade);

        $descricao = new \Zend\Form\Element\Text('descricao');
        $descricao->setLabel('Descrição ')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('class', 'campotexto');
        $this->add($descricao);
    }

}
