<?php

namespace App\Form;

use App\Form\AbstractForm;

/**
 * Description of Produtos
 *
 * @author Midiauai
 */
class NotaFiscal extends AbstractForm {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);

//        $this->setInputFilter(new Filter\Cidade());
        $this->setAttribute('method', 'post');
        $this->setLabelOptions(['Should_create_template' => false]);

        //Adição do campo id
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);

        $naturezaOperacao = new \Zend\Form\Element\Hidden('naturezaOperacao');
        $naturezaOperacao->setAttribute('id', 'naturezaOperacao')
                ->setAttribute('value', '');
        $this->add($naturezaOperacao);

        $numero = new \Zend\Form\Element\Text('numero');
        $numero->setLabel('Número')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($numero);

        $serie = new \Zend\Form\Element\Text('serie');
        $serie->setLabel('Série')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($serie);

        $pesquisaNaturezaOperacao = new \Zend\Form\Element\Text('pesquisaNaturezaOperacao');
        $pesquisaNaturezaOperacao->setLabel('Natureza Operação *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('autocomplete', 'off')
                ->setAttribute('class', 'campotexto th-naturezaOperacao');
        $this->add($pesquisaNaturezaOperacao);

        $chaveAcesso = new \Zend\Form\Element\Text('chaveAcesso');
        $chaveAcesso->setLabel('Chave de Acesso')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($chaveAcesso);

        $dataEmissao = new \Zend\Form\Element\Date('dataEmissao');
        $dataEmissao->setLabel('Data Emissao')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($dataEmissao);

        $dataSaida = new \Zend\Form\Element\Date('dataSaida');
        $dataSaida->setLabel('Data de Saída')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($dataSaida);

        $horaSaida = new \Zend\Form\Element\Time('horaSaida');
        $horaSaida->setLabel('Hora da Saída')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($horaSaida);

        $dadosAdicionais = new \Zend\Form\Element\Textarea('dadosAdicionais');
        $dadosAdicionais->setLabel('Dados Adicionais')
                ->setLabelAttributes(['class' => 'labeltexto col-md-12'])
                ->setAttribute('placeholder', '')
                ->setAttributes(['class' => 'campotexto', 'rows' => '3']);
        $this->add($dadosAdicionais);
    }

}
