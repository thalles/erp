<?php

namespace App\Form\Entrada;

use App\Form\AbstractForm;

/**
 * Description of Produtos
 *
 * @author Midiauai
 */
class Produto extends AbstractForm {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);

//        $this->setInputFilter(new Filter\Cidade());
        $this->setAttribute('method', 'post');
        $this->setLabelOptions(['Should_create_template' => false]);

        //Adição do campo id
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);

        $naturezaOperacao = new \Zend\Form\Element\Hidden('naturezaOperacao');
        $naturezaOperacao->setAttribute('id', 'naturezaOperacao')
                ->setAttribute('value', '');
        $this->add($naturezaOperacao);

        $movimentoTipo = new \Zend\Form\Element\Hidden('movimentoTipo');
        $movimentoTipo->setAttribute('id', 'movimentoTipo');
        $this->add($movimentoTipo);

        $emissor = new \Zend\Form\Element\Hidden('emissor');
        $emissor->setAttribute('id', 'emissor');
        $this->add($emissor);

        $enderecoEmissor = new \Zend\Form\Element\Hidden('enderecoEmissor');
        $enderecoEmissor->setAttribute('id', 'enderecoEmissor');
        $this->add($enderecoEmissor);

        $destinatario = new \Zend\Form\Element\Hidden('destinatario');
        $destinatario->setAttribute('id', 'destinatario');
        $this->add($destinatario);

        $enderecoDestinatario = new \Zend\Form\Element\Hidden('enderecoDestinatario');
        $enderecoDestinatario->setAttribute('id', 'enderecoDestinatario');
        $this->add($enderecoDestinatario);

        $transportador = new \Zend\Form\Element\Hidden('transportador');
        $transportador->setAttribute('id', 'transportador');
        $this->add($transportador);

        $carga = new \Zend\Form\Element\Hidden('carga');
        $carga->setAttribute('id', 'carga');
        $this->add($carga);

        $pesquisaNaturezaOperacao = new \Zend\Form\Element\Text('pesquisaNaturezaOperacao');
        $pesquisaNaturezaOperacao->setLabel('Natureza Operação *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('autocomplete', 'off')
                ->setAttribute('class', 'campotexto th-mercadoria');
        $this->add($pesquisaMercadoria);

        $pesquisaMarca = new \Zend\Form\Element\Text('pesquisaMarca');
        $pesquisaMarca->setLabel('Marca *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('autocomplete', 'off')
                ->setAttribute('class', 'campotexto th-marca');
        $this->add($pesquisaMarca);

        $pesquisaModelo = new \Zend\Form\Element\Text('pesquisaModelo');
        $pesquisaModelo->setLabel('Modelo *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('autocomplete', 'off')
                ->setAttribute('class', 'campotexto th-modelo');
        $this->add($pesquisaModelo);

        $pesquisaAcabamento = new \Zend\Form\Element\Text('pesquisaAcabamento');
        $pesquisaAcabamento->setLabel('Acabamento ')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('autocomplete', 'off')
                ->setAttribute('class', 'campotexto th-acabamento');
        $this->add($pesquisaAcabamento);

        $pesquisaCor = new \Zend\Form\Element\Text('pesquisaCor');
        $pesquisaCor->setLabel('Cor ')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('autocomplete', 'off')
                ->setAttribute('class', 'campotexto th-cor');
        $this->add($pesquisaCor);

        $altura = new \Zend\Form\Element\Text('altura');
        $altura->setLabel('Altura')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($altura);

        $largura = new \Zend\Form\Element\Text('largura');
        $largura->setLabel('Largura')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($largura);

        $profundidade = new \Zend\Form\Element\Text('profundidade');
        $profundidade->setLabel('Profundidade')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($profundidade);

        $cst = new \Zend\Form\Element\Text('cst');
        $cst->setLabel('CST')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($cst);

        $cfop = new \Zend\Form\Element\Text('cfop');
        $cfop->setLabel('CFOP')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($cfop);

        $valorAtual = new \Zend\Form\Element\Text('valorAtual');
        $valorAtual->setLabel('Valor Atual')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($valorAtual);

        $valorMinimo = new \Zend\Form\Element\Text('valorMinimo');
        $valorMinimo->setLabel('Valor Mínimo de Venda')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('placeholder', '')
                ->setAttribute('class', 'campotexto');
        $this->add($valorMinimo);
    }

}
