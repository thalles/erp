<?php

namespace App\Form\Entrada;

use App\Form\AbstractForm;

/**
 * Description of Produtos
 *
 * @author Midiauai
 */
class MovimentoProduto extends AbstractForm {

    function __construct($notaId = '', $name = null, $options = []) {
        parent::__construct($name, $options);
        $id = new \Zend\Form\Element\Hidden('id');
        $this->add($id);

        $notaFiscal = new \Zend\Form\Element\Hidden('notaFiscal');
        $notaFiscal->setAttribute('id', 'notaFiscal')->setValue($notaId);
        $this->add($notaFiscal);

        $produto = new \Zend\Form\Element\Hidden('produto');
        $produto->setAttribute('id', 'produto');
        $this->add($produto);

        $unidadeMedida = new \Zend\Form\Element\Hidden('unidadeMedida');
        $unidadeMedida->setAttribute('id', 'unidadeMedida');
        $this->add($unidadeMedida);

        $cfop = new \Zend\Form\Element\Hidden('cfop');
        $cfop->setAttribute('id', 'cfop');
        $this->add($cfop);

        $idNaNota = new \Zend\Form\Element\Text('idNaNota');
        $idNaNota->setLabel('Código')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('class', 'campotexto');
        $this->add($idNaNota);

        $pesquisaProduto = new \Zend\Form\Element\Text('pesquisarProduto');
        $pesquisaProduto->setLabel('Produto *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-5'])
                ->setAttribute('autocomplete', 'off')
                ->setAttribute('class', 'campotexto th-produto');
        $this->add($pesquisaProduto);

        $pesquisaCfop = new \Zend\Form\Element\Text('pesquisaCfop');
        $pesquisaCfop->setLabel('CFOP *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-5'])
                ->setAttribute('autocomplete', 'off')
                ->setAttribute('class', 'campotexto th-cfop');
        $this->add($pesquisaCfop);

        $pesquisaUnidadeMedida = new \Zend\Form\Element\Text('pesquisaUnidadeMedida');
        $pesquisaUnidadeMedida->setLabel('Unidade de Medida *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('autocomplete', 'off')
                ->setAttribute('class', 'campotexto th-unidadeMedida');
        $this->add($pesquisaUnidadeMedida);

        $quantidade = new \Zend\Form\Element\Text('quantidade');
        $quantidade->setLabel('Quantidade')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('class', 'campotexto');
        $this->add($quantidade);

        $valorUnitario = new \Zend\Form\Element\Text('valorUnitario');
        $valorUnitario->setLabel('Valor unitário')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('class', 'campotexto dinheiro');
        $this->add($valorUnitario);

        $desconto = new \Zend\Form\Element\Text('desconto');
        $desconto->setLabel('Valor Desconto')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('class', 'campotexto dinheiro');
        $this->add($desconto);

        $valorMinimo = new \Zend\Form\Element\Text('valorMinimo');
        $valorMinimo->setLabel('Mínimo')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('class', 'campotexto dinheiro');
        $this->add($valorMinimo);

        $valorTotal = new \Zend\Form\Element\Text('valorTotal');
        $valorTotal->setLabel('Total')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('class', 'campotexto dinheiro');
        $this->add($valorTotal);


        $aliquotaIcms = new \Zend\Form\Element\Text('aliquotaIcms');
        $aliquotaIcms->setLabel('ICMS %')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('class', 'campotexto dinheiro');
        $this->add($aliquotaIcms);

        $valorIcms = new \Zend\Form\Element\Text('valorIcms');
        $valorIcms->setLabel('ICMS R$')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('disabled', 'disabled')
                ->setAttribute('class', 'campotexto dinheiro');
        $this->add($valorIcms);

        $aliquotaIpi = new \Zend\Form\Element\Text('aliquotaIpi');
        $aliquotaIpi->setLabel('IPI %')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('class', 'campotexto dinheiro');
        $this->add($aliquotaIpi);

        $valorIpi = new \Zend\Form\Element\Text('valorIpi');
        $valorIpi->setLabel('IPI R$')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('disabled', 'disabled')
                ->setAttribute('class', 'campotexto dinheiro');
        $this->add($valorIpi);

        $observacao = new \Zend\Form\Element\Text('observacao');
        $observacao->setLabel('Observação')
                ->setLabelAttributes(['class' => 'labeltexto col-md-12'])
                ->setAttribute('class', 'campotexto');
        $this->add($observacao);
    }

}
