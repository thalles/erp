<?php

namespace App\Form\Entrada;

use App\Form\AbstractForm;

/**
 * Description of Produtos
 *
 * @author Midiauai
 */
class EntradaNF extends \App\Form\CargaNF {

    function __construct($name = null, $options = []) {
        parent::__construct($name, $options);

        $movimentoTipo = new \Zend\Form\Element\Hidden('movimentoTipo');
        $movimentoTipo->setAttribute('id', 'movimentoTipo')->setValue(2);
        $this->add($movimentoTipo);

        $destinatario = new \Zend\Form\Element\Hidden('destinatario');
        $destinatario->setAttribute('id', 'destinatario')
                ->setValue(1);
        $this->add($destinatario);

        $enderecoDestinatario = new \Zend\Form\Element\Hidden('enderecoDestinatario');
        $enderecoDestinatario->setAttribute('id', 'enderecoDestinatario')
                ->setValue(1);
        $this->add($enderecoDestinatario);

        $emissor = new \Zend\Form\Element\Hidden('emissor');
        $emissor->setAttribute('id', 'emissor');
        $this->add($emissor);

        $pesquisaEmissor = new \Zend\Form\Element\Text('pesquisaEmissor');
        $pesquisaEmissor->setLabel('Emissor *')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('autocomplete', 'off')
                ->setAttribute('class', 'campotexto th-emissor');
        $this->add($pesquisaEmissor);

        $enderecoEmissor = new \Zend\Form\Element\Select('enderecoEmissor');
        $enderecoEmissor->setLabel('Endereço do Emissor')
                ->setLabelAttributes(['class' => 'labeltexto col-md-6'])
                ->setAttribute('autocomplete', 'off')
                ->setAttribute('class', 'campotexto');
        $this->add($enderecoEmissor);
    }

}
