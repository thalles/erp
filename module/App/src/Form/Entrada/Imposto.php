<?php

namespace App\Form\Entrada;

use App\Form\AbstractForm;

/**
 * Description of Produtos
 *
 * @author Midiauai
 */
class Imposto extends AbstractForm {

    function __construct($idNota = '', $name = null, $options = []) {
        parent::__construct($name, $options);

        $this->setAttributes([
            'method' => 'post',
            'id' => 'form-imposto',
            'action' => '/gestao/entrada/impostos'
        ]);
        $this->setLabelOptions(['Should_create_template' => false]);

        $id = new \Zend\Form\Element\Hidden('id');
        $id->setValue($idNota);
        $this->add($id);

        $baseCalculoIcms = new \Zend\Form\Element\Text('baseCalculoIcms');
        $baseCalculoIcms->setLabel('Base de Calculo do ICMS')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('class', 'campotexto');
        $this->add($baseCalculoIcms);

        $valorIcms = new \Zend\Form\Element\Text('valorIcms');
        $valorIcms->setLabel('Valor do ICMS')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('class', 'campotexto');
        $this->add($valorIcms);

        $baseCalculoIcmsSubstituicao = new \Zend\Form\Element\Text('baseCalculoIcmsSubstituicao');
        $baseCalculoIcmsSubstituicao->setLabel('Base cálc. ICMS Sub.')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('class', 'campotexto dinheiro');
        $this->add($baseCalculoIcmsSubstituicao);

        $valorIcmsSubstituicao = new \Zend\Form\Element\Text('valorIcmsSubstituicao');
        $valorIcmsSubstituicao->setLabel('Valor ICMS Sub.')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('class', 'campotexto dinheiro');
        $this->add($valorIcmsSubstituicao);

        $valorPis = new \Zend\Form\Element\Text('valorPis');
        $valorPis->setLabel('Valor do PIS')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('class', 'campotexto dinheiro');
        $this->add($valorPis);

        $valorCofins = new \Zend\Form\Element\Text('valorCofins');
        $valorCofins->setLabel('Valor do COFINS')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('class', 'campotexto dinheiro');
        $this->add($valorCofins);

        $valorTotalProdutos = new \Zend\Form\Element\Text('valorTotalProdutos');
        $valorTotalProdutos->setLabel('Valor total dos produtos')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('class', 'campotexto dinheiro');
        $this->add($valorTotalProdutos);

        $valorFrete = new \Zend\Form\Element\Text('valorFrete');
        $valorFrete->setLabel('Valor do Frete')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('class', 'campotexto dinheiro');
        $this->add($valorFrete);

        $valorSeguro = new \Zend\Form\Element\Text('valorSeguro');
        $valorSeguro->setLabel('Valor do Seguro')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('class', 'campotexto dinheiro');
        $this->add($valorSeguro);

        $desconto = new \Zend\Form\Element\Text('desconto');
        $desconto->setLabel('Valor do Desconto')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('class', 'campotexto dinheiro');
        $this->add($desconto);

        $valorIpi = new \Zend\Form\Element\Text('valorIpi');
        $valorIpi->setLabel('Valor do IPI')
                ->setLabelAttributes(['class' => 'labeltexto col-md-2'])
                ->setAttribute('class', 'campotexto dinheiro');
        $this->add($valorIpi);

        $outrasDespesasAcessorias = new \Zend\Form\Element\Text('outrasDespesasAcessorias');
        $outrasDespesasAcessorias->setLabel('Outras despesas acessórias')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('class', 'campotexto dinheiro');
        $this->add($outrasDespesasAcessorias);

        $valorTotalNota = new \Zend\Form\Element\Text('valorTotalNota');
        $valorTotalNota->setLabel('Valor total da nota')
                ->setLabelAttributes(['class' => 'labeltexto col-md-3'])
                ->setAttribute('class', 'campotexto dinheiro');
        $this->add($valorTotalNota);
    }

}
