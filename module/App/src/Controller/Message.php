<?php

namespace App\Controller;

trait Message {

    public function setMessageInfo($menssage) {
        $this->getSession()->mensagem = [
            "texto" => $menssage,
            "tipo" => "info"
        ];
    }

    public function setMessageError($menssage) {
        $this->getSession()->mensagem = [
            "texto" => $menssage,
            "tipo" => "danger"
        ];
    }

    public function setMessageSuccess($menssage) {
        $this->getSession()->mensagem = [
            "texto" => $menssage,
            "tipo" => "success"
        ];
    }

    public function setMessageIfNull($menssage, $tipo = "info") {
        if (!isset($this->getSession()->mensagem)) {
            $this->getSession()->mensagem = [
                "texto" => $menssage,
                "tipo" => $tipo,
            ];
        }
    }

}
