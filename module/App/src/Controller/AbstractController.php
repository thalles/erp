<?php

namespace App\Controller;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container as SessionContainer;

/**
 * Description of AbstractControlle
 *
 * @author Thalles Ferreira
 */
abstract class AbstractController extends AbstractActionController {

    use Request;

    use Message;

    protected $em;

    /**
     * @return Service Manager
     */
    protected function getServiceManager() {
        return $this->getEvent()->getApplication()->getServiceManager();
    }

    /**
     * @return Doctrine EntityManager
     */
    protected function getEm(): \Doctrine\ORM\EntityManager {
        if (!isset($this->em)) {
            $this->em = $this->getServiceManager()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    /**
     * @param string $entityName
     * @return Repository Doctrine
     */
    protected function getRepository($entityName): \App\Entity\Repository\AbstractRepository {
        if (!strpos($entityName, 'Entity')) {
            return $this->getEm()->getRepository(trim('App\Entity\\' . $entityName));
        }
        return $this->getEm()->getRepository(trim($entityName));
    }

    protected function getViewHelper($helperName) {
        return $this->getServiceManager()->get('ViewHelperManager')->get($helperName);
    }

    /**
     * Pega o nome da rota específica
     * @return string
     * @example $this->getRota();
     */
    public function getRota() {
        $event = $this->getEvent();
        return $event->getRouteMatch()->getMatchedRouteName();
    }

    /**
     * add a new file javascript on layout
     *
     * @param string $fileAdress
     * @return void
     * @example $this->addJsFile('adress-of-file.js');
     */
    protected function addJsFile($fileAdress) {
        $this->getViewHelper('HeadScript')->appendFile($fileAdress);
    }

    /**
     * add a new file javascript on layout
     *
     * @param string $fileAdress
     * @return void
     * @example $this->addCssFile('adress-of-file.css');
     */
    protected function addCssFile($fileAdress) {
        $this->getViewHelper('HeadLink')->prependStylesheet($fileAdress);
    }

    protected function setHeadTitle($titlePage) {
        $this->getViewHelper('HeadTitle')->set($titlePage);
    }

    public function getIdentity() {
        $authService = new AuthenticationService();
        $authService->setStorage(new Session('Usuario'));
        return $authService->getIdentity();
    }

    public function getSession() {
        return new SessionContainer();
    }

}
