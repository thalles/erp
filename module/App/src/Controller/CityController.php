<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonApp for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace App\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class CityController extends AbstractController {

    public function indexAction() {

        return new ViewModel();
    }

    public function listaUfAction() {

        $cidades = $this->getRepository("Cidade")->findBy(['estado' => $this->getPost('uf')], ['nome' => 'asc']);
        $opcoes = "";
        foreach ($cidades as $cidade) {
            $opcoes .= "<option value='" . $cidade->getId() . "'>" . $cidade->getNome() . "</option>";
        }

        return new JsonModel(['option' => $opcoes]);
    }

}

//Fatal error: Uncaught Error: Call to a member function getParam() on null in
//C:\inetpub\wwwroot\brasao\erp\module\App\src\View\Helper\GetMenuOfModule.php:21 Stack trace:
//#0 [internal function]: App\View\Helper\GetMenuOfModule->__invoke()
//#1 C:\inetpub\wwwroot\brasao\erp\vendor\zendframework\zend-view\src\Renderer\PhpRenderer.php(397): call_user_func_array(Object(App\View\Helper\GetMenuOfModule), Array)
//#2 C:\inetpub\wwwroot\brasao\erp\module\App\view\layout\layout.phtml(334): Zend\View\Renderer\PhpRenderer->__call('getMenuOfModule', Array)
//#3 C:\inetpub\wwwroot\brasao\erp\vendor\zendframework\zend-view\src\Renderer\PhpRenderer.php(505): include('C:\\inetpub\\wwwr...')
//#4 C:\inetpub\wwwroot\brasao\erp\vendor\zendframework\zend-view\src\View.php(207): Zend\View\Renderer\PhpRenderer->render()
//#5 C:\inetpub\wwwroot\brasao\erp\vendor\zendframework\zend-mvc\src\View\Http\DefaultRenderingStrategy.php(105): Zend\View\View->render(Object(Zend\View\Model\ViewModel))
//#6 C:\inetpub\wwwroot\brasao\erp\vendor\zendframework\zend-eventma in C:\inetpub\wwwroot\brasao\erp\module\App\src\View\Helper\GetMenuOfModule.php on line 21
