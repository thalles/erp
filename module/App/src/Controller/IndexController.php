<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonApp for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace App\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use App\Form\Fisica as formPessoa;

class IndexController extends AbstractController {

    public function indexAction() {
        // echo '<pre>';
        // $juridica = $this->getRepository("Fisica")->find(1);
        // print_r($juridica->toArray());
        // exit; 
        return new ViewModel();
    }

}
