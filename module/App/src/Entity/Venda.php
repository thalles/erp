<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Venda
 *
 * @ORM\Table(name="venda", indexes={@ORM\Index(name="fk_pedido_pessoa1_idx", columns={"cliente"}), @ORM\Index(name="fk_venda_estatus_venda1_idx", columns={"estatus_venda"}), @ORM\Index(name="fk_venda_tipo_pagamento1_idx", columns={"tipo_pagamento"}), @ORM\Index(name="fk_venda_pessoa1_idx", columns={"vendedor"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Venda")
 */
class Venda extends \App\Entity\AbstractEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_venda", type="datetime", nullable=true)
     */
    private $dataVenda;

    /**
     * @var float
     *
     * @ORM\Column(name="a_vista", type="float", precision=10, scale=0, nullable=true)
     */
    private $aVista;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantidade_parcelas", type="integer", nullable=true)
     */
    private $quantidadeParcelas;

    /**
     * @var \App\Entity\Pessoa
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cliente", referencedColumnName="id")
     * })
     */
    private $cliente;

    /**
     * @var \App\Entity\EstatusVenda
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\EstatusVenda")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="estatus_venda", referencedColumnName="id")
     * })
     */
    private $estatusVenda;

    /**
     * @var \App\Entity\TipoPagamento
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoPagamento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_pagamento", referencedColumnName="id")
     * })
     */
    private $tipoPagamento;

    /**
     * @var \App\Entity\Pessoa
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vendedor", referencedColumnName="id")
     * })
     */
    private $vendedor;

    /**
     * One Vneda has Many Produtos.
     * @ORM\OneToMany(targetEntity="App\Entity\ProdutoVenda", mappedBy="venda")
     */
    private $produtoVenda;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Produto", inversedBy="venda")
     * @ORM\JoinTable(name="produto_venda",
     *   joinColumns={
     *     @ORM\JoinColumn(name="venda", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="produto", referencedColumnName="id")
     *   }
     * )
     */
    private $produto;

    /**
     *
     * @var string
     */
    private $pesquisaVendedor;

    /**
     *
     * @var string
     */
    private $pesquisaCliente;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set dataVenda
     *
     * @param \DateTime $dataVenda
     *
     * @return Venda
     */
    public function setDataVenda($dataVenda) {
        $this->dataVenda = $dataVenda;

        return $this;
    }

    /**
     * Get dataVenda
     *
     * @return \DateTime
     */
    public function getDataVenda() {
        return $this->dataVenda;
    }

    /**
     * Set aVista
     *
     * @param float $aVista
     *
     * @return Venda
     */
    public function setAVista($aVista) {
        $this->aVista = $aVista;

        return $this;
    }

    /**
     * Get aVista
     *
     * @return float
     */
    public function getAVista() {
        return $this->aVista;
    }

    /**
     * Set quantidadeParcelas
     *
     * @param integer $quantidadeParcelas
     *
     * @return Venda
     */
    public function setQuantidadeParcelas($quantidadeParcelas) {
        $this->quantidadeParcelas = $quantidadeParcelas;

        return $this;
    }

    /**
     * Get quantidadeParcelas
     *
     * @return integer
     */
    public function getQuantidadeParcelas() {
        return $this->quantidadeParcelas;
    }

    /**
     * Set vendedor
     *
     * @param \App\Entity\Pessoa $vendedor
     *
     * @return Venda
     */
    public function setVendedor($vendedor = null) {
        $this->vendedor = $vendedor;

        return $this;
    }

    /**
     * Get vendedor
     *
     * @return \App\Entity\Fisica
     */
    public function getVendedor() {
        return $this->vendedor;
    }

    /**
     * Set cliente
     *
     * @param \App\Entity\Pessoa $cliente
     *
     * @return Venda
     */
    public function setCliente($cliente = null) {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \App\Entity\Pessoa
     */
    public function getCliente() {
        return $this->cliente;
    }

    /**
     * Set estatusVenda
     *
     * @param \App\Entity\EstatusVenda $estatusVenda
     *
     * @return Venda
     */
    public function setEstatusVenda($estatusVenda = null) {
        $this->estatusVenda = $estatusVenda;

        return $this;
    }

    /**
     * Get estatusVenda
     *
     * @return \App\Entity\EstatusVenda
     */
    public function getEstatusVenda() {
        return $this->estatusVenda;
    }

    /**
     * Set tipoPagamento
     *
     * @param \App\Entity\TipoPagamento $tipoPagamento
     *
     * @return Venda
     */
    public function setTipoPagamento($tipoPagamento = null) {
        $this->tipoPagamento = $tipoPagamento;

        return $this;
    }

    /**
     * Get tipoPagamento
     *
     * @return \App\Entity\TipoPagamento
     */
    public function getTipoPagamento() {
        return $this->tipoPagamento;
    }

    function getProdutoVenda() {
        return $this->produtoVenda;
    }

    function getProduto(): \Doctrine\Common\Collections\Collection {
        return $this->produto;
    }

    function getPesquisaVendedor() {
        return $this->vendedor->getNome();
    }

    function getPesquisaCliente() {
        if ($this->cliente)
            return $this->cliente->getNome();
        return null;
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

}
