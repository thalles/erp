<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bandeira
 *
 * @ORM\Table(name="bandeira")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Bandeira")
 */
class Bandeira extends \App\Entity\AbstractEntity
{   /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=20, nullable=true)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="banco_credito", type="string", length=45, nullable=true)
     */
    private $bancoCredito;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Bandeira
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set bancoCredito
     *
     * @param string $bancoCredito
     *
     * @return Bandeira
     */
    public function setBancoCredito($bancoCredito)
    {
        $this->bancoCredito = $bancoCredito;

        return $this;
    }

    /**
     * Get bancoCredito
     *
     * @return string
     */
    public function getBancoCredito()
    {
        return $this->bancoCredito;
    }
}
