<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Produto
 *
 * @ORM\Table(name="produto", indexes={
 *  @ORM\Index(name="fk_produto_mercadoria1_idx", columns={"mercadoria"}),
 *  @ORM\Index(name="fk_produto_marca1_idx", columns={"marca"})
 * })
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Produto")
 */
class Produto extends AbstractEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="altura", type="string", length=20, nullable=true)
     */
    private $altura;

    /**
     * @var string
     *
     * @ORM\Column(name="largura", type="string", length=20, nullable=true)
     */
    private $largura;

    /**
     * @var string
     *
     * @ORM\Column(name="profundidade", type="string", length=20, nullable=true)
     */
    private $profundidade;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_atual", type="float", precision=10, scale=0, nullable=false)
     */
    private $valorAtual;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=150, nullable=true)
     */
    private $descricao;

    /**
     * @var string
     *
     * @ORM\Column(name="informacao_adicional", type="string", length=200, nullable=true)
     */
    private $informacaoAdicional;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_fabrica", type="string", length=9, nullable=true)
     */
    private $codigoFabrica;

    /**
     * @var \App\Entity\Marca
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Marca")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="marca", referencedColumnName="id")
     * })
     */
    private $marca;

    /**
     * @var \App\Entity\Mercadoria
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Mercadoria")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mercadoria", referencedColumnName="id")
     * })
     */
    private $mercadoria;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Imagem", mappedBy="produto")
     */
    private $imagem;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set altura
     *
     * @param string $altura
     *
     * @return Produto
     */
    public function setAltura($altura) {
        $this->altura = $altura;

        return $this;
    }

    /**
     * Get altura
     *
     * @return string
     */
    public function getAltura() {
        return (float) $this->altura;
    }

    /**
     * Set largura
     *
     * @param string $largura
     *
     * @return Produto
     */
    public function setLargura($largura) {
        $this->largura = $largura;

        return $this;
    }

    /**
     * Get largura
     *
     * @return string
     */
    public function getLargura() {
        return (float) $this->largura;
    }

########################################################################################
    /**
     * Set profundidade
     *
     * @param string $profundidade
     *
     * @return Produto
     */

    public function setProfundidade($profundidade) {
        $this->profundidade = $profundidade;

        return $this;
    }

    /**
     * Get profundidade
     *
     * @return string
     */
    public function getProfundidade() {
        return (float) $this->profundidade;
    }

########################################################################################
    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return Produto
     */

    public function setDescricao($descricao) {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao() {
        return $this->descricao;
    }

########################################################################################
    /**
     * Set informacaoAdicional
     *
     * @param string $informacaoAdicional
     *
     * @return Produto
     */

    public function setInformacaoAdicional($adicional) {
        $this->informacaoAdicional = $adicional;

        return $this;
    }

    /**
     * Get informacaoAdicional
     *
     * @return string
     */
    public function getInformacaoAdicional() {
        return $this->informacaoAdicional;
    }

################################################################################
    /**
     * Set codigoFabrica
     *
     * @param string $codigoFabrica
     *
     * @return Produto
     */

    public function setCodigoFabrica($codigoFabrica) {
        $this->codigoFabrica = $codigoFabrica;
        return $this;
    }

    /**
     * Get codigoFabrica
     *
     * @return string
     */
    public function getCodigoFabrica() {
        return $this->codigoFabrica;
    }

################################################################################

    public function getDimensoes() {
        return $this->getAltura() . ' X ' . $this->getLargura() . ' X ' . $this->getProfundidade() . '(A X L X P) ';
    }

    /**
     * Set valorAtual
     *
     * @param float $valorAtual
     *
     * @return Produto
     */
    public function setValorAtual($valorAtual) {
        $this->valorAtual = $valorAtual;

        return $this;
    }

    /**
     * Get valorAtual
     *
     * @return float
     */
    public function getValorAtual() {
        return $this->valorAtual;
    }

    /**
     * Set marca
     *
     * @param \App\Entity\Marca $marca
     *
     * @return Produto
     */
    public function setMarca($marca = null) {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca
     *
     * @return \App\Entity\Marca
     */
    public function getMarca() {
        return $this->marca;
    }

    /**
     * Get marcaNome
     *
     * @return string
     */
    public function getMarcaNome() {
        return $this->marca->getNome();
    }

    /**
     * Set mercadoria
     *
     * @param \App\Entity\Mercadoria $mercadoria
     *
     * @return Produto
     */
    public function setMercadoria($mercadoria = null) {
        $this->mercadoria = $mercadoria;

        return $this;
    }

    /**
     * Get mercadoria
     *
     * @return \App\Entity\Mercadoria
     */
    public function getMercadoria() {
        return $this->mercadoria;
    }

    function getMercadoriaDescricao() {
        return $this->getMercadoria()->getDescricao();
    }

    /**
     * Add imagem
     *
     * @param \App\Entity\Imagem $imagem
     *
     * @return Produto
     */
    public function addImagem(\App\Entity\Imagem $imagem) {
        $this->imagem[] = $imagem;

        return $this;
    }

    /**
     * Remove imagem
     *
     * @param \App\Entity\Imagem $imagem
     */
    public function removeImagem(\App\Entity\Imagem $imagem) {
        $this->imagem->removeElement($imagem);
    }

    /**
     * Get imagem
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImagem() {
        return $this->imagem;
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

}
