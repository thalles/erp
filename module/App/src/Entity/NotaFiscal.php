<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NotaFiscal
 *
 * @ORM\Table(name="nota_fiscal", indexes={@ORM\Index(name="fk_nota_fiscal_natureza_operacao1_idx", columns={"natureza_operacao"}),
 *      @ORM\Index(name="fk_nota_fiscal_movimento_tipo1_idx", columns={"movimento_tipo"}),
 *      @ORM\Index(name="fk_nota_fiscal_pessoa1_idx", columns={"emissor"}),
 *      @ORM\Index(name="fk_nota_fiscal_endereco1_idx", columns={"endereco_emissor"}),
 *      @ORM\Index(name="fk_nota_fiscal_pessoa2_idx", columns={"destinatario"}),
 *      @ORM\Index(name="fk_nota_fiscal_endereco2_idx", columns={"endereco_destinatario"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\NotaFiscal")
 */
class NotaFiscal extends AbstractEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=45, nullable=false)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="serie", type="string", length=5, nullable=false)
     */
    private $serie;

    /**
     * @var string
     *
     * @ORM\Column(name="chave_acesso", type="string", length=50, nullable=false)
     */
    private $chaveAcesso;

    /**
     * @var string
     *
     * @ORM\Column(name="dados_adicionais", type="text", length=65535, nullable=true)
     */
    private $dadosAdicionais;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_emissao", type="datetime", nullable=true)
     */
    private $dataEmissao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_saida", type="date", nullable=true)
     */
    private $dataSaida;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hora_saida", type="time", nullable=true)
     */
    private $horaSaida;

    /**
     * @var float
     *
     * @ORM\Column(name="base_calculo_icms", type="float", precision=10, scale=0, nullable=false)
     */
    private $baseCalculoIcms;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_icms", type="float", precision=10, scale=0, nullable=false)
     */
    private $valorIcms;

    /**
     * @var float
     *
     * @ORM\Column(name="base_calculo_icms_substituicao", type="float", precision=10, scale=0, nullable=false)
     */
    private $baseCalculoIcmsSubstituicao;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_pis", type="float", precision=10, scale=0, nullable=false)
     */
    private $valorPis;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_cofins", type="float", precision=10, scale=0, nullable=false)
     */
    private $valorCofins;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_total_produtos", type="float", precision=10, scale=0, nullable=false)
     */
    private $valorTotalProdutos;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_frete", type="float", precision=10, scale=0, nullable=false)
     */
    private $valorFrete;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_seguro", type="float", precision=10, scale=0, nullable=false)
     */
    private $valorSeguro;

    /**
     * @var float
     *
     * @ORM\Column(name="desconto", type="float", precision=10, scale=0, nullable=false)
     */
    private $desconto;

    /**
     * @var float
     *
     * @ORM\Column(name="outras_despesas_acessorias", type="float", precision=10, scale=0, nullable=false)
     */
    private $outrasDespesasAcessorias;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_ipi", type="float", precision=10, scale=0, nullable=false)
     */
    private $valorIpi;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_total_nota", type="float", precision=10, scale=0, nullable=false)
     */
    private $valorTotalNota;

    /**
     * One Carga has One Nota Fiscal.
     * @ORM\OneToOne(targetEntity="App\Entity\Carga", mappedBy="notaFiscal")
     */
    private $carga;

    /**
     * @var \App\Entity\Endereco
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Endereco")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="endereco_emissor", referencedColumnName="id")
     * })
     */
    private $enderecoEmissor;

    /**
     * @var \App\Entity\Endereco
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Endereco")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="endereco_destinatario", referencedColumnName="id")
     * })
     */
    private $enderecoDestinatario;

    /**
     * @var \App\Entity\MovimentoTipo
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\MovimentoTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="movimento_tipo", referencedColumnName="id")
     * })
     */
    private $movimentoTipo;

    /**
     * @var \App\Entity\NaturezaOperacao
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\NaturezaOperacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="natureza_operacao", referencedColumnName="id")
     * })
     */
    private $naturezaOperacao;

    /**
     * @var \App\Entity\Pessoa
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="emissor", referencedColumnName="id")
     * })
     */
    private $emissor;

    /**
     * @var \App\Entity\Pessoa
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="destinatario", referencedColumnName="id")
     * })
     */
    private $destinatario;

    /**
     * One NotaFiscal has Many MovimentoProduto.
     * @ORM\OneToMany(targetEntity="App\Entity\MovimentoProduto", mappedBy="notaFiscal")
     */
    private $movimentoProduto;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param string $numero
     *
     * @return NotaFiscal
     */
    public function setNumero($numero) {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero() {
        return $this->numero;
    }

    /**
     * Set chaveAcesso
     *
     * @param string $chaveAcesso
     *
     * @return NotaFiscal
     */
    public function setChaveAcesso($chaveAcesso) {
        $this->chaveAcesso = $chaveAcesso;

        return $this;
    }

    /**
     * Get chaveAcesso
     *
     * @return string
     */
    public function getChaveAcesso() {
        return $this->chaveAcesso;
    }

    /**
     * Set dadosAdicionais
     *
     * @param string $dadosAdicionais
     *
     * @return NotaFiscal
     */
    public function setDadosAdicionais($dadosAdicionais) {
        $this->dadosAdicionais = $dadosAdicionais;

        return $this;
    }

    /**
     * Get dadosAdicionais
     *
     * @return string
     */
    public function getDadosAdicionais() {
        return $this->dadosAdicionais;
    }

    /**
     * Set dataEmissao
     *
     * @param \DateTime $dataEmissao
     *
     * @return NotaFiscal
     */
    public function setDataEmissao($dataEmissao) {
        $this->dataEmissao = $this->getDateObject($dataEmissao);

        return $this;
    }

    /**
     * Get dataEmissao
     *
     * @return \DateTime
     */
    public function getDataEmissao() {
        return $this->dataEmissao;
    }

    /**
     * Set dataSaida
     *
     * @param \DateTime $dataSaida
     *
     * @return NotaFiscal
     */
    public function setDataSaida($dataSaida) {
        $this->dataSaida = $this->getDateObject($dataSaida);

        return $this;
    }

    /**
     * Get dataSaida
     *
     * @return \DateTime
     */
    public function getDataSaida() {
        return $this->dataSaida;
    }

    /**
     * Set horaSaida
     *
     * @param \DateTime $horaSaida
     *
     * @return NotaFiscal
     */
    public function setHoraSaida($horaSaida) {
        $this->horaSaida = \DateTime::createFromFormat('H:i', $horaSaida);

        return $this;
    }

    /**
     * Get horaSaida
     *
     * @return \DateTime
     */
    public function getHoraSaida() {
        return $this->horaSaida;
    }

    public function getBaseCalculoIcms() {
        return $this->baseCalculoIcms;
    }

    public function getValorIcms() {
        return $this->valorIcms;
    }

    public function getBaseCalculoIcmsSubstituicao() {
        return $this->baseCalculoIcmsSubstituicao;
    }

    public function getValorPis() {
        return $this->valorPis;
    }

    public function getValorCofins() {
        return $this->valorCofins;
    }

    public function getValorTotalProdutos() {
        return $this->valorTotalProdutos;
    }

    public function getValorFrete() {
        return $this->valorFrete;
    }

    public function getValorSeguro() {
        return $this->valorSeguro;
    }

    public function getDesconto() {
        return $this->desconto;
    }

    public function getOutrasDespesasAcessorias() {
        return $this->outrasDespesasAcessorias;
    }

    public function getValorIpi() {
        return $this->valorIpi;
    }

    public function getValorTotalNota() {
        return $this->valorTotalNota;
    }

    public function setBaseCalculoIcms($baseCalculoIcms) {
        $this->baseCalculoIcms = $baseCalculoIcms;
        return $this;
    }

    public function setValorIcms($valorIcms) {
        $this->valorIcms = $valorIcms;
        return $this;
    }

    public function setBaseCalculoIcmsSubstituicao($baseCalculoIcmsSubstituicao) {
        $this->baseCalculoIcmsSubstituicao = $baseCalculoIcmsSubstituicao;
        return $this;
    }

    public function setValorPis($valorPis) {
        $this->valorPis = $valorPis;
        return $this;
    }

    public function setValorCofins($valorCofins) {
        $this->valorCofins = $valorCofins;
        return $this;
    }

    public function setValorTotalProdutos($valorTotalProdutos) {
        $this->valorTotalProdutos = $valorTotalProdutos;
        return $this;
    }

    public function setValorFrete($valorFrete) {
        $this->valorFrete = $valorFrete;
        return $this;
    }

    public function setValorSeguro($valorSeguro) {
        $this->valorSeguro = $valorSeguro;
        return $this;
    }

    public function setDesconto($desconto) {
        $this->desconto = $desconto;
        return $this;
    }

    public function setOutrasDespesasAcessorias($outrasDespesasAcessorias) {
        $this->outrasDespesasAcessorias = $outrasDespesasAcessorias;
        return $this;
    }

    public function setValorIpi($valorIpi) {
        $this->valorIpi = $valorIpi;
        return $this;
    }

    public function setValorTotalNota($valorTotalNota) {
        $this->valorTotalNota = $valorTotalNota;
        return $this;
    }

    /**
     * Get carga
     *
     * @return \App\Entity\Carga
     */
    public function getCarga() {
        return $this->carga;
    }

    /**
     * Set enderecoEmissor
     *
     * @param \App\Entity\Endereco $enderecoEmissor
     *
     * @return NotaFiscal
     */
    public function setEnderecoEmissor($enderecoEmissor = null) {
        $this->enderecoEmissor = $enderecoEmissor;
        return $this;
    }

    /**
     * Get enderecoEmissor
     *
     * @return \App\Entity\Endereco
     */
    public function getEnderecoEmissor() {
        return $this->enderecoEmissor;
    }

    /**
     * Set enderecoDestinatario
     *
     * @param \App\Entity\Endereco $enderecoDestinatario
     *
     * @return NotaFiscal
     */
    public function setEnderecoDestinatario($enderecoDestinatario = null) {
        $this->enderecoDestinatario = $enderecoDestinatario;

        return $this;
    }

    /**
     * Get enderecoDestinatario
     *
     * @return \App\Entity\Endereco
     */
    public function getEnderecoDestinatario() {
        return $this->enderecoDestinatario;
    }

    /**
     * Set movimentoTipo
     *
     * @param \App\Entity\MovimentoTipo $movimentoTipo
     *
     * @return NotaFiscal
     */
    public function setMovimentoTipo($movimentoTipo = null) {
        $this->movimentoTipo = $movimentoTipo;

        return $this;
    }

    /**
     * Get movimentoTipo
     *
     * @return \App\Entity\MovimentoTipo
     */
    public function getMovimentoTipo() {
        return $this->movimentoTipo;
    }

    /**
     * Set naturezaOperacao
     *
     * @param \App\Entity\NaturezaOperacao $naturezaOperacao
     *
     * @return NotaFiscal
     */
    public function setNaturezaOperacao($naturezaOperacao = null) {
        $this->naturezaOperacao = $naturezaOperacao;

        return $this;
    }

    /**
     * Get naturezaOperacao
     *
     * @return \App\Entity\NaturezaOperacao
     */
    public function getNaturezaOperacao() {
        return $this->naturezaOperacao;
    }

    /**
     * Set emissor
     *
     * @param \App\Entity\Pessoa $emissor
     *
     * @return NotaFiscal
     */
    public function setEmissor($emissor = null) {
        $this->emissor = $emissor;

        return $this;
    }

    /**
     * Get emissor
     *
     * @return \App\Entity\Pessoa
     */
    public function getEmissor() {
        return $this->emissor;
    }

    /**
     * Set destinatario
     *
     * @param \App\Entity\Pessoa $destinatario
     *
     * @return NotaFiscal
     */
    public function setDestinatario($destinatario = null) {
        $this->destinatario = $destinatario;

        return $this;
    }

    /**
     * Get destinatario
     *
     * @return \App\Entity\Pessoa
     */
    public function getDestinatario() {
        return $this->destinatario;
    }

    public function getSerie() {
        return $this->serie;
    }

    public function setSerie($serie) {
        $this->serie = $serie;
        return $this;
    }

    public function getMovimentoProduto() {
        return $this->movimentoProduto;
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

}
