<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cartao
 *
 * @ORM\Table(name="cartao", indexes={@ORM\Index(name="fk_cartao_caixa1_idx", columns={"caixa"}), @ORM\Index(name="fk_cartao_venda1_idx", columns={"venda"}), @ORM\Index(name="fk_cartao_bandeira1_idx", columns={"bandeira"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Cartao")
 */
class Cartao extends \App\Entity\AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="entrada_caixa", type="integer", nullable=false)
     */
    private $entradaCaixa;

    /**
     * @var string
     *
     * @ORM\Column(name="protocolo", type="string", length=45, nullable=true)
     */
    private $protocolo;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantidade_parcelas", type="integer", nullable=true)
     */
    private $quantidadeParcelas;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_parcelas", type="float", precision=10, scale=0, nullable=true)
     */
    private $valorParcelas;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=1, nullable=true)
     */
    private $tipo;

    /**
     * @var \App\Entity\Caixa
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Caixa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="caixa", referencedColumnName="id")
     * })
     */
    private $caixa;

    /**
     * @var \App\Entity\Venda
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Venda")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="venda", referencedColumnName="id")
     * })
     */
    private $venda;

    /**
     * @var \App\Entity\Bandeira
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Bandeira")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bandeira", referencedColumnName="id")
     * })
     */
    private $bandeira;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entradaCaixa
     *
     * @param integer $entradaCaixa
     *
     * @return Cartao
     */
    public function setEntradaCaixa($entradaCaixa)
    {
        $this->entradaCaixa = $entradaCaixa;

        return $this;
    }

    /**
     * Get entradaCaixa
     *
     * @return integer
     */
    public function getEntradaCaixa()
    {
        return $this->entradaCaixa;
    }

    /**
     * Set protocolo
     *
     * @param string $protocolo
     *
     * @return Cartao
     */
    public function setProtocolo($protocolo)
    {
        $this->protocolo = $protocolo;

        return $this;
    }

    /**
     * Get protocolo
     *
     * @return string
     */
    public function getProtocolo()
    {
        return $this->protocolo;
    }

    /**
     * Set quantidadeParcelas
     *
     * @param integer $quantidadeParcelas
     *
     * @return Cartao
     */
    public function setQuantidadeParcelas($quantidadeParcelas)
    {
        $this->quantidadeParcelas = $quantidadeParcelas;

        return $this;
    }

    /**
     * Get quantidadeParcelas
     *
     * @return integer
     */
    public function getQuantidadeParcelas()
    {
        return $this->quantidadeParcelas;
    }

    /**
     * Set valorParcelas
     *
     * @param float $valorParcelas
     *
     * @return Cartao
     */
    public function setValorParcelas($valorParcelas)
    {
        $this->valorParcelas = $valorParcelas;

        return $this;
    }

    /**
     * Get valorParcelas
     *
     * @return float
     */
    public function getValorParcelas()
    {
        return $this->valorParcelas;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Cartao
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set caixa
     *
     * @param \App\Entity\Caixa $caixa
     *
     * @return Cartao
     */
    public function setCaixa(\App\Entity\Caixa $caixa = null)
    {
        $this->caixa = $caixa;

        return $this;
    }

    /**
     * Get caixa
     *
     * @return \App\Entity\Caixa
     */
    public function getCaixa()
    {
        return $this->caixa;
    }

    /**
     * Set venda
     *
     * @param \App\Entity\Venda $venda
     *
     * @return Cartao
     */
    public function setVenda(\App\Entity\Venda $venda = null)
    {
        $this->venda = $venda;

        return $this;
    }

    /**
     * Get venda
     *
     * @return \App\Entity\Venda
     */
    public function getVenda()
    {
        return $this->venda;
    }

    /**
     * Set bandeira
     *
     * @param \App\Entity\Bandeira $bandeira
     *
     * @return Cartao
     */
    public function setBandeira(\App\Entity\Bandeira $bandeira = null)
    {
        $this->bandeira = $bandeira;

        return $this;
    }

    /**
     * Get bandeira
     *
     * @return \App\Entity\Bandeira
     */
    public function getBandeira()
    {
        return $this->bandeira;
    }
    
    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }
}