<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contrato
 *
 * @ORM\Table(name="contrato", indexes={@ORM\Index(name="fk_contrato_credito1_idx", columns={"credito"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Contrato")
 */
class Contrato extends \App\Entity\AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=45, nullable=true)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="protocolo_autorizacao", type="string", length=45, nullable=true)
     */
    private $protocoloAutorizacao;

    /**
     * @var \App\Entity\Credito
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Credito")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="credito", referencedColumnName="id")
     * })
     */
    private $credito;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param string $numero
     *
     * @return Contrato
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set protocoloAutorizacao
     *
     * @param string $protocoloAutorizacao
     *
     * @return Contrato
     */
    public function setProtocoloAutorizacao($protocoloAutorizacao)
    {
        $this->protocoloAutorizacao = $protocoloAutorizacao;

        return $this;
    }

    /**
     * Get protocoloAutorizacao
     *
     * @return string
     */
    public function getProtocoloAutorizacao()
    {
        return $this->protocoloAutorizacao;
    }

    /**
     * Set credito
     *
     * @param \App\Entity\Credito $credito
     *
     * @return Contrato
     */
    public function setCredito($credito = null)
    {
        $this->credito = $credito;

        return $this;
    }

    /**
     * Get credito
     *
     * @return \App\Entity\Credito
     */
    public function getCredito()
    {
        return $this->credito;
    }
    
    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }
}