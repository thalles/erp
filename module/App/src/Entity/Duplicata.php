<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Duplicata
 *
 * @ORM\Table(name="duplicata", indexes={@ORM\Index(name="fk_duplicata_nota_fiscal1_idx", columns={"nota_fiscal"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Duplicata")
 */
class Duplicata extends \App\Entity\AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=10, nullable=false)
     */
    private $numero;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="vencimento", type="date", nullable=false)
     */
    private $vencimento;

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float", precision=10, scale=0, nullable=false)
     */
    private $valor;

    /**
     * @var \App\Entity\NotaFiscal
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\NotaFiscal")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="nota_fiscal", referencedColumnName="id")
     * })
     */
    private $notaFiscal;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param string $numero
     *
     * @return Duplicata
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set vencimento
     *
     * @param \DateTime $vencimento
     *
     * @return Duplicata
     */
    public function setVencimento($vencimento)
    {
        $this->vencimento = $vencimento;

        return $this;
    }

    /**
     * Get vencimento
     *
     * @return \DateTime
     */
    public function getVencimento()
    {
        return $this->vencimento;
    }

    /**
     * Set valor
     *
     * @param float $valor
     *
     * @return Duplicata
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set notaFiscal
     *
     * @param \App\Entity\NotaFiscal $notaFiscal
     *
     * @return Duplicata
     */
    public function setNotaFiscal(\App\Entity\NotaFiscal $notaFiscal = null)
    {
        $this->notaFiscal = $notaFiscal;

        return $this;
    }

    /**
     * Get notaFiscal
     *
     * @return \App\Entity\NotaFiscal
     */
    public function getNotaFiscal()
    {
        return $this->notaFiscal;
    }
    
    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }
}