<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mercadoria
 *
 * @ORM\Table(name="mercadoria", indexes={@ORM\Index(name="fk_mercadoria_categoria1_idx", columns={"categoria"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Mercadoria")
 */
class Mercadoria extends \App\Entity\AbstractEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=100, nullable=true)
     */
    private $descricao;

    /**
     * @var \App\Entity\Categoria
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categoria", referencedColumnName="id")
     * })
     */
    private $categoria;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    public function getId2() {
        return $this->id;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return Mercadoria
     */
    public function setDescricao($descricao) {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao() {
        return $this->descricao;
    }

    /**
     * Set categoria
     *
     * @param \App\Entity\Categoria $categoria
     *
     * @return Mercadoria
     */
    public function setCategoria($categoria = null) {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \App\Entity\Categoria
     */
    public function getCategoria() {
        return $this->categoria;
    }

    /**
     * Get NomesVariaveis
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

}
