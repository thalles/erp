<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Parcelas
 *
 * @ORM\Table(name="parcelas", indexes={@ORM\Index(name="fk_parcelas_crediario1_idx", columns={"crediario"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Parcelas")
 */
class Parcelas extends \App\Entity\AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="vencimento", type="date", nullable=true)
     */
    private $vencimento;

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float", precision=10, scale=0, nullable=true)
     */
    private $valor;

    /**
     * @var float
     *
     * @ORM\Column(name="desconto", type="float", precision=10, scale=0, nullable=true)
     */
    private $desconto = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="juros", type="float", precision=10, scale=0, nullable=true)
     */
    private $juros = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="multa", type="float", precision=10, scale=0, nullable=true)
     */
    private $multa = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="sequencia", type="integer", nullable=true)
     */
    private $sequencia;

    /**
     * @var float
     *
     * @ORM\Column(name="abatimento", type="float", precision=10, scale=0, nullable=true)
     */
    private $abatimento = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=1, nullable=true)
     */
    private $status;

    /**
     * @var \App\Entity\Crediario
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Crediario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="crediario", referencedColumnName="id")
     * })
     */
    private $crediario;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vencimento
     *
     * @param \DateTime $vencimento
     *
     * @return Parcelas
     */
    public function setVencimento($vencimento)
    {
        $this->vencimento = $vencimento;

        return $this;
    }

    /**
     * Get vencimento
     *
     * @return \DateTime
     */
    public function getVencimento()
    {
        return $this->vencimento;
    }

    /**
     * Set valor
     *
     * @param float $valor
     *
     * @return Parcelas
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set desconto
     *
     * @param float $desconto
     *
     * @return Parcelas
     */
    public function setDesconto($desconto)
    {
        $this->desconto = $desconto;

        return $this;
    }

    /**
     * Get desconto
     *
     * @return float
     */
    public function getDesconto()
    {
        return $this->desconto;
    }

    /**
     * Set juros
     *
     * @param float $juros
     *
     * @return Parcelas
     */
    public function setJuros($juros)
    {
        $this->juros = $juros;

        return $this;
    }

    /**
     * Get juros
     *
     * @return float
     */
    public function getJuros()
    {
        return $this->juros;
    }

    /**
     * Set multa
     *
     * @param float $multa
     *
     * @return Parcelas
     */
    public function setMulta($multa)
    {
        $this->multa = $multa;

        return $this;
    }

    /**
     * Get multa
     *
     * @return float
     */
    public function getMulta()
    {
        return $this->multa;
    }

    /**
     * Set sequencia
     *
     * @param integer $sequencia
     *
     * @return Parcelas
     */
    public function setSequencia($sequencia)
    {
        $this->sequencia = $sequencia;

        return $this;
    }

    /**
     * Get sequencia
     *
     * @return integer
     */
    public function getSequencia()
    {
        return $this->sequencia;
    }

    /**
     * Set abatimento
     *
     * @param float $abatimento
     *
     * @return Parcelas
     */
    public function setAbatimento($abatimento)
    {
        $this->abatimento = $abatimento;

        return $this;
    }

    /**
     * Get abatimento
     *
     * @return float
     */
    public function getAbatimento()
    {
        return $this->abatimento;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Parcelas
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set crediario
     *
     * @param \App\Entity\Crediario $crediario
     *
     * @return Parcelas
     */
    public function setCrediario(\App\Entity\Crediario $crediario = null)
    {
        $this->crediario = $crediario;

        return $this;
    }

    /**
     * Get crediario
     *
     * @return \App\Entity\Crediario
     */
    public function getCrediario()
    {
        return $this->crediario;
    }
    
    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }
}