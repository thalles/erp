<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;

/**
 * Juridica
 *
 * @ORM\Table(name="transportador")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Transportador")
 */
class Transportador extends AbstractEntity {

    /**
     * @var \App\Entity\Pessoa
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Pessoa", mappedBy="id")
     * @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="pessoa", referencedColumnName="id")
     * })
     */
    private $pessoa;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_antt", type="string", length=20, nullable=true)
     */
    private $codigoAntt;

    /**
     * @var string
     *
     * @ORM\Column(name="cadastro_nacional", type="string", length=20, nullable=true)
     */
    private $cadastroNacional;

    /**
     * @var string
     *
     * @ORM\Column(name="cnpj", type="string", length=20, nullable=true)
     */
    private $cnpj;

    /**
     * @var string
     *
     * @ORM\Column(name="cpf", type="string", length=20, nullable=true)
     */
    private $cpf;

    /**
     * @var string
     *
     * @ORM\Column(name="inscricao_estadual", type="string", length=20, nullable=true)
     */
    private $inscricaoEstadual;

    /**
     * Set cnpj
     *
     * @param string $cnpj
     *
     * @return Juridica
     */
    public function setCnpj($cnpj) {
        $this->cnpj = $cnpj;

        return $this;
    }

    /**
     * Get cnpj
     *
     * @return string
     */
    public function getCnpj() {
        return $this->cnpj;
    }

    /**
     * Set inscricaoEstadual
     *
     * @param string $inscricaoEstadual
     *
     * @return Juridica
     */
    public function setInscricaoEstadual($inscricaoEstadual) {
        $this->inscricaoEstadual = $inscricaoEstadual;
        return $this;
    }

    /**
     * Get inscricaoEstadual
     *
     * @return string
     */
    public function getInscricaoEstadual() {
        return $this->inscricaoEstadual;
    }

    /**
     * Set inscricaoMunicipal
     *
     * @param string $codigoAntt
     *
     * @return Juridica
     */
    public function setCodigoAntt($codigoAntt) {
        $this->codigoAntt = $codigoAntt;
        return $this;
    }

    /**
     * Get codigoAntt
     *
     * @return string
     */
    public function getCodigoAntt() {
        return $this->codigoAntt;
    }

    /**
     * Set cadastroNacional
     *
     * @param string $cadastroNacional
     *
     * @return Transportador
     */
    public function setCadastroNacional($cadastroNacional) {
        $this->cadastroNacional = $cadastroNacional;
        return $this;
    }

    /**
     * Get cadastroNacional
     *
     * @return string
     */
    public function getCadastroNacional() {
        return $this->cadastroNacional;
    }

    /**
     * Set pessoa
     *
     * @param \App\Entity\Pessoa $pessoa
     *
     * @return Juridica
     */
    public function setPessoa($pessoa) {
        $this->pessoa = $pessoa;
        return $this;
    }

    /**
     * Get pessoa
     *
     * @return \App\Entity\Pessoa
     */
    public function getPessoa() {
        return $this->pessoa;
    }

    /**
     * Get pessoa
     *
     * @return string
     */
    public function getCpf() {
        return $this->cpf;
    }

    /**
     * Set Cpf
     *
     * @param string $cpf
     *
     * @return Transportador
     */
    public function setCpf($cpf) {
        $this->cpf = $cpf;
        return $this;
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

    public function toArray() {
        return array_merge($this->getPessoa()->toArray(), parent::toArray());
    }

}
