<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Regiao
 *
 * @ORM\Table(name="regiao")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Regiao")
 */
class Regiao extends \App\Entity\AbstractEntity {

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="regiao", type="string", length=80, nullable=true)
     */
    private $regiao;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $regiao
     *
     * @return Regiao
     */
    public function setTipo($regiao) {
        $this->regiao = $regiao;
        return $this;
    }

    /**
     * Get regiao
     *
     * @return string
     */
    public function getRegiao() {
        return $this->regiao;
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

}
