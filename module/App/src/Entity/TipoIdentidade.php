<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoIdentidade
 *
 * @ORM\Table(name="tipo_identidade")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\TipoIdentidade")
 */
class TipoIdentidade extends \App\Entity\AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=100, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="sigla_identidade", type="string", length=10, nullable=false)
     */
    private $siglaIdentidade;

    /**
     * @var string
     *
     * @ORM\Column(name="emissor", type="string", length=45, nullable=false)
     */
    private $emissor;

    /**
     * @var string
     *
     * @ORM\Column(name="sigla", type="string", length=10, nullable=false)
     */
    private $sigla;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return TipoIdentidade
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set siglaIdentidade
     *
     * @param string $siglaIdentidade
     *
     * @return TipoIdentidade
     */
    public function setSiglaIdentidade($siglaIdentidade)
    {
        $this->siglaIdentidade = $siglaIdentidade;

        return $this;
    }

    /**
     * Get siglaIdentidade
     *
     * @return string
     */
    public function getSiglaIdentidade()
    {
        return $this->siglaIdentidade;
    }

    /**
     * Set emissor
     *
     * @param string $emissor
     *
     * @return TipoIdentidade
     */
    public function setEmissor($emissor)
    {
        $this->emissor = $emissor;

        return $this;
    }

    /**
     * Get emissor
     *
     * @return string
     */
    public function getEmissor()
    {
        return $this->emissor;
    }

    /**
     * Set sigla
     *
     * @param string $sigla
     *
     * @return TipoIdentidade
     */
    public function setSigla($sigla)
    {
        $this->sigla = $sigla;

        return $this;
    }

    /**
     * Get sigla
     *
     * @return string
     */
    public function getSigla()
    {
        return $this->sigla;
    }
    
    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }
}