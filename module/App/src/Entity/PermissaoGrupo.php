<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PermissaoGrupo
 *
 * @ORM\Table(name="permissao_grupo", indexes={@ORM\Index(name="fk_permissao_has_grupo_grupo1_idx", columns={"grupo"}), @ORM\Index(name="fk_permissao_has_grupo_permissao1_idx", columns={"permissao"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\PermissaoGrupo")
 */
class PermissaoGrupo extends \App\Entity\AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \App\Entity\Grupo
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Grupo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="grupo", referencedColumnName="id")
     * })
     */
    private $grupo;

    /**
     * @var \App\Entity\Permissao
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Permissao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="permissao", referencedColumnName="id")
     * })
     */
    private $permissao;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set grupo
     *
     * @param \App\Entity\Grupo $grupo
     *
     * @return PermissaoGrupo
     */
    public function setGrupo(\App\Entity\Grupo $grupo = null)
    {
        $this->grupo = $grupo;

        return $this;
    }

    /**
     * Get grupo
     *
     * @return \App\Entity\Grupo
     */
    public function getGrupo()
    {
        return $this->grupo;
    }

    /**
     * Set permissao
     *
     * @param \App\Entity\Permissao $permissao
     *
     * @return PermissaoGrupo
     */
    public function setPermissao(\App\Entity\Permissao $permissao = null)
    {
        $this->permissao = $permissao;

        return $this;
    }

    /**
     * Get permissao
     *
     * @return \App\Entity\Permissao
     */
    public function getPermissao()
    {
        return $this->permissao;
    }
    
    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }
}