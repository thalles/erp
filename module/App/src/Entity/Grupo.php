<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Grupo
 *
 * @ORM\Table(name="grupo")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Grupo")
 */
class Grupo extends \App\Entity\AbstractEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=50, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=300, nullable=false)
     */
    private $descricao;

    /**
     * @var string
     *
     * @ORM\Column(name="icone", type="string", length=60, nullable=true)
     */
    private $icone;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Permissao", mappedBy="grupo")
     */
    private $permissao;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Grupo
     */
    public function setNome($nome) {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome() {
        return $this->nome;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return Grupo
     */
    public function setDescricao($descricao) {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao() {
        return $this->descricao;
    }

    /**
     * Set icone
     *
     * @param string $icone
     *
     * @return Grupo
     */
    public function setIcone($icone) {
        $this->icone = $icone;

        return $this;
    }

    /**
     * Get icone
     *
     * @return string
     */
    public function getIcone() {
        return $this->icone;
    }

    /**
     * Add permissao
     *
     * @param \App\Entity\Permissao $permissao
     *
     * @return Grupo
     */
    public function addPermissao(\App\Entity\Permissao $permissao) {
        $this->permissao[] = $permissao;

        return $this;
    }

    /**
     * Remove permissao
     *
     * @param \App\Entity\Permissao $permissao
     */
    public function removePermissao(\App\Entity\Permissao $permissao) {
        $this->permissao->removeElement($permissao);
    }

    /**
     * Get permissao
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPermissao() {
        return $this->permissao;
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

}
