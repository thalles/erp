<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transportador
 *
 * @ORM\Table(name="transportador", indexes={@ORM\Index(name="fk_transportador_cidade1_idx", columns={"cidade"}), @ORM\Index(name="fk_transportador_estado1_idx", columns={"uf"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Transportador")
 */
class Transportador2 extends \App\Entity\AbstractEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=70, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_ANTT", type="string", length=20, nullable=false)
     */
    private $codigoAntt;

    /**
     * @var string
     *
     * @ORM\Column(name="cadastro_nacional", type="string", length=20, nullable=false)
     */
    private $cadastroNacional;

    /**
     * @var string
     *
     * @ORM\Column(name="endereco", type="string", length=100, nullable=false)
     */
    private $endereco;

    /**
     * @var string
     *
     * @ORM\Column(name="inscricao_estadual", type="string", length=20, nullable=true)
     */
    private $inscricaoEstadual;

    /**
     * @var \App\Entity\Cidade
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Cidade")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cidade", referencedColumnName="id")
     * })
     */
    private $cidade;

    /**
     * @var \App\Entity\Estado
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Estado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="uf", referencedColumnName="id")
     * })
     */
    private $uf;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Transportador
     */
    public function setNome($nome) {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome() {
        return $this->nome;
    }

    /**
     * Set codigoAntt
     *
     * @param string $codigoAntt
     *
     * @return Transportador
     */
    public function setCodigoAntt($codigoAntt) {
        $this->codigoAntt = $codigoAntt;

        return $this;
    }

    /**
     * Get codigoAntt
     *
     * @return string
     */
    public function getCodigoAntt() {
        return $this->codigoAntt;
    }

    /**
     * Set cadastroNacional
     *
     * @param string $cadastroNacional
     *
     * @return Transportador
     */
    public function setCadastroNacional($cadastroNacional) {
        $this->cadastroNacional = $cadastroNacional;

        return $this;
    }

    /**
     * Get cadastroNacional
     *
     * @return string
     */
    public function getCadastroNacional() {
        return $this->cadastroNacional;
    }

    /**
     * Set endereco
     *
     * @param string $endereco
     *
     * @return Transportador
     */
    public function setEndereco($endereco) {
        $this->endereco = $endereco;

        return $this;
    }

    /**
     * Get endereco
     *
     * @return string
     */
    public function getEndereco() {
        return $this->endereco;
    }

    /**
     * Set inscricaoEstadual
     *
     * @param string $inscricaoEstadual
     *
     * @return Transportador
     */
    public function setInscricaoEstadual($inscricaoEstadual) {
        $this->inscricaoEstadual = $inscricaoEstadual;

        return $this;
    }

    /**
     * Get inscricaoEstadual
     *
     * @return string
     */
    public function getInscricaoEstadual() {
        return $this->inscricaoEstadual;
    }

    /**
     * Set cidade
     *
     * @param \App\Entity\Cidade $cidade
     *
     * @return Transportador
     */
    public function setCidade(\App\Entity\Cidade $cidade = null) {
        $this->cidade = $cidade;

        return $this;
    }

    /**
     * Get cidade
     *
     * @return \App\Entity\Cidade
     */
    public function getCidade() {
        return $this->cidade;
    }

    /**
     * Set uf
     *
     * @param \App\Entity\Estado $uf
     *
     * @return Transportador
     */
    public function setUf(\App\Entity\Estado $uf = null) {
        $this->uf = $uf;

        return $this;
    }

    /**
     * Get uf
     *
     * @return \App\Entity\Estado
     */
    public function getUf() {
        return $this->uf;
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

}
