<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Estado
 *
 * @ORM\Table(name="estado", indexes={@ORM\Index(name="fk_estado_pais1_idx", columns={"pais"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Estado")
 */
class Estado extends \App\Entity\AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=75, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="uf", type="string", length=2, nullable=false)
     */
    private $uf;

    /**
     * @var float
     *
     * @ORM\Column(name="aliquota_icms_entrada", type="float", precision=10, scale=0, nullable=true)
     */
    private $aliquotaIcmsEntrada;

    /**
     * @var float
     *
     * @ORM\Column(name="aliquota_icms_saida", type="float", precision=10, scale=0, nullable=true)
     */
    private $aliquotaIcmsSaida;

    /**
     * @var float
     *
     * @ORM\Column(name="aliquota_icms_entrada_cpf", type="float", precision=10, scale=0, nullable=true)
     */
    private $aliquotaIcmsEntradaCpf;

    /**
     * @var float
     *
     * @ORM\Column(name="aliquota_icms_saida_cpf", type="float", precision=10, scale=0, nullable=true)
     */
    private $aliquotaIcmsSaidaCpf;

    /**
     * @var string
     *
     * @ORM\Column(name="inscricao_substituicao_tributaria", type="string", length=20, nullable=true)
     */
    private $inscricaoSubstituicaoTributaria;

    /**
     * @var integer
     *
     * @ORM\Column(name="uf_ibge", type="integer", nullable=true)
     */
    private $ufIbge;

    /**
     * @var float
     *
     * @ORM\Column(name="aliquota_pobreza", type="float", precision=10, scale=0, nullable=true)
     */
    private $aliquotaPobreza;

    /**
     * @var \App\Entity\Pais
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Pais")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pais", referencedColumnName="id")
     * })
     */
    private $pais;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Estado
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set uf
     *
     * @param string $uf
     *
     * @return Estado
     */
    public function setUf($uf)
    {
        $this->uf = $uf;

        return $this;
    }

    /**
     * Get uf
     *
     * @return string
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * Set aliquotaIcmsEntrada
     *
     * @param float $aliquotaIcmsEntrada
     *
     * @return Estado
     */
    public function setAliquotaIcmsEntrada($aliquotaIcmsEntrada)
    {
        $this->aliquotaIcmsEntrada = $aliquotaIcmsEntrada;

        return $this;
    }

    /**
     * Get aliquotaIcmsEntrada
     *
     * @return float
     */
    public function getAliquotaIcmsEntrada()
    {
        return $this->aliquotaIcmsEntrada;
    }

    /**
     * Set aliquotaIcmsSaida
     *
     * @param float $aliquotaIcmsSaida
     *
     * @return Estado
     */
    public function setAliquotaIcmsSaida($aliquotaIcmsSaida)
    {
        $this->aliquotaIcmsSaida = $aliquotaIcmsSaida;

        return $this;
    }

    /**
     * Get aliquotaIcmsSaida
     *
     * @return float
     */
    public function getAliquotaIcmsSaida()
    {
        return $this->aliquotaIcmsSaida;
    }

    /**
     * Set aliquotaIcmsEntradaCpf
     *
     * @param float $aliquotaIcmsEntradaCpf
     *
     * @return Estado
     */
    public function setAliquotaIcmsEntradaCpf($aliquotaIcmsEntradaCpf)
    {
        $this->aliquotaIcmsEntradaCpf = $aliquotaIcmsEntradaCpf;

        return $this;
    }

    /**
     * Get aliquotaIcmsEntradaCpf
     *
     * @return float
     */
    public function getAliquotaIcmsEntradaCpf()
    {
        return $this->aliquotaIcmsEntradaCpf;
    }

    /**
     * Set aliquotaIcmsSaidaCpf
     *
     * @param float $aliquotaIcmsSaidaCpf
     *
     * @return Estado
     */
    public function setAliquotaIcmsSaidaCpf($aliquotaIcmsSaidaCpf)
    {
        $this->aliquotaIcmsSaidaCpf = $aliquotaIcmsSaidaCpf;

        return $this;
    }

    /**
     * Get aliquotaIcmsSaidaCpf
     *
     * @return float
     */
    public function getAliquotaIcmsSaidaCpf()
    {
        return $this->aliquotaIcmsSaidaCpf;
    }

    /**
     * Set inscricaoSubstituicaoTributaria
     *
     * @param string $inscricaoSubstituicaoTributaria
     *
     * @return Estado
     */
    public function setInscricaoSubstituicaoTributaria($inscricaoSubstituicaoTributaria)
    {
        $this->inscricaoSubstituicaoTributaria = $inscricaoSubstituicaoTributaria;

        return $this;
    }

    /**
     * Get inscricaoSubstituicaoTributaria
     *
     * @return string
     */
    public function getInscricaoSubstituicaoTributaria()
    {
        return $this->inscricaoSubstituicaoTributaria;
    }

    /**
     * Set ufIbge
     *
     * @param integer $ufIbge
     *
     * @return Estado
     */
    public function setUfIbge($ufIbge)
    {
        $this->ufIbge = $ufIbge;

        return $this;
    }

    /**
     * Get ufIbge
     *
     * @return integer
     */
    public function getUfIbge()
    {
        return $this->ufIbge;
    }

    /**
     * Set aliquotaPobreza
     *
     * @param float $aliquotaPobreza
     *
     * @return Estado
     */
    public function setAliquotaPobreza($aliquotaPobreza)
    {
        $this->aliquotaPobreza = $aliquotaPobreza;

        return $this;
    }

    /**
     * Get aliquotaPobreza
     *
     * @return float
     */
    public function getAliquotaPobreza()
    {
        return $this->aliquotaPobreza;
    }

    /**
     * Set pais
     *
     * @param \App\Entity\Pais $pais
     *
     * @return Estado
     */
    public function setPais(\App\Entity\Pais $pais = null)
    {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return \App\Entity\Pais
     */
    public function getPais()
    {
        return $this->pais;
    }
    
    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }
}