<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Financeira
 *
 * @ORM\Table(name="financeira")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Financeira")
 */
class Financeira extends \App\Entity\AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=40, nullable=true)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="banco_credito", type="string", length=45, nullable=true)
     */
    private $bancoCredito;

    /**
     * @var string
     *
     * @ORM\Column(name="telefone", type="string", length=18, nullable=true)
     */
    private $telefone;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Financeira
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set bancoCredito
     *
     * @param string $bancoCredito
     *
     * @return Financeira
     */
    public function setBancoCredito($bancoCredito)
    {
        $this->bancoCredito = $bancoCredito;

        return $this;
    }

    /**
     * Get bancoCredito
     *
     * @return string
     */
    public function getBancoCredito()
    {
        return $this->bancoCredito;
    }

    /**
     * Set telefone
     *
     * @param string $telefone
     *
     * @return Financeira
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;

        return $this;
    }

    /**
     * Get telefone
     *
     * @return string
     */
    public function getTelefone()
    {
        return $this->telefone;
    }
    
    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }
}