<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contato
 *
 * @ORM\Table(name="contato", indexes={@ORM\Index(name="fk_contato_pessoa1_idx", columns={"pessoa"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Contato")
 */
class Contato extends \App\Entity\AbstractEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=45, nullable=true)
     */
    private $descricao;

    /**
     * @var string
     *
     * @ORM\Column(name="responsavel", type="string", length=60, nullable=true)
     */
    private $responsavel;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=20, nullable=true)
     */
    private $numero;

    /**
     * @var integer
     *
     * @ORM\Column(name="ramal", type="integer", nullable=true)
     */
    private $ramal = null;

    /**
     * @var string
     *
     * @ORM\Column(name="principal", type="string", length=1, nullable=true)
     */
    private $principal = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=1, nullable=true)
     */
    private $status = '1';

    /**
     * @var \App\Entity\Pessoa
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pessoa", referencedColumnName="id")
     * })
     */
    private $pessoa;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return Contato
     */
    public function setDescricao($descricao) {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao() {
        return $this->descricao;
    }

    /**
     * Set responsavel
     *
     * @param string $responsavel
     *
     * @return Contato
     */
    public function setResponsavel($responsavel) {
        $this->responsavel = $responsavel;

        return $this;
    }

    /**
     * Get responsavel
     *
     * @return string
     */
    public function getResponsavel() {
        return $this->responsavel;
    }

    /**
     * Set numero
     *
     * @param string $numero
     *
     * @return Contato
     */
    public function setNumero($numero) {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero() {
        return $this->numero;
    }

    /**
     * Set ramal
     *
     * @param integer $ramal
     *
     * @return Contato
     */
    public function setRamal($ramal) {
        $this->ramal = $ramal;

        return $this;
    }

    /**
     * Get ramal
     *
     * @return integer
     */
    public function getRamal() {
        return $this->ramal;
    }

    /**
     * Set principal
     *
     * @param string $principal
     *
     * @return Contato
     */
    public function setPrincipal($principal) {
        $this->principal = $principal;

        return $this;
    }

    /**
     * Get principal
     *
     * @return string
     */
    public function getPrincipal() {
        return $this->principal;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Contato
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set pessoa
     *
     * @param \App\Entity\Pessoa $pessoa
     *
     * @return Contato
     */
    public function setPessoa($pessoa = null) {
        $this->pessoa = $pessoa;

        return $this;
    }

    /**
     * Get pessoa
     *
     * @return \App\Entity\Pessoa
     */
    public function getPessoa() {
        return $this->pessoa;
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

}
