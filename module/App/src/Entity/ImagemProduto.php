<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImagemProduto
 *
 * @ORM\Table(name="imagem_produto", indexes={@ORM\Index(name="fk_imagem_produto_produto1_idx", columns={"produto"}), @ORM\Index(name="fk_imagem_produto_imagem1_idx", columns={"imagem"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\ImagemProduto")
 */
class ImagemProduto extends \App\Entity\AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \App\Entity\Imagem
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Imagem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="imagem", referencedColumnName="id")
     * })
     */
    private $imagem;

    /**
     * @var \App\Entity\Produto
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Produto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="produto", referencedColumnName="id")
     * })
     */
    private $produto;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imagem
     *
     * @param \App\Entity\Imagem $imagem
     *
     * @return ImagemProduto
     */
    public function setImagem(\App\Entity\Imagem $imagem = null)
    {
        $this->imagem = $imagem;

        return $this;
    }

    /**
     * Get imagem
     *
     * @return \App\Entity\Imagem
     */
    public function getImagem()
    {
        return $this->imagem;
    }

    /**
     * Set produto
     *
     * @param \App\Entity\Produto $produto
     *
     * @return ImagemProduto
     */
    public function setProduto(\App\Entity\Produto $produto = null)
    {
        $this->produto = $produto;

        return $this;
    }

    /**
     * Get produto
     *
     * @return \App\Entity\Produto
     */
    public function getProduto()
    {
        return $this->produto;
    }
    
    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }
}