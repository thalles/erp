<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Permissao
 *
 * @ORM\Table(name="permissao")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Permissao")
 */
class Permissao extends \App\Entity\AbstractEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="controller", type="string", length=50, nullable=false)
     */
    private $controller;

    /**
     * @var string
     *
     * @ORM\Column(name="actions", type="string", length=200, nullable=false)
     */
    private $actions;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Grupo", inversedBy="permissao")
     * @ORM\JoinTable(name="permissao_grupo",
     *   joinColumns={
     *     @ORM\JoinColumn(name="permissao", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="grupo", referencedColumnName="id")
     *   }
     * )
     */
    private $grupo;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set controller
     *
     * @param string $controller
     *
     * @return Permissao
     */
    public function setController($controller) {
        $this->controller = $controller;

        return $this;
    }

    /**
     * Get controller
     *
     * @return string
     */
    public function getController() {
        return $this->controller;
    }

    /**
     * Set actions
     *
     * @param string $actions
     *
     * @return Permissao
     */
    public function setActions($actions) {
        $this->actions = $actions;

        return $this;
    }

    /**
     * Get actions
     *
     * @return string
     */
    public function getActions() {
        return $this->actions;
    }

    /**
     * Add grupo
     *
     * @param \App\Entity\Grupo $grupo
     *
     * @return Permissao
     */
    public function addGrupo(\App\Entity\Grupo $grupo) {
        $this->grupo[] = $grupo;

        return $this;
    }

    /**
     * Remove grupo
     *
     * @param \App\Entity\Grupo $grupo
     */
    public function removeGrupo(\App\Entity\Grupo $grupo) {
        $this->grupo->removeElement($grupo);
    }

    /**
     * Get grupo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGrupo() {
        return $this->grupo;
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

}
