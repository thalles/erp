<?php

namespace App\Entity\Repository;

/**
 * ProdutoVenda
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProdutoVenda extends AbstractRepository {

    public function getProdutoByVenda($vendaId) {

        $query = "SELECT p.id produto, p.altura, p.largura, p.profundidade, p.valorAtual, "
                . "CONCAT(me.descricao,' - ',ma.nome) descricao, p.informacaoAdicional, p.codigoFabrica, "
                . "pv.id produtoVenda, pv.desconto, pv.quantidade "
                . "FROM {$this->getEntityName()} pv "
                . " JOIN App\Entity\Produto p WITH p.id = pv.produto "
                . " JOIN App\Entity\Mercadoria me WITH p.mercadoria = me.id "
                . " JOIN App\Entity\Marca ma WITH p.marca = ma.id "
                . "WHERE pv.venda = ?1";
        return $this->getEntityManager()
                        ->createQuery($query)
                        ->setParameter(1, $vendaId)
                        ->getArrayResult();
    }

}
