<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Zend\Session\Container;
use Zend\Hydrator;

/**
 * Description of AbstractRepository
 * @author Thalles
 */
class AbstractRepository extends EntityRepository {

    public function setReferencias(array $data, $entity) {
        $associations = $this->getClassMetadata()->getAssociationMappings();
        foreach ($associations as $colunm => $arrayAssociation) {
            $association = $colunm;
            if (array_key_exists('joinColumns', $arrayAssociation) && isset($data[$association])) {
                $entityAssociationName = $arrayAssociation['targetEntity'];
                $referencia = $this->getEntityManager()->getReference($entityAssociationName, $data[$association]);
                $metodoSet = 'set' . ucfirst($association);
                $entity->$metodoSet($referencia);
            }
        }
    }

    public function delete($id) {
        $entity = $this->getEntityManager()->getReference($this->getEntityName(), $id);
        if ($entity) {
            try {
                $this->getEntityManager()->remove($entity);
                $this->getEntityManager()->flush();
            } catch (\Doctrine\DBAL\DBALException $exe) {
                $session = new Container();
                $session->mensagem = [
                    'tipo' => 'warning',
                    'texto' => 'Esse registro está sendo ultilizado, não pode ser excluído!'
                ];
                return false;
            }
            return $entity;
        } else {
            return false;
        }
    }

    public function salvar(array $data) {
        if (isset($data['id']) && $data['id'] > 0) {
            return $this->update($data);
        }
        return $this->insert($data);
    }

    public function insert(array $data) {
        $entityName = $this->getEntityName();
        $entity = new $entityName($data);
        $this->setReferencias($data, $entity);
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function update(array $data) {
        $entityName = $this->getEntityName();
        $entity = $this->getEntityManager()->getReference($entityName, $data['id']);
        (new Hydrator\ClassMethods())->hydrate($data, $entity);
        $this->setReferencias($data, $entity);
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        return $entity;
    }

    public function deleteCriteria($campo, $valor) {
        $entity = $this->getEntityManager()
                ->createQuery("delete from " . $this->_entityName . " e "
                . "where e." . $campo . " = " . $valor);
        $entity->execute();
        return $entity;
    }

    public function atualizaCriteria($campo, $valor, $id) {
        $result = $this->getEntityManager()->createQuery("UPDATE $this->_entityName e "
                        . "SET e." . $campo . " = :valor "
                        . "WHERE e.id  = :id "
                )->setParameters(['valor' => $valor, 'id' => $id,])->getArrayResult();
        return $result;
    }

    public function atualizaByReferencia($campo, $valor, $referencia, $criterio) {
        return $this->getEntityManager()->createQuery("UPDATE $this->_entityName e "
                        . "SET e." . $campo . " = :valor WHERE e." . $referencia . "  = :criterio "
                )->setParameters(['valor' => $valor, 'criterio' => $criterio,])->getArrayResult();
    }

    public function isRegistro($campo, $valor) {
        $result = $this->findOneBy([$campo => $valor]);
        if (count($result) > 0) {
            return $result->getId();
        }
        return false;
    }

}
