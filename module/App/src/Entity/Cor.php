<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cor
 *
 * @ORM\Table(name="cor", indexes={@ORM\Index(name="fk_cor_cor1_idx", columns={"cor_pai"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Cor")
 */
class Cor extends \App\Entity\AbstractEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=50, nullable=false)
     */
    private $nome;

    /**
     * @var \App\Entity\Cor
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Cor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cor_pai", referencedColumnName="id")
     * })
     */
    private $corPai;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Cor
     */
    public function setNome($nome) {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome() {
        return $this->nome;
    }

    /**
     * Set corPai
     *
     * @param \App\Entity\Cor $corPai
     *
     * @return Cor
     */
    public function setCorPai($corPai = null) {
        $this->corPai = $corPai;

        return $this;
    }

    /**
     * Get corPai
     *
     * @return \App\Entity\Cor
     */
    public function getCorPai() {
        return $this->corPai;
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

}
