<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Crediario
 *
 * @ORM\Table(name="crediario", indexes={@ORM\Index(name="fk_crediario_venda1_idx", columns={"venda"}), @ORM\Index(name="fk_crediario_caixa1_idx", columns={"caixa"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Crediario")
 */
class Crediario extends \App\Entity\AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="observacao", type="text", length=65535, nullable=true)
     */
    private $observacao;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=1, nullable=true)
     */
    private $status;

    /**
     * @var \App\Entity\Caixa
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Caixa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="caixa", referencedColumnName="id")
     * })
     */
    private $caixa;

    /**
     * @var \App\Entity\Venda
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Venda")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="venda", referencedColumnName="id")
     * })
     */
    private $venda;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set observacao
     *
     * @param string $observacao
     *
     * @return Crediario
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;

        return $this;
    }

    /**
     * Get observacao
     *
     * @return string
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Crediario
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set caixa
     *
     * @param \App\Entity\Caixa $caixa
     *
     * @return Crediario
     */
    public function setCaixa(\App\Entity\Caixa $caixa = null)
    {
        $this->caixa = $caixa;

        return $this;
    }

    /**
     * Get caixa
     *
     * @return \App\Entity\Caixa
     */
    public function getCaixa()
    {
        return $this->caixa;
    }

    /**
     * Set venda
     *
     * @param \App\Entity\Venda $venda
     *
     * @return Crediario
     */
    public function setVenda(\App\Entity\Venda $venda = null)
    {
        $this->venda = $venda;

        return $this;
    }

    /**
     * Get venda
     *
     * @return \App\Entity\Venda
     */
    public function getVenda()
    {
        return $this->venda;
    }
    
    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }
}