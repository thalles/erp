<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Modelo
 *
 * @ORM\Table(name="modelo", indexes={
 *  @ORM\Index(name="fk_modelo_modelo1_idx", columns={"modelo_pai"}),
 *  @ORM\Index(name="fk_modelo_marca1_idx", columns={"marca"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Modelo")
 */
class Modelo extends \App\Entity\AbstractEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=50, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="portas", type="string", length=2, nullable=true)
     */
    private $portas;

    /**
     * @var string
     *
     * @ORM\Column(name="gavetas", type="string", length=2, nullable=true)
     */
    private $gavetas;

    /**
     * @var \App\Entity\Marca
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Marca")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="marca", referencedColumnName="id")
     * })
     */
    private $marca;

    /**
     * @var \App\Entity\Modelo
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Modelo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="modelo_pai", referencedColumnName="id")
     * })
     */
    private $modeloPai;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Modelo
     */
    public function setNome($nome) {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome() {
        return $this->nome;
    }

    /**
     * Set portas
     *
     * @param string $portas
     *
     * @return Modelo
     */
    public function setPortas($portas) {
        $this->portas = $portas;

        return $this;
    }

    /**
     * Get portas
     *
     * @return string
     */
    public function getPortas() {
        return $this->portas;
    }

    /**
     * Set gavetas
     *
     * @param string $gavetas
     *
     * @return Modelo
     */
    public function setGavetas($gavetas) {
        $this->gavetas = $gavetas;

        return $this;
    }

    /**
     * Get gavetas
     *
     * @return string
     */
    public function getGavetas() {
        return $this->gavetas;
    }

    /**
     * Set modeloPai
     *
     * @param \App\Entity\Modelo $modeloPai
     *
     * @return Modelo
     */
    public function setModeloPai($modeloPai = null) {
        $this->modeloPai = $modeloPai;
        return $this;
    }

    
    /**
     * Set marca
     *
     * @param \App\Entity\Marca $marca
     *
     * @return Produto
     */
    public function setMarca( $marca = null) {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca
     *
     * @return \App\Entity\Marca
     */
    public function getMarca() {
        return $this->marca;
    }

    /**
     * Get modeloPai
     *
     * @return \App\Entity\Modelo
     */
    public function getModeloPai() {
        return $this->modeloPai;
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

}
