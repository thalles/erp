<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Recebimento
 *
 * @ORM\Table(name="recebimento", indexes={@ORM\Index(name="fk_recebimento_caixa1_idx", columns={"caixa"}), @ORM\Index(name="fk_recebimento_venda1_idx", columns={"venda"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Recebimento")
 */
class Recebimento extends \App\Entity\AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="observacao", type="string", length=45, nullable=true)
     */
    private $observacao;

    /**
     * @var float
     *
     * @ORM\Column(name="valorRecebido", type="float", precision=10, scale=0, nullable=true)
     */
    private $valorrecebido;

    /**
     * @var \App\Entity\Caixa
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Caixa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="caixa", referencedColumnName="id")
     * })
     */
    private $caixa;

    /**
     * @var \App\Entity\Venda
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Venda")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="venda", referencedColumnName="id")
     * })
     */
    private $venda;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set observacao
     *
     * @param string $observacao
     *
     * @return Recebimento
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;

        return $this;
    }

    /**
     * Get observacao
     *
     * @return string
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * Set valorrecebido
     *
     * @param float $valorrecebido
     *
     * @return Recebimento
     */
    public function setValorrecebido($valorrecebido)
    {
        $this->valorrecebido = $valorrecebido;

        return $this;
    }

    /**
     * Get valorrecebido
     *
     * @return float
     */
    public function getValorrecebido()
    {
        return $this->valorrecebido;
    }

    /**
     * Set caixa
     *
     * @param \App\Entity\Caixa $caixa
     *
     * @return Recebimento
     */
    public function setCaixa(\App\Entity\Caixa $caixa = null)
    {
        $this->caixa = $caixa;

        return $this;
    }

    /**
     * Get caixa
     *
     * @return \App\Entity\Caixa
     */
    public function getCaixa()
    {
        return $this->caixa;
    }

    /**
     * Set venda
     *
     * @param \App\Entity\Venda $venda
     *
     * @return Recebimento
     */
    public function setVenda(\App\Entity\Venda $venda = null)
    {
        $this->venda = $venda;

        return $this;
    }

    /**
     * Get venda
     *
     * @return \App\Entity\Venda
     */
    public function getVenda()
    {
        return $this->venda;
    }
    
    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }
}