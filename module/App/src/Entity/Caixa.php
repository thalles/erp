<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Caixa
 *
 * @ORM\Table(name="caixa", indexes={@ORM\Index(name="fk_caixa_fisica1_idx", columns={"caixa"}), @ORM\Index(name="fk_caixa_fisica2_idx", columns={"gerente"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Caixa")
 */
class Caixa extends \App\Entity\AbstractEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_caixa", type="datetime", nullable=true)
     */
    private $dataCaixa;

    /**
     * @var string
     *
     * @ORM\Column(name="troco_inicial", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $trocoInicial;

    /**
     * @var \App\Entity\Fisica
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Fisica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="caixa", referencedColumnName="pessoa")
     * })
     */
    private $caixa;

    /**
     * @var \App\Entity\Fisica
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Fisica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="gerente", referencedColumnName="pessoa")
     * })
     */
    private $gerente;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set dataCaixa
     *
     * @param \DateTime $dataCaixa
     *
     * @return Caixa
     */
    public function setDataCaixa($dataCaixa) {
        $this->dataCaixa = $dataCaixa;

        return $this;
    }

    /**
     * Get dataCaixa
     *
     * @return \DateTime
     */
    public function getDataCaixa() {
        return $this->dataCaixa;
    }

    /**
     * Set trocoInicial
     *
     * @param string $trocoInicial
     *
     * @return Caixa
     */
    public function setTrocoInicial($trocoInicial) {
        $this->trocoInicial = $trocoInicial;

        return $this;
    }

    /**
     * Get trocoInicial
     *
     * @return string
     */
    public function getTrocoInicial() {
        return $this->trocoInicial;
    }

    /**
     * Set caixa
     *
     * @param \App\Entity\Fisica $caixa
     *
     * @return Caixa
     */
    public function setCaixa(Fisica $caixa = null) {
        $this->caixa = $caixa;

        return $this;
    }

    /**
     * Get caixa
     *
     * @return \App\Entity\Fisica
     */
    public function getCaixa() {
        return $this->caixa;
    }

    /**
     * Set gerente
     *
     * @param \App\Entity\Fisica $gerente
     *
     * @return Caixa
     */
    public function setGerente($gerente = null) {
        $this->gerente = $gerente;

        return $this;
    }

    /**
     * Get gerente
     *
     * @return \App\Entity\Fisica
     */
    public function getGerente() {
        return $this->gerente;
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

}
