<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;

/**
 * Juridica
 *
 * @ORM\Table(name="juridica")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Juridica")
 */
class Juridica extends AbstractEntity {

    /**
     * @var string
     *
     * @ORM\Column(name="razao_social", type="string", length=100, nullable=true)
     */
    private $razaoSocial;

    /**
     * @var string
     *
     * @ORM\Column(name="cnpj", type="string", length=20, nullable=true)
     */
    private $cnpj;

    /**
     * @var string
     *
     * @ORM\Column(name="inscricao_estadual", type="string", length=20, nullable=true)
     */
    private $inscricaoEstadual;

    /**
     * @var string
     *
     * @ORM\Column(name="inscricao_municipal", type="string", length=20, nullable=true)
     */
    private $inscricaoMunicipal;

    /**
     * @var string
     *
     * @ORM\Column(name="nome_contato", type="string", length=45, nullable=true)
     */
    private $nomeContato;

    /**
     * @var string
     *
     * @ORM\Column(name="endereco_eletronico", type="string", length=45, nullable=true)
     */
    private $enderecoEletronico;

    /**
     * @var \App\Entity\Pessoa
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Pessoa", mappedBy="id")
     * @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="pessoa", referencedColumnName="id")
     * })
     */
    private $pessoa;

    /**
     * One Juridica has Many Seguranca.
     * @ORM\OneToMany(targetEntity="App\Entity\Seguranca", mappedBy="juridica")
     */
    private $seguranca;

    /**
     * Set razaoSocial
     *
     * @param string $razaoSocial
     *
     * @return Juridica
     */
    public function setRazaoSocial($razaoSocial) {
        $this->razaoSocial = $razaoSocial;

        return $this;
    }

    /**
     * Get razaoSocial
     *
     * @return string
     */
    public function getRazaoSocial() {
        return $this->razaoSocial;
    }

    /**
     * Set cnpj
     *
     * @param string $cnpj
     *
     * @return Juridica
     */
    public function setCnpj($cnpj) {
        $this->cnpj = $cnpj;

        return $this;
    }

    /**
     * Get cnpj
     *
     * @return string
     */
    public function getCnpj() {
        return $this->cnpj;
    }

    /**
     * Set inscricaoEstadual
     *
     * @param string $inscricaoEstadual
     *
     * @return Juridica
     */
    public function setInscricaoEstadual($inscricaoEstadual) {
        $this->inscricaoEstadual = $inscricaoEstadual;

        return $this;
    }

    /**
     * Get inscricaoEstadual
     *
     * @return string
     */
    public function getInscricaoEstadual() {
        return $this->inscricaoEstadual;
    }

    /**
     * Set inscricaoMunicipal
     *
     * @param string $inscricaoMunicipal
     *
     * @return Juridica
     */
    public function setInscricaoMunicipal($inscricaoMunicipal) {
        $this->inscricaoMunicipal = $inscricaoMunicipal;

        return $this;
    }

    /**
     * Get inscricaoMunicipal
     *
     * @return string
     */
    public function getInscricaoMunicipal() {
        return $this->inscricaoMunicipal;
    }

    /**
     * Set nomeContato
     *
     * @param string $nomeContato
     *
     * @return Juridica
     */
    public function setNomeContato($nomeContato) {
        $this->nomeContato = $nomeContato;

        return $this;
    }

    /**
     * Get nomeContato
     *
     * @return string
     */
    public function getNomeContato() {
        return $this->nomeContato;
    }

    /**
     * Set enderecoEletronico
     *
     * @param string $enderecoEletronico
     *
     * @return Juridica
     */
    public function setEnderecoEletronico($enderecoEletronico) {
        $this->enderecoEletronico = $enderecoEletronico;

        return $this;
    }

    /**
     * Get enderecoEletronico
     *
     * @return string
     */
    public function getEnderecoEletronico() {
        return $this->enderecoEletronico;
    }

    /**
     * Set pessoa
     *
     * @param \App\Entity\Pessoa $pessoa
     *
     * @return Juridica
     */
    public function setPessoa($pessoa) {
        $this->pessoa = $pessoa;

        return $this;
    }

    /**
     * Get pessoa
     *
     * @return \App\Entity\Pessoa
     */
    public function getPessoa() {
        return $this->pessoa;
    }

    /**
     * Get PessoaContato
     *
     * @return \App\Entity\Contato
     */
    public function getSegurancas() {
        return $this->seguranca;
    }

    /**
     * Get PessoaContato
     *
     * @return \App\Entity\Contato
     */
    public function getSeguranca() {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('status', 1));
        return $this->seguranca[0];
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

    public function toArray() {
        return array_merge($this->getPessoa()->toArray(), parent::toArray());
    }

}
