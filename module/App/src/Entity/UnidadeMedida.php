<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UnidadeMedida
 *
 * @ORM\Table(name="unidade_medida")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\UnidadeMedida")
 */
class UnidadeMedida extends \App\Entity\AbstractEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="unidade", type="string", length=45, nullable=false)
     */
    private $unidade;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=45, nullable=false)
     */
    private $descricao;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set unidade
     *
     * @param string $unidade
     *
     * @return UnidadeMedida
     */
    public function setUnidade($unidade) {
        $this->unidade = $unidade;

        return $this;
    }

    /**
     * Get unidade
     *
     * @return string
     */
    public function getUnidade() {
        return $this->unidade;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return UnidadeMedida
     */
    public function setTipoMedida($descricao) {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao() {
        return $this->descricao;
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

}
