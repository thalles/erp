<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MovimentoProduto
 *
 * @ORM\Table(name="movimento_produto", indexes={@ORM\Index(name="fk_movimento_produto_lote1_idx", columns={"lote"}), @ORM\Index(name="fk_movimento_produto_produto1_idx", columns={"produto"}), @ORM\Index(name="fk_movimento_produto_nota_fiscal1_idx", columns={"nota_fiscal"}), @ORM\Index(name="fk_movimento_produto_unidade_medida1_idx", columns={"unidade_medida"}), @ORM\Index(name="fk_movimento_produto_venda1_idx", columns={"venda"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\MovimentoProduto")
 */
class MovimentoProduto extends \App\Entity\AbstractEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_na_nota", type="integer", nullable=false)
     */
    private $idNaNota;

    /**
     * @var float
     *
     * @ORM\Column(name="quantidade", type="float", precision=10, scale=0, nullable=false)
     */
    private $quantidade;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_unitario", type="float", precision=10, scale=0, nullable=false)
     */
    private $valorUnitario;

    /**
     * @var float
     *
     * @ORM\Column(name="desconto", type="float", precision=10, scale=0, nullable=false)
     */
    private $desconto = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="observacao", type="text", length=65535, nullable=true)
     */
    private $observacao;

    /**
     * @var float
     *
     * @ORM\Column(name="aliquota_ICMS", type="float", precision=10, scale=0, nullable=false)
     */
    private $aliquotaIcms;

    /**
     * @var float
     *
     * @ORM\Column(name="aliquota_IPI", type="float", precision=10, scale=0, nullable=false)
     */
    private $aliquotaIpi;

    /**
     * @var \App\Entity\Lote
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Lote")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="lote", referencedColumnName="id")
     * })
     */
    private $lote;

    /**
     * @var \App\Entity\NotaFiscal
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\NotaFiscal")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="nota_fiscal", referencedColumnName="id")
     * })
     */
    private $notaFiscal;

    /**
     * @var \App\Entity\Produto
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Produto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="produto", referencedColumnName="id")
     * })
     */
    private $produto;

    /**
     * @var \App\Entity\UnidadeMedida
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\UnidadeMedida")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="unidade_medida", referencedColumnName="id")
     * })
     */
    private $unidadeMedida;

    /**
     * @var \App\Entity\Venda
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Venda")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="venda", referencedColumnName="id")
     * })
     */
    private $venda;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_minimo", type="float", precision=10, scale=0, nullable=true)
     */
    private $valorMinimo;

    /**
     * @var \App\Entity\NaturezaOperacao
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\NaturezaOperacao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cfop", referencedColumnName="id")
     * })
     */
    private $cfop;
    private $valorTotal;
    private $valorIcms;
    private $valorIpi;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set idNaNota
     *
     * @param integer $idNaNota
     *
     * @return MovimentoProduto
     */
    public function setIdNaNota($idNaNota) {
        $this->idNaNota = $idNaNota;

        return $this;
    }

    /**
     * Get idNaNota
     *
     * @return integer
     */
    public function getIdNaNota() {
        return $this->idNaNota;
    }

    /**
     * Set quantidade
     *
     * @param float $quantidade
     *
     * @return MovimentoProduto
     */
    public function setQuantidade($quantidade) {
        $this->quantidade = $quantidade;

        return $this;
    }

    /**
     * Get quantidade
     *
     * @return float
     */
    public function getQuantidade() {
        return $this->quantidade;
    }

    /**
     * Set valorUnitario
     *
     * @param float $valorUnitario
     *
     * @return MovimentoProduto
     */
    public function setValorUnitario($valorUnitario) {
        $this->valorUnitario = $valorUnitario;

        return $this;
    }

    /**
     * Get valorUnitario
     *
     * @return float
     */
    public function getValorUnitario() {
        return round($this->valorUnitario, 3);
    }

    /**
     * Set desconto
     *
     * @param float $desconto
     *
     * @return MovimentoProduto
     */
    public function setDesconto($desconto) {
        $this->desconto = $desconto;

        return $this;
    }

    /**
     * Get desconto
     *
     * @return float
     */
    public function getDesconto() {
        return $this->desconto;
    }

    /**
     * Set observacao
     *
     * @param string $observacao
     *
     * @return MovimentoProduto
     */
    public function setObservacao($observacao) {
        $this->observacao = $observacao;

        return $this;
    }

    /**
     * Get observacao
     *
     * @return string
     */
    public function getObservacao() {
        return $this->observacao;
    }

    /**
     * Set aliquotaIcms
     *
     * @param float $aliquotaIcms
     *
     * @return MovimentoProduto
     */
    public function setAliquotaIcms($aliquotaIcms) {
        $this->aliquotaIcms = $aliquotaIcms;

        return $this;
    }

    /**
     * Get aliquotaIcms
     *
     * @return float
     */
    public function getAliquotaIcms() {
        return $this->aliquotaIcms;
    }

    /**
     * Set aliquotaIpi
     *
     * @param float $aliquotaIpi
     *
     * @return MovimentoProduto
     */
    public function setAliquotaIpi($aliquotaIpi) {
        $this->aliquotaIpi = $aliquotaIpi;

        return $this;
    }

    /**
     * Get aliquotaIpi
     *
     * @return float
     */
    public function getAliquotaIpi() {
        return $this->aliquotaIpi;
    }

    /**
     * Set lote
     *
     * @param \App\Entity\Lote $lote
     *
     * @return MovimentoProduto
     */
    public function setLote($lote = null) {
        $this->lote = $lote;

        return $this;
    }

    /**
     * Get lote
     *
     * @return \App\Entity\Lote
     */
    public function getLote() {
        return $this->lote;
    }

    /**
     * Set notaFiscal
     *
     * @param \App\Entity\NotaFiscal $notaFiscal
     *
     * @return MovimentoProduto
     */
    public function setNotaFiscal($notaFiscal = null) {
        $this->notaFiscal = $notaFiscal;

        return $this;
    }

    /**
     * Get notaFiscal
     *
     * @return \App\Entity\NotaFiscal
     */
    public function getNotaFiscal() {
        return $this->notaFiscal;
    }

    /**
     * Set produto
     *
     * @param \App\Entity\Produto $produto
     *
     * @return MovimentoProduto
     */
    public function setProduto($produto = null) {
        $this->produto = $produto;

        return $this;
    }

    /**
     * Get produto
     *
     * @return \App\Entity\Produto
     */
    public function getProduto() {
        return $this->produto;
    }

    /**
     * Set unidadeMedida
     *
     * @param \App\Entity\UnidadeMedida $unidadeMedida
     *
     * @return MovimentoProduto
     */
    public function setUnidadeMedida($unidadeMedida) {
        $this->unidadeMedida = $unidadeMedida;

        return $this;
    }

    /**
     * Get unidadeMedida
     *
     * @return \App\Entity\UnidadeMedida
     */
    public function getUnidadeMedida() {
        return $this->unidadeMedida;
    }

    /**
     * Set venda
     *
     * @param \App\Entity\Venda $venda
     *
     * @return MovimentoProduto
     */
    public function setVenda($venda) {
        $this->venda = $venda;

        return $this;
    }

    /**
     * Get venda
     *
     * @return \App\Entity\Venda
     */
    public function getVenda() {
        return $this->venda;
    }

    /**
     * Set valorMinimo
     *
     * @param float $valorMinimo
     *
     * @return Produto
     */
    public function setValorMinimo($valorMinimo) {
        $this->valorMinimo = $valorMinimo;

        return $this;
    }

    /**
     * Get valorMinimo
     *
     * @return float
     */
    public function getValorMinimo() {
        return round($this->valorMinimo, 2);
    }

    /**
     * Set cfop
     *
     * @param integer $cfop
     *
     * @return Produto
     */
    public function setCfop($cfop) {
        $this->cfop = $cfop;

        return $this;
    }

    /**
     * Get cfop
     *
     * @return integer
     */
    public function getCfop() {
        return $this->cfop;
    }

    public function getValorTotal() {
        $this->valorTotal = round($this->getValorUnitario() * $this->getQuantidade() - $this->getDesconto() * $this->getQuantidade(), 2);
        return $this->valorTotal;
    }

    public function getValorIcms() {
        $this->valorIcms = round($this->getValorTotal() * $this->getAliquotaIcms() / 100, 2);
        return $this->valorIcms;
    }

    public function getValorIpi() {
        $this->valorIpi = round($this->getValorTotal() * $this->getAliquotaIpi() / 100, 2);
        return $this->valorIpi;
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

}
