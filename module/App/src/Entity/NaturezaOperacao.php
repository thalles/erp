<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NaturezaOperacao
 *
 * @ORM\Table(name="natureza_operacao")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\NaturezaOperacao")
 */
class NaturezaOperacao extends \App\Entity\AbstractEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descricao", type="string", length=150, nullable=true)
     */
    private $descricao;

    /**
     * @var string
     *
     * @ORM\Column(name="altera_estoque", type="string", length=1, nullable=true)
     */
    private $alteraEstoque;

    /**
     * @var string
     *
     * @ORM\Column(name="cfop", type="string", length=8, nullable=true)
     */
    private $cfop;

    /**
     * @var string
     *
     * @ORM\Column(name="ind_nfe", type="string", length=8, nullable=false)
     */
    private $indNfe;

    /**
     * @var string
     *
     * @ORM\Column(name="ind_comunica", type="string", length=8, nullable=false)
     */
    private $indComunica;

    /**
     * @var string
     *
     * @ORM\Column(name="ind_transp", type="string", length=8, nullable=false)
     */
    private $indTransp;

    /**
     * @var string
     *
     * @ORM\Column(name="ind_devol", type="string", length=8, nullable=false)
     */
    private $indDevol;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     *
     * @return NaturezaOperacao
     */
    public function setDescricao($descricao) {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Get descricao
     *
     * @return string
     */
    public function getDescricao() {
        return $this->descricao;
    }

    /**
     * Set alteraEstoque
     *
     * @param string $alteraEstoque
     *
     * @return NaturezaOperacao
     */
    public function setAlteraEstoque($alteraEstoque) {
        $this->alteraEstoque = $alteraEstoque;

        return $this;
    }

    /**
     * Get alteraEstoque
     *
     * @return string
     */
    public function getAlteraEstoque() {
        return $this->alteraEstoque;
    }

    function setCfop($cfop) {
        $this->cfop = $cfop;
    }

    function getCfop() {
        return $this->cfop;
    }

    function setIndNfe($indNfe) {
        $this->indNfe = $indNfe;
    }

    function getIndNfe() {
        return $this->indNfe;
    }

    function setIndComunica($indComunica) {
        $this->indComunica = $indComunica;
    }

    function getIndComunica() {
        return $this->indComunica;
    }

    function setIndTransp($indTransp) {
        $this->indTransp = $indTransp;
    }

    function getIndTransp() {
        return $this->indTransp;
    }

    function setIndDevol($indDevol) {
        $this->indDevol = $indDevol;
    }

    function getIndDevol() {
        return $this->indDevol;
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

}
