<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;

///
/**
 * Fisica
 *
 * @ORM\Table(name="fisica", indexes={@ORM\Index(name="fk_fisica_tipo_identidade1_idx", columns={"tipo_identidade"}), @ORM\Index(name="IDX_B1A4CEAF1CDFAB82", columns={"pessoa"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Fisica")
 */
class Fisica extends AbstractEntity {

    /**
     * @var string
     *
     * @ORM\Column(name="cpf", type="string", length=14, nullable=true)
     */
    private $cpf;

    /**
     * @var string
     *
     * @ORM\Column(name="sexo", type="string", length=1, nullable=true)
     */
    private $sexo;

    /**
     * @var string
     *
     * @ORM\Column(name="identidade", type="string", length=12, nullable=true)
     */
    private $identidade;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="nascimento", type="date", nullable=true)
     */
    private $nascimento;

    /**
     * @var string
     *
     * @ORM\Column(name="nacionalidade", type="string", length=1, nullable=true)
     */
    private $nacionalidade = 'B';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_admissao", type="datetime", nullable=true)
     */
    private $dataAdmissao;

    /**
     * @var string
     *
     * @ORM\Column(name="renda_mensal", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $rendaMensal;

    /**
     * @var string
     *
     * @ORM\Column(name="estado_civil", type="string", length=1, nullable=true)
     */
    private $estadoCivil;

    /**
     * @var \Pessoa
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Pessoa", mappedBy="id")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pessoa", referencedColumnName="id")
     * })
     */
    private $pessoa;

    /**
     * @var \TipoIdentidade
     *
     * @ORM\ManyToOne(targetEntity="TipoIdentidade")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_identidade", referencedColumnName="id")
     * })
     */
    private $tipoIdentidade;

    /**
     * One Fisica has Many Seguranca.
     * @ORM\OneToMany(targetEntity="App\Entity\Seguranca", mappedBy="fisica")
     */
    private $seguranca;

    /**
     * Set cpf
     *
     * @param string $cpf
     *
     * @return Fisica
     */
    public function setCpf($cpf) {
        $this->cpf = $cpf;

        return $this;
    }

    /**
     * Get cpf
     *
     * @return string
     */
    public function getCpf() {
        return $this->cpf;
    }

    /**
     * Set sexo
     *
     * @param string $sexo
     *
     * @return Fisica
     */
    public function setSexo($sexo) {
        $this->sexo = $sexo;

        return $this;
    }

    /**
     * Get sexo
     *
     * @return string
     */
    public function getSexo() {
        return $this->sexo;
    }

    /**
     * Set identidade
     *
     * @param string $identidade
     *
     * @return Fisica
     */
    public function setIdentidade($identidade) {
        $this->identidade = $identidade;

        return $this;
    }

    /**
     * Get identidade
     *
     * @return string
     */
    public function getIdentidade() {
        return $this->identidade;
    }

    /**
     * Set nascimento
     *
     * @param \DateTime $nascimento
     *
     * @return Fisica
     */
    public function setNascimento($nascimento = '1111-11-11') {

        $this->nascimento = \DateTime::createFromFormat('Y-m-d', $nascimento);
        return $this;
    }

    /**
     * Get nascimento
     *
     * @return \DateTime
     */
    public function getNascimento() {
        return $this->nascimento;
    }

    /**
     * Set nacionalidade
     *
     * @param string $nacionalidade
     *
     * @return Fisica
     */
    public function setNacionalidade($nacionalidade) {
        $this->nacionalidade = $nacionalidade;

        return $this;
    }

    /**
     * Get nacionalidade
     *
     * @return string
     */
    public function getNacionalidade() {
        return $this->nacionalidade;
    }

    /**
     * Set estadoCivil
     *
     * @param string $estadoCivil
     *
     * @return Fisica
     */
    public function setEstadoCivil($estadoCivil) {
        $this->estadoCivil = $estadoCivil;
        return $this;
    }

    /**
     * Get estadoCivil
     *
     * @return string
     */
    public function getEstadoCivil() {
        return $this->estadoCivil;
    }

    /**
     * Set dataAdmissao
     *
     * @param \DateTime $dataAdmissao
     *
     * @return Fisica
     */
    public function setDataAdmissao($dataAdmissao = null) {
        if ($dataAdmissao):
            $this->dataAdmissao = \DateTime::createFromFormat('Y-m-d', $dataAdmissao);
        else:
            $this->dataAdmissao = $dataAdmissao;
        endif;
        return $this;
    }

    /**
     * Get dataAdmissao
     *
     * @return \DateTime
     */
    public function getDataAdmissao() {
        return $this->dataAdmissao;
    }

    /**
     * Set rendaMensal
     *
     * @param string $rendaMensal
     *
     * @return Fisica
     */
    public function setRendaMensal($rendaMensal) {
        $this->rendaMensal = $rendaMensal;
        return $this;
    }

    /**
     * Get rendaMensal
     *
     * @return string
     */
    public function getRendaMensal() {
        return $this->rendaMensal;
    }

    /**
     * Set pessoa
     *
     * @param \App\Entity\Pessoa $pessoa
     *
     * @return Fisica
     */
    public function setPessoa($pessoa) {
        $this->pessoa = $pessoa;

        return $this;
    }

    /**
     * Get pessoa
     *
     * @return \App\Entity\Pessoa
     */
    public function getPessoa() {
        return $this->pessoa;
    }

    /**
     * Set tipoIdentidade
     *
     * @param \App\Entity\TipoIdentidade $tipoIdentidade
     *
     * @return Fisica
     */
    public function setTipoIdentidade($tipoIdentidade) {
        $this->tipoIdentidade = $tipoIdentidade;

        return $this;
    }

    /**
     * Get tipoIdentidade
     *
     * @return \App\Entity\TipoIdentidade
     */
    public function getTipoIdentidade() {
        return $this->tipoIdentidade;
    }

    /**
     * Get PessoaContato
     *
     * @return \App\Entity\Contato
     */
    public function getSegurancas() {
        return $this->seguranca;
    }

    /**
     * Get PessoaContato
     *
     * @return \App\Entity\Contato
     */
    public function getSeguranca() {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('status', 1));
        return $this->seguranca[0];
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

    public function toArray() {
        return array_merge($this->getPessoa()->toArray(), parent::toArray());
    }

}
