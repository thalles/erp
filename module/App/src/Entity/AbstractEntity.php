<?php

namespace App\Entity;

use Zend\Hydrator;

/**
 * Description of abstractEntity
 *
 * @author Thalles
 */
class AbstractEntity {

    public function __construct($options = null) {
        if ($options) {
            (new Hydrator\ClassMethods())->hydrate($options, $this);
        }
    }

    public function __toString() {
        return (string) $this->getId();
    }

    public function __get($name) {
        $get = 'get' . $name;
        return $this->$get();
    }

    public function __set($name, $value) {
        $set = 'set' . $name;
        $this->$set($value);
    }

    public function toArray() {

        $arrayTo = [];

        foreach ($this->getNomesVariaveis() as $chav => $valor):
            $metodo = 'get' . ucfirst($chav);
            if (is_object($this->$metodo())) {
                if (method_exists($this->$metodo(), 'format')) {
                    if ($this->$metodo()->format('Y') == 1970)
                        $arrayTo[$chav] = $this->$metodo()->format('H:i');
                    else
                        $arrayTo[$chav] = $this->$metodo()->format('Y-m-d');
                } else if (method_exists($this->$metodo(), 'getId')) {
                    $arrayTo[$chav] = $this->$metodo()->getId();
                } else if (method_exists($this->$metodo(), 'getPessoa')) {
                    $arrayTo[$chav] = $this->$metodo()->getPessoa()->getId();
                }
            } else {
                $arrayTo[$chav] = $this->$metodo();
            }
        endforeach;
        return $arrayTo;
    }

    public function getDateObject($date) {
        if (count(explode("-", $date)) > 1) {
            return \DateTime::createFromFormat('Y-m-d', $date);
        } else if (count(explode("/", $date)) > 1) {
            return \DateTime::createFromFormat('d/m/Y', $date);
        }
        return null;
    }

    public function getNomeOrigem($identificador = 'Nome') {
        $pai = 'get' . $this->getNomeClasse() . 'Pai';
        $atual = $this;
        $nome = 'get' . ucfirst($identificador);
        $filha = $atual->$nome();
        while (is_object($atual->$pai())):
            $atual = $atual->$pai();
            $filha = $atual->$nome() . ' - ' . $filha;
        endwhile;
        return $filha;
    }

}
