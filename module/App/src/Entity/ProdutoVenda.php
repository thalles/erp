<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProdutoVenda
 *
 * @ORM\Table(name="produto_venda", indexes={@ORM\Index(name="fk_produto_has_venda_venda1_idx", columns={"venda"}), @ORM\Index(name="fk_produto_has_venda_produto1_idx", columns={"produto"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\ProdutoVenda")
 */
class ProdutoVenda extends \App\Entity\AbstractEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="desconto", type="float", precision=10, scale=0, nullable=true)
     */
    private $desconto;

    /**
     * @var \App\Entity\Produto
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Produto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="produto", referencedColumnName="id")
     * })
     */
    private $produto;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantidade", type="integer", nullable=false)
     */
    private $quantidade;

    /**
     * @var \App\Entity\Venda
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Venda")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="venda", referencedColumnName="id")
     * })
     */
    private $venda;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set desconto
     *
     * @param float $desconto
     *
     * @return ProdutoVenda
     */
    public function setDesconto($desconto) {
        $this->desconto = $desconto;

        return $this;
    }

    /**
     * Get desconto
     *
     * @return float
     */
    public function getDesconto() {
        return $this->desconto;
    }

    /**
     * Set produto
     *
     * @param \App\Entity\Produto $produto
     *
     * @return ProdutoVenda
     */
    public function setProduto($produto) {
        $this->produto = $produto;

        return $this;
    }

    /**
     * Get produto
     *
     * @return \App\Entity\Produto
     */
    public function getProduto() {
        return $this->produto;
    }

    /**
     * Set venda
     *
     * @param \App\Entity\Venda $venda
     *
     * @return ProdutoVenda
     */
    public function setVenda($venda = null) {
        $this->venda = $venda;

        return $this;
    }

    /**
     * Get venda
     *
     * @return \App\Entity\Venda
     */
    public function getVenda() {
        return $this->venda;
    }

    function getQuantidade() {
        return $this->quantidade;
    }

    function setQuantidade($quantidade) {
        $this->quantidade = $quantidade;
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

}
