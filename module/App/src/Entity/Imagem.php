<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Imagem
 *
 * @ORM\Table(name="imagem")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Imagem")
 */
class Imagem extends \App\Entity\AbstractEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="src", type="string", length=70, nullable=false)
     */
    private $src;

    /**
     * @var string
     *
     * @ORM\Column(name="legenda", type="string", length=50, nullable=true)
     */
    private $legenda;

    /**
     * @var string
     *
     * @ORM\Column(name="ordem", type="string", length=2, nullable=true)
     */
    private $ordem = '0';

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Produto", inversedBy="imagem")
     * @ORM\JoinTable(name="imagem_produto",
     *   joinColumns={
     *     @ORM\JoinColumn(name="imagem", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="produto", referencedColumnName="id")
     *   }
     * )
     */
    private $produto;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set src
     *
     * @param string $src
     *
     * @return Imagem
     */
    public function setSrc($src) {
        $this->src = $src;

        return $this;
    }

    /**
     * Get src
     *
     * @return string
     */
    public function getSrc() {
        return $this->src;
    }

    /**
     * Set legenda
     *
     * @param string $legenda
     *
     * @return Imagem
     */
    public function setLegenda($legenda) {
        $this->legenda = $legenda;

        return $this;
    }

    /**
     * Get legenda
     *
     * @return string
     */
    public function getLegenda() {
        return $this->legenda;
    }

    /**
     * Set ordem
     *
     * @param string $ordem
     *
     * @return Imagem
     */
    public function setOrdem($ordem) {
        $this->ordem = $ordem;

        return $this;
    }

    /**
     * Get ordem
     *
     * @return string
     */
    public function getOrdem() {
        return $this->ordem;
    }

    /**
     * Add produto
     *
     * @param \App\Entity\Produto $produto
     *
     * @return Imagem
     */
    public function addProduto(\App\Entity\Produto $produto) {
        $this->produto[] = $produto;

        return $this;
    }

    /**
     * Remove produto
     *
     * @param \App\Entity\Produto $produto
     */
    public function removeProduto(\App\Entity\Produto $produto) {
        $this->produto->removeElement($produto);
    }

    /**
     * Get produto
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduto() {
        return $this->produto;
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

}
