<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Carga
 *
 * @ORM\Table(name="carga")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Carga")
 */
class Carga extends \App\Entity\AbstractEntity {

    /**
     * @var \NotaFiscal
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="App\Entity\NotaFiscal", mappedBy="id")
     * @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="nota_fiscal", referencedColumnName="id")
     * })
     */
    private $notaFiscal;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantidade", type="integer", nullable=false)
     */
    private $quantidade;

    /**
     * @var string
     *
     * @ORM\Column(name="especie", type="string", length=15, nullable=false)
     */
    private $especie;

    /**
     * @var string
     *
     * @ORM\Column(name="marca", type="string", length=20, nullable=true)
     */
    private $marca;

    /**
     * @var string
     *
     * @ORM\Column(name="numeracao", type="string", length=15, nullable=true)
     */
    private $numeracao;

    /**
     * @var float
     *
     * @ORM\Column(name="peso_bruto", type="float", precision=10, scale=0, nullable=false)
     */
    private $pesoBruto;

    /**
     * @var float
     *
     * @ORM\Column(name="peso_liquido", type="float", precision=10, scale=0, nullable=false)
     */
    private $pesoLiquido;

    /**
     * @var string
     *
     * @ORM\Column(name="placa_veiculo", type="string", length=7, nullable=false)
     */
    private $placaVeiculo;

    /**
     * @var string
     *
     * @ORM\Column(name="frete_pagador", type="string", length=1, nullable=true)
     */
    private $fretePagador;

    /**
     * @var \App\Entity\Transportador
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Transportador")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="transportador", referencedColumnName="pessoa")
     * })
     */
    private $transportador;

    /**
     * Set Nota Fiscal
     *
     * @param \App\Entity\NotaFiscal $notaFiscal
     *
     * @return Carga
     */
    public function setNotaFiscal($notaFiscal) {
        $this->notaFiscal = $notaFiscal;
        return $this;
    }

    /**
     * Get pessoa
     *
     * @return \App\Entity\NotaFiscal
     */
    public function getNotaFiscal() {
        return $this->notaFiscal;
    }

    /**
     * Set quantidade
     *
     * @param integer $quantidade
     *
     * @return Carga
     */
    public function setQuantidade($quantidade) {
        $this->quantidade = $quantidade;

        return $this;
    }

    /**
     * Get quantidade
     *
     * @return integer
     */
    public function getQuantidade() {
        return $this->quantidade;
    }

    /**
     * Set especie
     *
     * @param string $especie
     *
     * @return Carga
     */
    public function setEspecie($especie) {
        $this->especie = $especie;

        return $this;
    }

    /**
     * Get especie
     *
     * @return string
     */
    public function getEspecie() {
        return $this->especie;
    }

    /**
     * Set marca
     *
     * @param string $marca
     *
     * @return Carga
     */
    public function setMarca($marca) {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca
     *
     * @return string
     */
    public function getMarca() {
        return $this->marca;
    }

    /**
     * Set numeracao
     *
     * @param string $numeracao
     *
     * @return Carga
     */
    public function setNumeracao($numeracao) {
        $this->numeracao = $numeracao;

        return $this;
    }

    /**
     * Get numeracao
     *
     * @return string
     */
    public function getNumeracao() {
        return $this->numeracao;
    }

    /**
     * Set pesoBruto
     *
     * @param float $pesoBruto
     *
     * @return Carga
     */
    public function setPesoBruto($pesoBruto) {
        $this->pesoBruto = $pesoBruto;

        return $this;
    }

    /**
     * Get pesoBruto
     *
     * @return float
     */
    public function getPesoBruto() {
        return $this->pesoBruto;
    }

    /**
     * Set pesoLiquido
     *
     * @param float $pesoLiquido
     *
     * @return Carga
     */
    public function setPesoLiquido($pesoLiquido) {
        $this->pesoLiquido = $pesoLiquido;

        return $this;
    }

    /**
     * Get pesoLiquido
     *
     * @return float
     */
    public function getPesoLiquido() {
        return $this->pesoLiquido;
    }

    /**
     * Set placaVeiculo
     *
     * @param string $placaVeiculo
     *
     * @return Carga
     */
    public function setPlacaVeiculo($placaVeiculo) {
        $this->placaVeiculo = $placaVeiculo;

        return $this;
    }

    /**
     * Get placaVeiculo
     *
     * @return string
     */
    public function getPlacaVeiculo() {
        return $this->placaVeiculo;
    }

    /**
     * Set fretePagador
     *
     * @param string $fretePagador
     *
     * @return Carga
     */
    public function setFretePagador($fretePagador) {
        $this->fretePagador = $fretePagador;

        return $this;
    }

    /**
     * Get fretePagador
     *
     * @return string
     */
    public function getFretePagador() {
        return $this->fretePagador;
    }

    /**
     * Set transportador
     *
     * @param \App\Entity\Transportador $transportador
     *
     * @return NotaFiscal
     */
    public function setTransportador($transportador = null) {
        $this->transportador = $transportador;

        return $this;
    }

    /**
     * Get transportador
     *
     * @return \App\Entity\Transportador
     */
    public function getTransportador() {
        return $this->transportador;
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

}
