<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

// * @ORM\InheritanceType("SINGLE_TABLE")
// * @ORM\DiscriminatorColumn(name="principal", type="string")
// * @ORM\DiscriminatorMap({"1" = "Sim", "0" = "Não"})
// *
/**
 * Endereco
 *
 * @ORM\Table(name="endereco", indexes={@ORM\Index(name="fk_endereco_pessoa1_idx", columns={"pessoa"}), @ORM\Index(name="fk_endereco_cidade1_idx", columns={"cidade"}), @ORM\Index(name="fk_endereco_estado1_idx", columns={"uf"})})
 *

 * @ORM\Entity(repositoryClass="App\Entity\Repository\Endereco")
 */
class Endereco extends \App\Entity\AbstractEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=40, nullable=false)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="logradouro", type="string", length=70, nullable=false)
     */
    private $logradouro;

    /**
     * @var string
     *
     * @ORM\Column(name="bairro", type="string", length=50, nullable=false)
     */
    private $bairro;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=6, nullable=false)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="complemento", type="string", length=20, nullable=true)
     */
    private $complemento;

    /**
     * @var string
     *
     * @ORM\Column(name="cep", type="string", length=10, nullable=false)
     */
    private $cep;

    /**
     * @var string
     *
     * @ORM\Column(name="referencia", type="string", length=300, nullable=true)
     */
    private $referencia;

    /**
     * @var string
     *
     * @ORM\Column(name="principal", type="string", length=1, nullable=false)
     */
    private $principal;

    /**
     * @var string
     */
    private $cidadeNome;

    /**
     * @var string
     */
    private $logradouroCompleto;

    /**
     * @var string
     */
    private $ufSigla;

    /**
     * @var \App\Entity\Cidade
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Cidade")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cidade", referencedColumnName="id")
     * })
     */
    private $cidade;

    /**
     * @var \App\Entity\Estado
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Estado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="uf", referencedColumnName="id")
     * })
     */
    private $uf;

    /**
     * @var \App\Entity\Pessoa
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Pessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pessoa", referencedColumnName="id")
     * })
     */
    private $pessoa;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return Endereco
     */
    public function setTitulo($titulo) {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo() {
        return $this->titulo;
    }

    /**
     * Set logradouro
     *
     * @param string $logradouro
     *
     * @return Endereco
     */
    public function setLogradouro($logradouro) {
        $this->logradouro = $logradouro;

        return $this;
    }

    /**
     * Get logradouro
     *
     * @return string
     */
    public function getLogradouro() {
        return $this->logradouro;
    }

    /**
     * Get logradouro completo
     *
     * @return string
     */
    public function getLogradouroCompleto() {
        if (strlen($this->logradouro) > 0)
            return $this->logradouro . ', ' . $this->numero . ' ' . $this->complemento;
        return $this->logradouro;
    }

    /**
     * Set bairro
     *
     * @param string $bairro
     *
     * @return Endereco
     */
    public function setBairro($bairro) {
        $this->bairro = $bairro;

        return $this;
    }

    /**
     * Get bairro
     *
     * @return string
     */
    public function getBairro() {
        return $this->bairro;
    }

    /**
     * Set numero
     *
     * @param string $numero
     *
     * @return Endereco
     */
    public function setNumero($numero) {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero() {
        return $this->numero;
    }

    /**
     * Set complemento
     *
     * @param string $complemento
     *
     * @return Endereco
     */
    public function setComplemento($complemento) {
        $this->complemento = $complemento;

        return $this;
    }

    /**
     * Get complemento
     *
     * @return string
     */
    public function getComplemento() {
        return $this->complemento;
    }

    /**
     * Set cep
     *
     * @param string $cep
     *
     * @return Endereco
     */
    public function setCep($cep) {
        $this->cep = $cep;

        return $this;
    }

    /**
     * Get cep
     *
     * @return string
     */
    public function getCep() {
        return $this->cep;
    }

    /**
     * Set referencia
     *
     * @param string $referencia
     *
     * @return Endereco
     */
    public function setReferencia($referencia) {
        $this->referencia = $referencia;

        return $this;
    }

    /**
     * Get referencia
     *
     * @return string
     */
    public function getReferencia() {
        return $this->referencia;
    }

    /**
     * Set principal
     *
     * @param string $principal
     *
     * @return Endereco
     */
    public function setPrincipal($principal) {
        $this->principal = $principal;

        return $this;
    }

    /**
     * Get principal
     *
     * @return string
     */
    public function getPrincipal() {
        return $this->principal;
    }

    /**
     * Set cidade
     *
     * @param \App\Entity\Cidade $cidade
     *
     * @return Endereco
     */
    public function setCidade($cidade = null) {
        $this->cidade = $cidade;

        return $this;
    }

    /**
     * Get cidade
     *
     * @return \App\Entity\Cidade
     */
    public function getCidade() {
        return $this->cidade;
    }

    /**
     * Get cidade
     *
     * @return string
     */
    public function getCidadeNome() {
        return $this->cidade->getNome();
    }

    /**
     * Set uf
     *
     * @param \App\Entity\Estado $uf
     *
     * @return Endereco
     */
    public function setUf($uf = null) {
        $this->uf = $uf;

        return $this;
    }

    /**
     * Get uf
     *
     * @return \App\Entity\Estado
     */
    public function getUf() {
        return $this->uf;
    }

    /**
     * Get uf
     *
     * @return string
     */
    public function getUfSigla() {
        return $this->uf->uf;
    }

    /**
     * Set pessoa
     *
     * @param \App\Entity\Pessoa $pessoa
     *
     * @return Endereco
     */
    public function setPessoa($pessoa = null) {
        $this->pessoa = $pessoa;

        return $this;
    }

    /**
     * Get pessoa
     *
     * @return \App\Entity\Pessoa
     */
    public function getPessoa() {
        return $this->pessoa;
    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

}
