<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Credito
 *
 * @ORM\Table(name="credito", indexes={@ORM\Index(name="fk_credito_venda1_idx", columns={"venda"}), @ORM\Index(name="fk_credito_financeira1_idx", columns={"financeira"}), @ORM\Index(name="fk_credito_caixa1_idx", columns={"caixa"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Credito")
 */
class Credito extends \App\Entity\AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="protocolo", type="string", length=45, nullable=true)
     */
    private $protocolo;

    /**
     * @var \App\Entity\Caixa
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Caixa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="caixa", referencedColumnName="id")
     * })
     */
    private $caixa;

    /**
     * @var \App\Entity\Financeira
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Financeira")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="financeira", referencedColumnName="id")
     * })
     */
    private $financeira;

    /**
     * @var \App\Entity\Venda
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Venda")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="venda", referencedColumnName="id")
     * })
     */
    private $venda;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set protocolo
     *
     * @param string $protocolo
     *
     * @return Credito
     */
    public function setProtocolo($protocolo)
    {
        $this->protocolo = $protocolo;

        return $this;
    }

    /**
     * Get protocolo
     *
     * @return string
     */
    public function getProtocolo()
    {
        return $this->protocolo;
    }

    /**
     * Set caixa
     *
     * @param \App\Entity\Caixa $caixa
     *
     * @return Credito
     */
    public function setCaixa(\App\Entity\Caixa $caixa = null)
    {
        $this->caixa = $caixa;

        return $this;
    }

    /**
     * Get caixa
     *
     * @return \App\Entity\Caixa
     */
    public function getCaixa()
    {
        return $this->caixa;
    }

    /**
     * Set financeira
     *
     * @param \App\Entity\Financeira $financeira
     *
     * @return Credito
     */
    public function setFinanceira(\App\Entity\Financeira $financeira = null)
    {
        $this->financeira = $financeira;

        return $this;
    }

    /**
     * Get financeira
     *
     * @return \App\Entity\Financeira
     */
    public function getFinanceira()
    {
        return $this->financeira;
    }

    /**
     * Set venda
     *
     * @param \App\Entity\Venda $venda
     *
     * @return Credito
     */
    public function setVenda(\App\Entity\Venda $venda = null)
    {
        $this->venda = $venda;

        return $this;
    }

    /**
     * Get venda
     *
     * @return \App\Entity\Venda
     */
    public function getVenda()
    {
        return $this->venda;
    }
    
    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }
}