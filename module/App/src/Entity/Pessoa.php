<?php

namespace App\Entity;

use Zend\Hydrator;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;

// *
// * @ORM\InheritanceType("JOINED")
// * @ORM\DiscriminatorColumn(name="tipo_pessoa", type="string")
// @ORM\DiscriminatorMap({ "0" = "Pessoa","1" = "Fisica", "2" = "Juridica"})

/**
 * Pessoa
 *
 * @ORM\Table(name="pessoa", indexes={
 *  @ORM\Index(name="fk_pessoa_padrao1_idx", columns={"padrao"}),
 *  @ORM\Index(name="fk_pessoa_tipo_pessoa1_idx", columns={"tipo_pessoa"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Pessoa")
 */
class Pessoa extends AbstractEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=100, nullable=true)
     */
    private $nome;

    /**
     * @var \App\Entity\Padrao
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Padrao")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="padrao", referencedColumnName="id")
     * })
     */
    private $padrao;

    /**
     * @var \App\Entity\TipoPessoa
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TipoPessoa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_pessoa", referencedColumnName="id")
     * })
     */
    private $tipoPessoa;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=1, nullable=true)
     */
    private $status = '1';

    /**
     * One Pessoa has Many endereço.
     * @ORM\OneToMany(targetEntity="App\Entity\Endereco", mappedBy="pessoa")
     * @ORM\OrderBy({"principal"="DESC"})
     */
    private $endereco;

    /**
     * One Pessoa has Many Contato.
     * @ORM\OneToMany(targetEntity="App\Entity\Contato", mappedBy="pessoa")
     */
    private $contato;

    /**
     * One Pessoa has One Fisica.
     * @ORM\OneToOne(targetEntity="App\Entity\Fisica", mappedBy="pessoa")
     */
    private $fisica;

    /**
     * One Pessoa has One juridica.
     * @ORM\OneToOne(targetEntity="App\Entity\Juridica", mappedBy="pessoa")
     */
    private $juridica;

    /**
     * One Pessoa has One juridica.
     * @ORM\OneToOne(targetEntity="App\Entity\Transportador", mappedBy="pessoa")
     */
    private $transportador;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Pessoa
     */
    public function setNome($nome) {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome() {
        return $this->nome;
    }

    /**
     * Set padrao
     *
     * @param \App\Entity\Padrao $padrao
     *
     * @return Pessoa
     */
    public function setPadrao($padrao = null) {
        $this->padrao = $padrao;
        return $this;
    }

    /**
     * Get padrao
     *
     * @return \App\Entity\Padrao
     */
    public function getPadrao() {
        return $this->padrao;
    }

    /**
     * Set padrao
     *
     * @param \App\Entity\TipoPessoa $tipoPessoa
     *
     * @return TipoPessoa
     */
    public function setTipoPessoa($tipoPessoa = 1) {
        $this->tipoPessoa = $tipoPessoa;
        return $this;
    }

    /**
     * Get tipoPessoa
     *
     * @return \App\Entity\TipoPessoa
     */
    public function getTipoPessoa() {
        return $this->tipoPessoa;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Fisica
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Get PessoaFisica
     *
     * @return \App\Entity\Fisica
     */
    public function getJuridica() {
        return $this->juridica;
    }

    /**
     * Get PessoaJuridica
     *
     * @return \App\Entity\Fisica
     */
    public function getFisica() {
        return $this->fisica;
    }

    /**
     * Get PessoaTranportador
     *
     * @return \App\Entity\Fisica
     */
    public function getTransportador() {
        return $this->transportador;
    }

    /**
     * Get PessoaEndereco
     *
     * @return \App\Entity\Endereco
     */
    public function getEnderecos() {
        return $this->endereco;
    }

    /**
     * Get PessoaEndereco
     * Retorna a primeria posição de um endereço principal
     * @return \App\Entity\Endereco
     */
    public function getEndereco() {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('principal', "1"));
        return count($this->endereco) > 0 ? $this->endereco->matching($criteria)[0] : [];
    }

    /**
     * Get PessoaContato
     *
     * @return \App\Entity\Endereco
     */
    public function getContatos() {
        return $this->contato;
    }

    /**
     * Get PessoaContato
     * Retorna a primeria posição de um contato principal
     * @return \App\Entity\Contato
     */
    public function getContato() {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('principal', "1"));
        return count($this->contato) > 0 ? $this->contato->matching($criteria)[0] : [];
    }

    /**
     * Get NomesVariaveis
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

}
