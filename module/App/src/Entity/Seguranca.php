<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Seguranca
 *
 * @ORM\Table(name="seguranca", uniqueConstraints={@ORM\UniqueConstraint(name="apelido_UNIQUE", columns={"apelido"}), @ORM\UniqueConstraint(name="email_UNIQUE", columns={"email"})}, indexes={@ORM\Index(name="fk_seguranca_grupo1_idx", columns={"grupo"}), @ORM\Index(name="fk_seguranca_fisica1_idx", columns={"fisica"}), @ORM\Index(name="fk_seguranca_juridica1_idx", columns={"juridica"})})
 * @ORM\Entity(repositoryClass="App\Entity\Repository\Seguranca")
 */
class Seguranca extends \App\Entity\AbstractEntity {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="apelido", type="string", length=45, nullable=true)
     */
    private $apelido;

    /**
     * @var string
     *
     * @ORM\Column(name="senha", type="string", length=45, nullable=true)
     */
    private $senha;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=1, nullable=true)
     */
    private $status = '1';

    /**
     * @var \App\Entity\Grupo
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Grupo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="grupo", referencedColumnName="id")
     * })
     */
    private $grupo;

    /**
     * @var \App\Entity\Fisica
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Fisica")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fisica", referencedColumnName="pessoa")
     * })
     */
    private $fisica;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set apelido
     *
     * @param string $apelido
     *
     * @return Seguranca
     */
    public function setApelido($apelido) {
        $this->apelido = $apelido;

        return $this;
    }

    /**
     * Get apelido
     *
     * @return string
     */
    public function getApelido() {
        return $this->apelido;
    }

    /**
     * Set senha
     *
     * @param string $senha
     *
     * @return Seguranca
     */
    public function setSenha($senha) {
        $this->senha = sha1($senha);

        return $this;
    }

    /**
     * Get senha
     *
     * @return string
     */
    public function getSenha() {
        return $this->senha;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Seguranca
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Seguranca
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set fisica
     *
     * @param \App\Entity\Fisica $fisica
     *
     * @return Seguranca
     */
    public function setFisica($fisica = null) {
        $this->fisica = $fisica;

        return $this;
    }

    /**
     * Get fisica
     *
     * @return \App\Entity\Fisica
     */
    public function getFisica() {
        return $this->fisica;
    }

    /**
     * Set grupo
     *
     * @param \App\Entity\Grupo $grupo
     *
     * @return Seguranca
     */
    public function setGrupo($grupo = null) {
        $this->grupo = $grupo;

        return $this;
    }

    /**
     * Get grupo
     *
     * @return \App\Entity\Grupo
     */
    public function getGrupo() {
        return $this->grupo;
    }

    public function getGrupoNome() {
        return $this->grupo->getNome();
    }

    public function getNomePessoa() {

    }

    /**
     * Get NomesVariaveis
     *
     * @return Array
     */
    public function getNomesVariaveis() {
        return get_class_vars(__CLASS__);
    }

    /**
     * Get NomeClasse
     *
     * @return string
     */
    public function getNomeClasse() {
        $class = explode('\\', __CLASS__);
        return ucfirst(end($class));
    }

    public function toArray() {
        return [
            'id'      => $this->getId(),
            'email'   => $this->getEmail(),
            'apelido' => $this->getApelido(),
            'grupo'   => $this->getGrupo()->getId(),
            'fisica'  => $this->getFisica()->getPessoa()->getId(),
            'status'  => $this->getStatus(),
        ];
    }

}
