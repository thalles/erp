<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonApp for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace App;

use Auth\Adapter\Adapter as AuthAdapter;
use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\MvcEvent;
use App\View\Helper\GetModuleName;
use App\View\Helper\GetMenuOfModule;

class Module {

    const VERSION = '3.0.3-dev';

    public $controllerClass = 0;

    public function onBootstrap(MvcEvent $e) {

        $eventManager = $e->getApplication()->getEventManager();
        $eventManager->attach('route', [$this, 'loadHelper'], 100);
        //$eventManager->attach('route', [$this, 'loadPlugin'], 100);

        /* set o Entity Manager para o plugin */
        \App\Plugin\EntityManager::setEm($e->getApplication()->getServiceManager()->get('Doctrine\ORM\EntityManager'));



        $eventManager->getSharedManager()->attach(\Zend\Mvc\Controller\AbstractController::class, 'dispatch', function($e) {
            $controller = $e->getTarget();
            $controllerClass = get_class($controller);
            $moduleNamespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));
            $config = $e->getApplication()->getServiceManager()->get('config');
//            echo $config['module_layouts'][$moduleNamespace];
//            exit;
            if (isset($config['module_layouts'][$moduleNamespace])) {
                $controller->layout($config['module_layouts'][$moduleNamespace]);
            }
        }, 100);
    }

    public function loadHelper(MvcEvent $e) {

        $application = $e->getApplication();
        $sm = $application->getServiceManager();
        $sharedManager = $application->getEventManager()->getSharedManager();

        $matchedRoute = $sm->get('router')->match($sm->get('request'));





        $e->getApplication()->getServiceManager()->get('ViewHelperManager')->setFactory('getMenuOfModule', function() use ($e) {
            return new GetMenuOfModule($e);
        }, 100);

        $e->getApplication()->getServiceManager()->get('ViewHelperManager')->setFactory('getModuleName', function() use ($e) {
            return new GetModuleName($e);
        }, 100);
    }

    public function loadPlugin(MvcEvent $e) {

        $application = $e->getApplication();
        $sm = $application->getServiceManager();
        $sharedManager = $application->getEventManager()->getSharedManager();

        $matchedRoute = $sm->get('router')->match($sm->get('request'));

        if (null !== $matchedRoute) {
            $sharedManager->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e) use ($sm) {
                //remover comentario para ativa controle de acesso
                return $sm->get('ControllerPluginManager')->get('Auth\Plugin\ControlAccess')->isAutorization($e);
            }, 100);
        }
    }

    public function getConfig() {
        return include __DIR__ . '/../config/module.config.php';
    }

}
