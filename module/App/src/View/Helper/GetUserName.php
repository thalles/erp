<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class GetUserName extends AbstractHelper {

    public function __invoke() {
        $authService = new \Zend\Authentication\AuthenticationService();
        $authService->setStorage(new \Zend\Authentication\Storage\Session('Usuario'));
        $nome = explode(' ', $authService->getIdentity()->getNome());
        return $nome[0];
    }

}
