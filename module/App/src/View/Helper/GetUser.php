<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class GetModuleName extends AbstractHelper {

    public function __invoke() {
        $authService = new \Zend\Authentication\AuthenticationService();
        $authService->setStorage(new \Zend\Authentication\Storage\Session('Usuario'));
        //echo $authService->getIdentity()->getNome();
        return $authService->getIdentity();
    }

}
