<?php

namespace App\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Mvc\MvcEvent;

class GetMenuOfModule extends AbstractHelper {

    private $e;

    public function __construct(MvcEvent $e) {
        $this->e = $e;
    }

    /**
     * @param MvcEvent $e
     * @return string
     */
    public function __invoke(): string {
        $controllerClass = $this->e->getRouteMatch()->getParam('controller');
        return 'app/menus-lateral/' . strtolower(substr($controllerClass, 0, strpos($controllerClass, '\\'))) . '.php';
    }

}
