
<li class="header"><h5><b>MENU GERENCIAL</b></h5></li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-users text-aqua"></i> <span>Usuários</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="<?= $this->url('manage'); ?>"><i class="fa fa-circle-o text-aqua"></i> Funcionários</a></li>
        <li><a href="<?= $this->url('manage-client'); ?>"><i class="fa fa-circle-o text-aqua"></i> Clientes</a></li>
        <li><a href="<?= $this->url('manage-provider'); ?>"><i class="fa fa-circle-o text-aqua"></i> Fornecedores</a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-files-o"></i>
        <span>Caixa</span>
        <span class="pull-right-container">
            <span class="label label-primary pull-right">4</span>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
        <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
        <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
        <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
    </ul>
</li>
<li class="treeview active">
    <a href="#">
        <i class="fa fa-edit"></i> <span>Estoque</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="<?= $this->url('manage-product'); ?>"><i class="fa fa-circle-o"></i> Produtos</a></li>
        <li><a href="<?= $this->url('manage-invoice'); ?>"><i class="fa fa-circle-o"></i> Entrada</a></li>
        <li><a href="<?= $this->url('manage-product'); ?>"><i class="fa fa-circle-o"></i> Funcionários</a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-laptop"></i>
        <span>Pedidos</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="pages/UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>
        <li><a href="pages/UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>
        <li><a href="pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>
        <li><a href="pages/UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>
        <li><a href="pages/UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>
        <li><a href="pages/UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-table"></i> <span>Vendas</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="<?= $this->url('vendas'); ?>"><i class="fa fa-circle-o"></i> Pré Venda</a></li>
        <li><a href="pages/tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-pie-chart"></i>
        <span>Relatórios</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
        <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
        <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
        <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-folder"></i> <span>Fiscal</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="pages/examples/invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>
        <li><a href="pages/examples/profile.html"><i class="fa fa-circle-o"></i> Profile</a></li>
    </ul>
</li>


