
<li class="treeview">
    <a href="#">
        <i class="fa fa-book"></i>
        <span>Documentação</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Manual de Uso</a></li>
        <li><a href="/AdminLTE/index2.html" target="AdminLTE"><i class="fa fa-circle-o"></i> Manual do AdminLTE</a></li>
        <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Instruções ao Desnvolvedor</a></li>
        <li><a href="https://github.com/nfephp-org/nfephp/wiki" target="NFE/PHP"><i class="fa fa-circle-o"></i> NFE/PHP</a></li>
        <li><a href="https://groups.google.com/forum/#!forum/nfephp" target="NFE/PHP-discurção"><i class="fa fa-circle-o"></i> Grupo Google NFE/PHP</a></li>
    </ul>
</li>
<li class="header">LABELS</li>
<li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
<li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
<li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
