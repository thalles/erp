
<li class="header"><h5><b>MENU DE ESTOQUE</b></h5></li>
<li class="treeview active">
    <a href="#">
        <i class="fa fa-users"></i> <span>Estoque</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu menu-open">
        <li><a href="<?= $this->url('manage-product'); ?>"><i class="fa fa-circle-o"></i> Produtos</a></li>
        <li><a href="<?= $this->url('manage-invoice'); ?>"><i class="fa fa-circle-o"></i> Entrada</a></li>

    </ul>
</li>