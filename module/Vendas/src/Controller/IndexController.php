<?php

namespace Vendas\Controller;

use App\Form\Products\PreVenda;
use Zend\View\Model\ViewModel;

/**
 * Description of GestorController
 *
 * @author Thalles Ferreira
 */
class IndexController extends AbstractController {

    public function indexAction() {
        $formPreVenda = new PreVenda();

        if (!$this->getRequest()->isPost()) {
            return new ViewModel(['formPreVenda' => $formPreVenda]);
        }

        if (count($this->params()->fromPost('produto')) < 1) {
            $this->setMessageError('Adicione pelo menos 1(um) produto.');
            return new ViewModel(['formPreVenda' => $formPreVenda]);
        }

        $arrayVenda = [
            'vendedor'      => $this->params()->fromPost('vendedor', null),
            'estatusVenda'  => '1',
            'tipoPagamento' => '1',
        ];
        $vendaId = $this->getRepository('Venda')->salvar($arrayVenda)->getId();

        $count = 0;
        foreach ($this->params()->fromPost('produto') as $produto) {
            $arrayProdutoVenda = [
                'produto'    => $produto,
                'venda'      => $vendaId,
                'quantidade' => $this->params()->fromPost('quantidade')[$count],
                'desconto'   => $this->params()->fromPost('desconto')[$count],
            ];

            $this->getRepository('ProdutoVenda')->salvar($arrayProdutoVenda);
            $count++;
        }
        $this->setMessageSuccess('Pedido lançado! Encaminhe ao caixa!');
        return $this->redirect()->toRoute();
    }

}
