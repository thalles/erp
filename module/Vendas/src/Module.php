<?php

namespace Vendas;

/**
 * Description of Module
 *
 * @author Thalles Ferreira
 */
class Module {

    public function getConfig() {
        return include __dir__ . "/../config/module.config.php";
    }

}
