

var options = {
    success: function (result) {
        if (result.id) {
            limpa = false;
            $('#categoria').val(result.id);
            $('.form-categoria').modal('hide');
            $('[name="pesquisaCategoria"]').typeahead('destroy');
            postcarregaLista('/gestao/categoria/lista', '[name="pesquisaCategoria"]', 'categoria');
            return disableLayer();
//                alertSuccess('Dados armazenado com sucesso!');
        } else {
            errorOnSave();
        }
        return disableLayer();
    },
    error: function () {
        errorOnSave();
        return disableLayer();
    },
    beforeSubmit: function () {
        return enableLayer();
    }
};
var options1 = {
    success: function (result) {
        if (result.id) {
            limpa = false;
            $('#marca').val(result.id);
            $('.form-marca').modal('hide');
            $('[name="pesquisaMarca"]').typeahead('destroy');
            postcarregaLista('/gestao/marca/lista', '[name="pesquisaMarca"]', 'marca');
            return disableLayer();
//                alertSuccess('Dados armazenado com sucesso!');
        } else {
            errorOnSave();
        }
        return disableLayer();
    },
    error: function () {
        errorOnSave();
        return disableLayer();
    },
    beforeSubmit: function () {
        return enableLayer();
    }
};

$('#form-categoria').ajaxForm(options);
$('#form-marca').ajaxForm(options1);


