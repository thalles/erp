$(document).ready(function () {
    loadTypeahead('/gestao/acabamento/lista', '.th-acabamento', 'acabamento');
    loadTypeahead('/gestao/cor/lista', '.th-cor', 'cor');
    loadTypeahead('/gestao/entrada/transportadores', '.th-transportador', 'transportador');
    loadTypeahead('/gestao/fornecedor/lista', '.th-emissor', 'emissor');
    loadTypeahead('/gestao/marca/lista', '.th-marca', 'marca');
    loadTypeahead('/gestao/mercadoria/lista', '.th-mercadoria', 'mercadoria');
    loadTypeahead('/gestao/natureza-operacao/lista', '.th-naturezaOperacao', 'naturezaOperacao');
    loadTypeahead('/gestao/natureza-operacao/lista', '.th-cfop', 'cfop');
    loadTypeahead('/gestao/produto/lista', '.th-produto', 'produto');
    loadTypeahead('/gestao/unidade-medida/lista', '.th-unidadeMedida', 'unidadeMedida');

    //A função carregaTypeahead possui callback como ultimo parametro
    //carregaTypeahead('/gestao/produto/lista', '[name="pesquisaProduto"]', 'idProduto');
    carregaTypeahead('/gestao/employees/lista', '[name="pesquisaVendedor"]', 'vendedor');
    carregaTypeahead('/gestao/cliente/lista', '[name="pesquisaCliente"]', 'cliente');
});

