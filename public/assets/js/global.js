if (typeof (Vue) !== 'undefined') {
    Vue.http.options.emulateJSON = true
    Vue.use(VueMask.VueMaskPlugin);
    Vue.http.interceptors.push((request, next) => {
        next((response) => {
            if (response.status === 401) {
                // do something
            }
        })
    })
}

/*--- Reconfiguração do alert ---*/
alert = function (mensagem) {
    if (!$('#alert-modal').length) {
        $.ajax({
            url: '/html/alert-modal.php',
            success: function (html) {
                $('body').append(html);
            },
            async: false,
            dataType: "html"
        });
    }
    styleAlert();
    $('#alert-modal .modal-body').text(mensagem);
    $('#alert-modal').modal('show');
};

//confirm = function (mensagem, clickExecute) {
//    if (!$('#alert-modal').length) {
//        $.ajax({
//            url: '/html/alert-modal.php',
//            success: function (html) {
//                $('body').append(html);
//            },
//            async: false,
//            dataType: "html"
//        });
//    }
//
//    $('#footer-confirm > #true').click(function () {
//        if (!clickExecute.indexOf('/')) {
//            return window.location = clickExecute;
//        }
//        return eval(clickExecute);
//    });
//    styleConfirm();
//    $('#alert-modal .modal-body').text(mensagem);
//    $('#alert-modal').modal('show');
//    return false;
//};

styleAlert = function () {
    $('#alert-modal-label').text('Alerta');
    $('#alert-modal #footer-comfirm').addClass('hidden');
    $('#alert-modal #footer-alert').removeClass('hidden');
};

styleConfirm = function () {
    $('#alert-modal-label').text('Comfirmação');
    $('#alert-modal').attr('class', 'modal fade modal-info');
    $('#alert-modal #footer-alert').addClass('hidden');
    $('#alert-modal #footer-confirm').removeClass('hidden');
};

alertDefault = function (mensagem) {
    alert(mensagem);
    $('#alert-modal').attr('class', 'modal fade');
};

alertDanger = function (mensagem) {
    alert(mensagem);
    $('#alert-modal').attr('class', 'modal fade modal-danger');
};

alertInfo = function (mensagem) {
    alert(mensagem);
    $('#alert-modal').attr('class', 'modal fade modal-info');
};

alertSuccess = function (mensagem) {
    alert(mensagem);
    $('#alert-modal').attr('class', 'modal fade modal-success');
};

/*--- Configuration of tabs ---*/
enableTabs = function () {
    $('.nav-tabs li > a').attr('data-toggle', 'tab');
};

disableTabs = function () {
    $('.nav-tabs li > a').attr('data-toggle', 'disabled');
};

isPerson = function () {
    return!!$("#pessoa").val();
};

enablePersonTabs = function () {
    if (isPerson())
        enableTabs();
};

/*--- methods for definition of layers of ajax ---*/
definitionLayer = function () {
    var hJanela = $(window).height();
    var wJanela = $(window).width();
    var img = $('#loading-layer img');
    var topImg = (hJanela / 2) - 100;
    var leftImg = (wJanela / 2) - 50;
    img.css({top: topImg, left: leftImg});
};

enableLayer = function () {
    definitionLayer();
    $('#loading-layer').fadeIn();
};

disableLayer = function () {
    $('#loading-layer').fadeOut();
};




