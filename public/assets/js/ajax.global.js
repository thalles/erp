sendForm = function (formSelector, callback) {
    // prepare Options Object
    var options = {
        success: function (result) {
            if (result.id) {
                $(formSelector + ' [name="id"]').val(result.id);
                if (formSelector == '#form-categoria') {
                    reloadOptionsCategorias(result.categorias);
                    $('#form-categoria').parents('.modal').modal('hide');
                }
                alertSuccess('Dados armazenado com sucesso!');
                if (callback) {
                    callback(result);
                }
            } else {
                errorOnSave();
            }
            return disableLayer();
        },
        error: function () {
            errorOnSave();
            return disableLayer();
        },
        beforeSubmit: function () {
            return enableLayer();
        }
    };
    $(formSelector).ajaxForm(options);
};



errorOnSave = function () {
    alertDanger('Ocorreu um erro, verifique se os dados foram salvos!');
};
//endereco = "/gestao/endereco/enderecos"
// dados = {'id': id},
//alvo = '[name="endereco' + campo + '"]'
function organizaOption(endereco, dados, alvo) {

    $.ajax({
        method: "post",
        url: endereco,
        data: dados,
        beforeSend: function () {
            enableLayer();
        },
        success: function (dados) {
            var options = "";
            $.each(dados, function (index, name) {
                options += "<option value='" + name.id + "'>" + name.name + "</option>";
            });
            if (options == "") {
                options = "<option value=''>Não há registros</option>";
            }
            $(alvo).html(options);
            disableLayer();
        },
        error: function () {
            alert('Ocorreu um erro!');
            disableLayer();
        }
    });
}

carregaTypeahead = function (url, seletor, campoId, callback) {

    var options = {
        method: 'get',
        url: url,
        beforeSend: function () {
            if (!$(seletor).length)
                return false;
        },
        success: function (result) {
            $(seletor).typeahead({source: result});
            setCampoId(seletor, campoId, callback);
        },
        error: function () {
            alertDanger('Erro ao carrgar opções!');
        }

    };
    $.ajax(options);
};

setCampoId = function (seletor, campoId, callback) {
    $(seletor).change(function () {
        var current = $(seletor).typeahead("getActive");
        if (current) {
            if (current.name == $(seletor).val()) {
                $('#' + campoId).val(current.id);
            } else {
                $('#' + campoId).val('');
            }
        } else {

        }
        callback && callback(current, seletor, campoId);
    });
}

