$(document).ready(function () {
    carregaTypeahead('/gestao/produto/lista', '[name="pesquisaProduto"]', 'idProduto'
            , function (current, seletor, campoId) {
                selectedProduct = JSON.parse(JSON.stringify(current));

            });
    //addProductOfCart('#form-pre-venda');
    //emoveProductOfCart('[href="#remove-of-cart"]');

});

if (document.getElementById('pedido')) {
    var selectedProduct = [];

    var appProducts = new Vue({
        el: '#pedido',
        data: {
            products: []
        },
        mounted: function () {
            //console.log(this.$el);
        },
        methods: {
            addProduct: function () {
                if (selectedProduct.id) {
                    selectedProduct.quantidade = 1;
                    selectedProduct.desconto = 0;
                    this.products.push(selectedProduct);
                    document.querySelector('[name="pesquisaProduto"]').value = '';
                    selectedProduct = [];
                }
            },
            removeProduct: function (index) {
                this.products.splice(index, 1);
            }
        }
    });
}

addProductOfCart = function (seletor) {
    $(seletor).on('submit', function () {
        var clone = $('#copia').clone(true);
        clone.removeAttr('id').removeClass('hidden');
        clone.children('[data-type="produto"]').text($('[name="pesquisaProduto"]').val());
        $('[name="pesquisaProduto"]').val('');
        $('#table-produtos tbody').append(clone);
        //alert(clone.);
        return false;
    });
};
removeProductOfCart = function (seletor) {
    $('a[href="#remove-of-cart"]').click(function () {
        $(this).parents('tr').fadeOut(function () {
            $(this).remove();
        });
        return false;
    });
};