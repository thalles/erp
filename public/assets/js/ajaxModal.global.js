sendFormModal = function (formSelector, callback) {
    // prepare Options Object
    var options = {
        success: function (result) {
            if (result.id) {
                $(formSelector + ' [name="id"]').val(result.id);
                alertSuccess('Dados armazenado com sucesso!');
                callback && callback();

            } else {
                errorOnSave();
            }
            return disableLayer();
        },
        error: function () {
            errorOnSave();
            return disableLayer();
        },
        beforeSubmit: function () {
            return enableLayer();
        }
    };
    $(formSelector).ajaxForm(options);
};