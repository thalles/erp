/**
 *documentação do plugin em https://igorescobar.github.io/jQuery-Mask-Plugin/docs.html
 */
$(document).ready(function () {
    $('[name="cpf"]').mask('000.000.000-00', {placeholder: "___.___.___-__", clearIfNotMatch: true, reverse: true});
    $('[name="cnpj"]').mask('00.000.000/0000-00', {placeholder: "__.___.___/____-__", clearIfNotMatch: true, reverse: true});
    //$('[name="cep"]').mask('00000-000', {placeholder: "_____-___", clearIfNotMatch: true, reverse: true});
    /*$('[name="numero"][type="tel"]').mask('(00) Z 0000-0000', {placeholder: "(__) _ ____-____ ",
     translation: {
     'Z': {
     pattern: /[0-9]/, optional: true
     }
     }
     }
     );*/

    $('.dinheiro').mask('#0.00', {reverse: true, placeholder: "0.00"});
    $('.dinheiro3').mask('#0.000', {reverse: true, placeholder: "0.00"});
    $('[name="aVista"]').mask('#0.00', {reverse: true});
    $('[name="valorParcela"]').mask('#0.00', {reverse: true, placeholder: "0.00"});
}
);