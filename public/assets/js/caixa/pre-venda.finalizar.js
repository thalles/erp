$(document).ready(function () {




    $('[name=aVista], [name="quantidadeParcelas"]').keyup(function () {
        var total = $('td#total-compra').attr('data-total');
        var quantidadeParcelas = $('[name="quantidadeParcelas"]').val();
        var aVista = $('[name=aVista]').val();
        var aPrazo = (total - aVista) / quantidadeParcelas;

        $('[name="valorParcela"]').val(aPrazo);
    });
});


if (document.getElementById('finalizar-pedido'))
    var appFinalizaPedido = new Vue({
        el: '#finalizar-pedido',
        data: {
            produtosVenda: [],
            venda: [],
            vendedor: [],

        },
        mounted: function () {
            var vendaId = document.querySelector('#vendaId').value;
            var action = '/caixa/pre-venda/get/id/' + vendaId;
            enableLayer();
            this.$http.get(action).then(response => {
                //console.log('Aqui!');
                if (response.body.message) {
                    alertDanger(response.body.message);
                    return false;
                }
                this.produtosVenda = response.body.produtos;
                this.venda = response.body.venda;
                this.vendedor = response.body.vendedor;
                document.querySelector('#vendedor').value = response.body.vendedor.id;
            }, response => {
                alert('Erro ao salvar dados!');
            });
            disableLayer();
        },
        methods: {

        },
        computed: {

            total: function () {
                var sum = 0;
                return this.produtosVenda.reduce(function (prev, product) {
                    return sum += (product.valorAtual * product.quantidade) -
                            (((product.valorAtual * product.quantidade) * product.desconto) / 100);
                }, 0);


                return this.teste;
            }
        }
    });
