$(document).ready(function () {

    $('#add-modal').click(function () {
        $('.form-categoria').modal();
        sendForm('#form-categoria');
    });

    if ($('[name="emissor"]').val() > 0) {
        loadEndereco($('[name="emissor"]').val(), 'Emissor');
        $('[name="enderecoEmissor"]').val($('[name="emissor"]').val());

    }
});
function loadModelo() {
    $('.th-marca').typeahead('destroy');
    loadTypeahead('/gestao/marca/lista', '.th-marca', 'marca');
}
;

reloadOptionsCategorias = function (categorias) {
    options = "<option value='-1'>...</option>";
    $.each(categorias, function (index, name) {
        options += "<option value='" + name.id + "'>" + name.name + " NCM: " + name.ncm + "</option>";
    });
    $('select[name="categoria"]').html(options);
};

loadTypeahead = function (url, seletor, campoId) {
    if (!$(seletor).length) {
        return false;
    }
    var input = $(seletor);
    $.post(url, function (data) {
        $(seletor).typeahead({source: data});
        if ($('#' + campoId).length > 0) {
            $.each(data, function (index, name) {
                if ($('#' + campoId).val() == name.id) {
                    $(seletor).val(name.name);
                }
            });
        }

        sendFormModal('#form-' + campoId, url, seletor, campoId);
        input.change(function () {
            var current = $(seletor).typeahead("getActive");//
            if (current) {
                if (current.name == $(seletor).val()) {
                    $('#' + campoId).val(current.id);
                    $(seletor).typeahead('destroy');
                    loadTypeahead(url, seletor, campoId);
                    if (campoId == 'marca') {
                        loadModelo();
                    } else if (campoId == 'emissor') {
                        loadEndereco(current.id, 'Emissor');
                    }
                } else {
                    $('#' + campoId).val('');
                }
            } else {
//                $('#' + campoId).val('');
            }
        });
        addModal($(seletor), campoId);
    }, "json");
};

addModal = function (input, campoId) {
    input.blur(function () {
        if (input.val().length > 0 && $('#' + campoId).val() == '') {
            if (!$('.modal').hasClass('in')) {
                //campo que será preenchido dentro do modal, deve estar marcado com a class alvo-modal
                preenche = input.val().split(";");
                $('#form-' + campoId + ' .alvo-modal').val(preenche[0]);
                $('.form-' + campoId).modal();
            }
        }

        if (input.val().length === 0) {
            $('#' + campoId).val('');
        }
    });
};


sendFormModal = function (formSelector, url, seletor, campoId) {
    // prepare Options Object
    var options = {
        resetForm: true,
        success: function (result) {
            if (result.id) {
                $(formSelector + ' [name="id"]').val(result.id);
                $('[name="' + campoId + '"]').val(result.id);
                $(formSelector).parents('.modal').modal('hide');
                $(seletor).typeahead('destroy');
                loadTypeahead(url, seletor, campoId);
                if (campoId == 'marca') {
                    loadModelo();
                } else if (campoId == 'emissor') {
                    loadEndereco(result.id, 'Emissor');
                }
            } else {
                errorOnSave();
            }
            return disableLayer();
        },
        error: function () {
            errorOnSave();
            return disableLayer();
        },
        beforeSubmit: function () {
            return enableLayer();
        }
    };
    $(formSelector).ajaxForm(options);
};





