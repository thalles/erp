if (document.getElementById('contato'))
    var appContato = new Vue({
        el: '#contato',
        data: {
            contacts: contacts,
            pessoa: document.querySelector('#pessoa').value,
            contactEdit: {
                responsavel: "",
                pessoa: document.querySelector('#pessoa').value
            }
        },
        mounted: function () {

        },
        methods: {
            saveContact: function () {
                var action = $('#form-contatoFisica').attr('action');
                this.pessoa = document.querySelector('#pessoa').value;
                this.contactEdit.pessoa = this.pessoa;
                enableLayer();
                this.$http.post(action, JSON.parse(JSON.stringify(this.contactEdit))).then(response => {

                    if (response.body.mensage) {
                        alertDanger(response.body.mensage);
                        return false
                    }
                    if (!this.contactEdit.id)
                        this.contacts.push(response.body);
                    this.contacts[this.contactEdit.index] = JSON.parse(JSON.stringify(this.contactEdit));
                    alertSuccess('Dados armazenado com sucesso.');
                    this.resetFormContact();
                }, response => {
                    alert('Erro ao salvar dados!');
                });
                disableLayer();
            },
            editContact(index) {
                this.contactEdit = JSON.parse(JSON.stringify(this.contacts[index]));
                this.contactEdit.index = index;
            },
            removeContact: function (index) {
                var confirma = confirm('Deseja excluir o contato?');
                enableLayer();
                if (confirma) {
                    this.$http.post('/gestao/contato/delete', {id: this.contacts[index].id}).then(function (results) {
                        var result = results.body;
                        if (result.mensagem) {
                            console.log(result.mensagem)
                            alertSuccess(result.mensagem);
                        }
                        if (result.excluido) {
                            this.contacts.splice(index, 1);
                        }
                    }, function () {
                        alertDanger('Erro ao excluir contato!');
                    });
                }
                disableLayer();
            },
            resetFormContact: function () {
                this.contactEdit = {
                    responsavel: "",
                    pessoa: document.querySelector('#pessoa').value
                }
            }

        }
    });