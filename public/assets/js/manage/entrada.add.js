$(document).ready(function () {
    simpleForm('#form-transportador-cpf');
    simpleForm('#form-transportador-cnpj');

    $('#modal-transportador-cpf').click(function () {
        $('.form-transportador-cpf').modal();

    });

    $('#modal-transportador-cnpj').click(function () {
        $('.form-transportador-cnpj').modal();
    });
});

simpleForm = function (formSelector) {
    // prepare Options Object
    var options = {
        url: '/gestao/entrada/transportador',
        success: function (result) {
            if (result.id) {
                $('.th-transportador').typeahead('destroy');
                loadTypeahead('/gestao/entrada/transportadores', '.th-transportador', 'transportador');
                $(formSelector + ' [name="pessoa"]').val(result.id);
            } else {
                errorOnSave();
            }
            return disableLayer();
        },
        error: function () {
            errorOnSave();
            return disableLayer();
        },
        beforeSubmit: function () {
            return enableLayer();
        }
    };
    $(formSelector).ajaxForm(options);
};




//id = sendForm('form-pessoa');

