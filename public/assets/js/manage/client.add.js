$(document).ready(function () {
    var redir = $('select[name="padrao"]');
    redir.change(function () {
        if (redir.val() === 'cpf') {
            $(location).attr("href", 'add-fisica');
        } else if (redir.val() === 'cnpj') {
            $(location).attr("href", 'add-juridica');
        }
    });
});
