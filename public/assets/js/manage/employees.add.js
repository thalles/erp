$(document).ready(function () {
    enablePersonTabs();
    submitPerson();
});




submitPerson = function () {
    var options = {
        beforeSubmit: function () {
            enableLayer();
        },
        success: function (result) {
            $('#pessoa').val(result.id);
            $('[name="pessoa"]').val(result.id);
            $('[name="fisica"]').val(result.id);
            enablePersonTabs();
            disableLayer();
        },
        error: function () {
            disableLayer();
            alertDanger("Ocorreu um erro.");
        },
        dataType: 'json'
    };

    // bind form using 'ajaxForm'
    $('#form-pessoa').ajaxForm(options);
};

submitForm = function (form) {
    var options = {
        beforeSubmit: function () {
            enableLayer();
        },
        success: function (result) {
            disableLayer();
        },
        error: function () {
            disableLayer();
            alert("Ocorreu um erro.");
        },
        dataType: 'json'
    };


    $(form).ajaxForm(options);
};