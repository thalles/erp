$(document).ready(function () {


});

$('[name="aliquotaIcms"]').keyup(function () {
    icms = $(this).val() * 1;
    total = $('[name="valorTotal"]').val() * 1;
    valorIcms = (icms * total / 100).toFixed(2);
    $('[name="valorIcms"]').val(valorIcms);
});

$('[name="aliquotaIpi"]').keyup(function () {
    ipi = $(this).val() * 1;
    total = $('[name="valorTotal"]').val() * 1;
    valorIpi = (ipi * total / 100).toFixed(2);
    $('[name="valorIpi"]').val(valorIpi);
});



$('[name="valorUnitario"], [name="desconto"], [name="quantidade"]').keyup(function () {
    calculaTotal();
});

function calculaTotal() {
    valorUnitario = $('[name="valorUnitario"]').val() * 1;
    desconto = $('[name="desconto"]').val() * 1;
    quantidade = $('[name="quantidade"]').val() * 1;
    custo = valorUnitario - desconto;
    total = custo * quantidade;
    $('[name="valorMinimo"]').val(custo);
    $('[name="valorTotal"]').val(total);
}