var editando = {};
var UFs = {
    "AC": "Acre",
    "AL": "Alagoas",
    "AP": "Amapá",
    "AM": "Amazonas",
    "BA": "Bahia",
    "CE": "Ceará",
    "DF": "Distrito Federal",
    "ES": "Espírito Santo",
    "GO": "Goiás",
    "MA": "Maranhão",
    "MT": "Mato Grosso",
    "MS": "Mato Grosso do Sul",
    "MG": "Minas Gerais",
    "PA": "Pará",
    "PB": "Paríba",
    "PR": "Paraná",
    "PE": "Pernambuco",
    "PI": "Piauí",
    "RJ": "Rio de Janeiro",
    "RN": "Rio Grande do Norte",
    "RS": "Rio Grande do Sul",
    "RO": "Rondónia",
    "RR": "Roraima",
    "SC": "Santa Catarina",
    "SP": "São Paulo",
    "SE": "Sergipe",
    "TO": "Tocantins"
};


(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);

editAdress = function (seletor) {
    $(seletor).on('click', function () {
        editando = $(this);
        var dataAdress = $.parseJSON($(this).attr('data-adress'));
        for (data in dataAdress) {
            $('[name="' + data + '"]').val(dataAdress[data]);
        }
        $('select[name="uf"]').trigger("change");
        $('[name="cidade"]').val(dataAdress['cidade']);
    });
};



getCitiesOnChange = function (seletoUf, seletorCity) {
    $(seletoUf).change(function () {
        var uf = $(this).val();
        $.ajax({
            method: "post",
            async: false,
            url: "/gestao/endereco/cidades",
            data: {'id': uf},
            beforeSend: function () {
                enableLayer();
            },
            success: function (dados) {

                options = "<option value='-1'>...</option>";
                $.each(dados, function (index, name) {
                    options += "<option value='" + name.id + "'>" + name.name + "</option>";
                });

                $(seletorCity).html(options);
                disableLayer();
            },
            error: function () {
                alert('Ocorreu um erro!');
                disableLayer();
            }
        });
    });
};

loadEndereco = function (id, campo) {
    $.ajax({
        method: "post",
        url: "/gestao/endereco/enderecos",
        data: {'id': id},
        beforeSend: function () {
            enableLayer();
        },
        success: function (dados) {
            options = "";
            $.each(dados, function (index, name) {
                options += "<option value='" + name.id + "'>" + name.name + "</option>";
            });
            if (options == "") {
                options = "<option value=''>Não há endereço cadastrado</option>";
            }
            $('[name="endereco' + campo + '"]').html(options);
            disableLayer();
        },
        error: function () {
            alert('Ocorreu um erro!');
            disableLayer();
        }
    });
};

if (document.getElementById('endereço'))
    var appAdress = new Vue({
        el: '#endereço',
        data: {
            adresses: adresses,
            pessoa: document.querySelector('#pessoa').value,
            adressEdit: {
                cep: "",
                logradouro: "",
                cidade: -1,
                uf: -1,
                regiao: -1
            }
        },
        mounted: function () {
            //console.log(this.$el);
        },
        methods: {
            editAdress(index) {
                enableLayer();
                this.$http.post('/gestao/endereco/cidades', {id: this.adresses[index].uf}).then(function (results) {
                    result = results.body
                    $('[name="cidade"]').html('');
                    for (position in result) {
                        $('[name="cidade"]').append('<option value="' + result[position].id + '">' + result[position].name + '</option>');
                    }
                    this.adressEdit = JSON.parse(JSON.stringify(this.adresses[index]));
                    this.adressEdit.index = index;
                    disableLayer();
                }, function () {
                    disableLayer();
                });
            },
            saveAdress() {
                var action = $('#form-endereco').attr('action');
                this.pessoa = document.querySelector('#pessoa').value;
                this.adressEdit.pessoa = this.pessoa;
                enableLayer();
                this.$http.post(action, JSON.parse(JSON.stringify(this.adressEdit))).then(function (results) {
                    disableLayer();
                    if (results.body.mensage) {
                        alertDanger(results.body.mensage);
                        return false;
                    }
                    if (!this.adressEdit.id) {
                        this.adressEdit.id = results.body.id;
                        this.adresses.push(results.body);
                    }

                    this.adresses[this.adressEdit.index] = JSON.parse(JSON.stringify(results.body));
                    alertSuccess('Dados armazenado com sucesso.');
                    this.resetAdress();
                }, function () {

                    disableLayer();
                });
                return false;
            },
            deleteAdress: function (index) {

                var confirma = confirm('Deseja excluir o endereço?');

                if (confirma) {
                    this.$http.post('/gestao/endereco/delete', {id: this.adresses[index].id}).then(function (results) {
                        var result = results.body;
                        if (result.mensagem) {
                            console.log(result.mensagem)
                            alertSuccess(result.mensagem);
                        }
                        if (result.excluido) {
                            this.adresses.splice(index, 1);
                        }
                    }, function () {
                        alertDanger('Erro ao excluir endereço!');
                    });
                }
            },
            setLogradouro: function () {
                if (this.adressEdit.cep.length >= 8) {
                    this.$http.get("https://viacep.com.br/ws/" + this.adressEdit.cep + "/json/").then(function (results) {
                        var data = results.body;
                        var cidade = 0;
                        this.adressEdit.uf = $('[name="uf"] option:contains(' + UFs[data.uf] + ')').val();
                        this.getCities(function () {
                            appAdress.adressEdit.cidade = $('[name="cidade"] option:contains(' + data.localidade + ')').val();
                            $('[name="cidade"] option:contains(' + data.localidade + ')').attr('selected', 'selected');
                        });

                        this.adressEdit.logradouro = data.logradouro;
                        this.adressEdit.bairro = data.bairro;
                        disableLayer();
                    }, function () {
                        disableLayer();
                    });
                }
            },
            resetAdress: function () {
                this.adressEdit = {
                    cep: "",
                    logradouro: ""
                };
            },
            getCities: function (callback = false) {
                enableLayer();
                this.$http.post('/gestao/endereco/cidades', {id: this.adressEdit.uf}).then(function (results) {
                    result = results.body
                    $('[name="cidade"]').html('');
                    $('[name="cidade"]').append('<option value="">...</option>');
                    for (position in result) {
                        $('[name="cidade"]').append('<option value="' + result[position].id + '">' + result[position].name + '</option>');
                    }
                    if (callback)
                        callback()
                    disableLayer();
                }, function () {
                    disableLayer();
                });
            }

        }
    });

//console.log(appAdress.pessoa)

