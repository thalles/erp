$(document).ready(function () {

    //sendForm('#form-seguranca');
});

if (document.getElementById('form-seguranca')) {

    var appAcess = new Vue({
        el: '#form-seguranca',
        data: {
            fisica: document.querySelector('#pessoa').value,
            acessEdit: {email: ""}
        },
        mounted: function () {
            if (!Array.isArray(seguranca)) {
                this.acessEdit = seguranca;
            }

        },
        methods: {
            saveAccess: function () {
                var action = $('#form-seguranca').attr('action');
                this.acessEdit.fisica = document.querySelector('#pessoa').value;

                enableLayer();
                this.$http.post(action, JSON.parse(JSON.stringify(this.acessEdit))).then(response => {
                    //console.log('Aqui!');

                    if (response.body.message) {
                        alertDanger(response.body.message);
                        return false;
                    }
                    alertSuccess('Dados armazenado com sucesso. A senha foi enviada para ' + this.acessEdit.email);
                }, response => {
                    alert('Erro ao salvar dados!');
                });
                disableLayer();
            },
        }
    });
}
