<div class="modal fade" id="alert-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="alert-modal-label">Alerta!</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div id="footer-alert" class="modal-footer">
                <button type="button" class="btn btn-outline" data-dismiss="modal">Fechar</button>
            </div>
            <div id="footer-confirm" class="modal-footer hidden">
                <button type="button" id="false" class="btn btn-outline pull-left" data-dismiss="modal">Fechar</button>
                <button type="button" id="true" class="btn btn-outline">Executar</button>
            </div>
        </div>
    </div>
</div>