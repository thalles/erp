
var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var pump = require('pump');
var imagemin = require('gulp-imagemin');
var livereload = require('gulp-livereload');
var connect = require('gulp-connect-php');
var compass = require('gulp-compass');
var sourcemaps = require('gulp-sourcemaps');


gulp.task('default', [
    'sass',
    'js',
    'image',
    'watch',
    'serve',
    'connect',
    'compass',
    'sourcemaps'
]);

gulp.task('sass', function () {
    return gulp.src('public/assets/sass/**/*.scss')
            .pipe(sourcemaps.init())
            .pipe(concat('style.min.css'))
            .pipe(sass({
                outputStyle: 'compressed'
            }).on('error', sass.logError))
            .pipe(sourcemaps.write('maps'))
            .pipe(gulp.dest('public/css'));
});

gulp.task('compass', function () {
    gulp.src('public/assets/sass/**/*.scss')
            .pipe(compass({
                css: 'public/assets/sass/css',
                sass: 'public/assets/sass',
                //sourcemap: true
            }))
            .pipe(concat('style.min.css'))
            .pipe(gulp.dest('public/css'));
});

gulp.task('js', function (cb) {
    pump([
        gulp.src('public/assets/js/**/*.js'),
        sourcemaps.init(),
        concat('script.min.js'),
        //uglify(),
        sourcemaps.write('maps'),
        gulp.dest('public/js')
    ], cb);
});

gulp.task('image', function () {
    return gulp.src('public/assets/img/*')
            .pipe(imagemin({
                progressive: true,
                optimizationLevel: 4,
                svgoPlugins: [{
                        removeViewBox: true
                    }]
            }))
            .pipe(gulp.dest('public/img'));
});



gulp.task('serve', function () {

    livereload.listen({});

    gulp.watch([
        'module/**/*.php',
        'module/**/*.phtml',
        'config/**/*.php',
        'public/**/*.php',
    ]).on('change', function (file) {
        setTimeout(function () {
            livereload.reload();
        }, 1000)

    });

    gulp.watch('public/assets/sass/**/*.scss', ['compass']);
    gulp.watch('public/assets/js/**/*.js', ['js']);
    gulp.watch('public/assets/img/*', ['image']);

    gulp.watch('public/css/**/*.css').on('change', function (file) {
        setTimeout(function () {
            livereload.reload('public/css/style.min.css');
        }, 2000)

    });

    gulp.watch(['public/js/**/*.js', 'public/assets/img/*']).on('change', function (file) {
        setTimeout(function () {
            livereload.reload(file);
        }, 2000);

    });
});

gulp.task('serveloc', function () {

    connect.server({
        base: 'public',
        port: '80'
    });

    livereload.listen({});

    gulp.watch([
        'module/**/*.php',
        'module/**/*.phtml',
        'config/**/*.php',
        'public/**/*.php',
    ]).on('change', function (file) {
        livereload.reload();
    });

    gulp.watch('public/assets/sass/**/*.scss', ['compass']);
    gulp.watch('public/assets/js/**/*.js', ['js']);
    gulp.watch('public/assets/img/*', ['image']);

    gulp.watch('public/css/**/*.css').on('change', function (file) {
        livereload.reload('public/css/style.min.css');
    });

    gulp.watch(['public/js/**/*.js', 'public/assets/img/*']).on('change', function (file) {
        livereload.reload(file);
    });
});
